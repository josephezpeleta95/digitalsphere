"use client";
import CallbackComponent from "./components/callback";

import { SessionProvider } from "next-auth/react";
import React from "react";

export default function Home() {
  return (
    <SessionProvider>
      <CallbackComponent />
    </SessionProvider>
  );
}
