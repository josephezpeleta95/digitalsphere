import React, { useEffect, useState } from "react";
import { c_generatesession } from "@/app/controller/(profile)/c_login";
import { useRouter } from "next/navigation";
import { useSession } from "next-auth/react";

export default function Callback() {
  const { data: session, status } = useSession();
  const router = useRouter();
  const [apiCalled, setApiCalled] = useState(false);

  const handleSignInWithGoogle = async () => {
    const redirectSuccesUrl = "../profile";
    const redirectErrorUrl = "/";
    try {
      if (session && session.user && session.user.email && !apiCalled) {
        const email = session.user.email;
        const name = session.user.name;
        const image = session.user.image;
        c_generatesession(email, name, image)
          .then((response) => {
            if (response.code == "1111") {
              router.push(redirectSuccesUrl);
            } else {
              router.push(redirectErrorUrl);
            }
          })
          .catch((error) => {
            router.push(redirectErrorUrl);
          });
        setApiCalled(true);
      }
    } catch (error) {
      console.error("Error signing in and saving user data:", error);
      router.push(redirectErrorUrl);
    }
  };

  useEffect(() => {
    const checkAuthentication = async () => {
      try {
        if (status === "authenticated" || status === "loading") {
          handleSignInWithGoogle();
        } else {
          await router.push("/");
        }
      } catch (error) {
        console.error("Error while redirecting:", error);
      }
    };
    checkAuthentication();
  }, [status, router]);

  return (
    <div className='w-full h-screen items-center justify-center flex'>
      <div className='w-6/12 relative rounded-lg flex items-center justify-between bg-white shadow-lg mt-4 p-6'>
        <div className='flex items-center space-x-4'>
          <svg
            className='animate-spin h-5 w-5 text-slate-600'
            xmlns='http://www.w3.org/2000/svg'
            fill='none'
            viewBox='0 0 24 24'>
            <circle
              className='opacity-25'
              cx='12'
              cy='12'
              r='10'
              stroke='currentColor'
              strokeWidth='4'></circle>
            <path
              className='opacity-75'
              fill='currentColor'
              d='M4 12a8 8 0 018-8v8h8a8 8 0 01-8 8v-8H4z'></path>
          </svg>
          <h1 className='text-slate-900 text-xl font-semibold'>
            Verifying Data...
          </h1>
        </div>
      </div>
    </div>
  );
}
