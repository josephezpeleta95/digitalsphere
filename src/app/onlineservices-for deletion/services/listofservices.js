export default function Services() {
  const services = [
    {
      service_title: "Web Development",
      service: "Full Stack Web Development with Logo Design",
      image: "/images/digitalproducts/flyers.jpg",
      rate: "50,000.00",
      per: "Mon",
      setup: "Full-time",
      service_description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
      Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa.`,
    },
    {
      service_title: "Data Analyst",
      service: "Data Research and Encoding",
      image: "/images/digitalproducts/flyers.jpg",
      rate: "50,000.00",
      per: "Mon",
      setup: "Full-time",
      service_description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit.  Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh.`,
    },
    {
      service_title: "Video Editing",
      service: "Brand Video Design",
      image: "/images/digitalproducts/flyers.jpg",
      rate: "50,000.00",
      per: "Mon",
      setup: "Full-time",
      service_description: `Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh.`,
    },
    {
      service_title: "Social Media Merketing Specialist",
      service: "Social Media MArket Research",
      image: "/images/digitalproducts/flyers.jpg",
      rate: "50,000.00",
      per: "Mon",
      setup: "Full-time",
      service_description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
      Integer nec odio.Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh.`,
    },
    {
      service_title: "Programming",
      service: "C# Programmer",
      image: "/images/digitalproducts/flyers.jpg",
      rate: "50,000.00",
      per: "Mon",
      setup: "Full-time",
      service_description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh.`,
    },
    {
      service_title: "AI",
      service: "AI Developer",
      image: "/images/digitalproducts/flyers.jpg",
      rate: "50,000.00",
      per: "Mon",
      setup: "Full-time",
      service_description: `Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh.`,
    },
    {
      service_title: "Graphic Illustration",
      service: "Graphic Artist",
      image: "/images/digitalproducts/flyers.jpg",
      rate: "50,000.00",
      per: "Mon",
      setup: "Full-time",
      service_description: ` Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh.`,
    },
    {
      service_title: "Bookkeeping",
      service: "Bookkeeper and Acccounting Servicesy",
      image: "/images/digitalproducts/flyers.jpg",
      rate: "50,000.00",
      per: "Mon",
      setup: "Full-time",
      service_description: `Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh.`,
    },
    {
      service_title: "Virtual Assistant",
      service: "Medical Realted Jobs",
      image: "/images/digitalproducts/flyers.jpg",
      rate: "50,000.00",
      per: "Mon",
      setup: "Full-time",
      service_description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
      Integer nec odio. Praesent libero. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh.`,
    },
    {
      service_title: "3D Illustration",
      service: "3D Animation",
      image: "/images/digitalproducts/flyers.jpg",
      rate: "50,000.00",
      per: "Mon",
      setup: "Full-time",
      service_description: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
      Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh.`,
    },
  ];

  const JobsCards = (
    { service_title, service, image, rate, per, setup, service_description },
    index
  ) => (
    <>
      <div className='w-10/12 px-5 py-1 border border-dashed  mr-2 mb-2'>
        <div className='w-full h-50  flex justify-between '>
          <div className='w-1/4 h-50  p-1 flex justify-between items-center transform transition-transform hover:scale-105 cursor-pointer'>
            <img
              src={image}
              alt={`Image Cayap${index}`}
              className='w-full h-full object-cover rounded-lg'
            />
          </div>
          <div className='w-3/4 h-50'>
            <div className='w-full flex items-start p-1'>
              <div className='w-6/12 text-lg px-4 text-sky-700 font-semibold'>
                {service_title}
              </div>
              <div className='w-6/12 text-sm p-1 flex justify-end text-slate-500 pr-4'>
                {setup}
              </div>
            </div>
            <div className='w-full flex px-1'>
              <div className='text-sm px-4  text-slate-500'>{service}</div>
            </div>
            <div className='w-full flex flex-grow h-48 px-1 mt-2'>
              <div className='px-4 text-ellipsis overflow-hidden'>
                <p className='text-slate-500 text-xs'>
                  {truncateString(service_description, 900)}
                </p>
              </div>
            </div>
            <div className='w-full flex px-1'>
              <div className='w-6/12 px-4  items-center'>
                <p className='text-sky-700 text-sm'>
                  Salary: {rate} /{per}
                </p>
              </div>
              <div className='w-6/12 flex justify-end pr-4'>
                <button className='bg-pink-500 rounded-full px-3 py-2 text-white text-xs'>
                  Message Now
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );

  function truncateString(str, maxLength) {
    if (str.length <= maxLength) {
      return str;
    } else {
      return str.slice(0, maxLength - 3) + "...";
    }
  }

  return (
    //   Header Navbar
    <div>
      <div className='bg-slate-50 w-full items-center flex justify-center'>
        <div className='w-11/12  mt-4'>
          <div className='w-full p-5 text-2xl justify-center flex'>
            <div className='w-10/12 flex'>
              <div className='w-5/12 items-center flex'>
                <h1 className='font-roboto leading-normal text-pink-500 font-normal'>
                  Online Jobs
                </h1>
              </div>

              <div className='w-7/12 items-center flex'>
                <div class='relative  w-full'>
                  <input
                    class='rounded-lg w-full pl-8 pr-4 py-1 text-slate-500 text-lg border border-gray-300 focus:border-blue-500 focus:outline-none ml-2'
                    type='search'
                    placeholder='Search'></input>
                  <div class='absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none'>
                    <svg
                      xmlns='http://www.w3.org/2000/svg'
                      viewBox='0 0 24 24'
                      fill='currentColor'
                      className='w-6 h-6'>
                      <path d='M8.25 10.875a2.625 2.625 0 1 1 5.25 0 2.625 2.625 0 0 1-5.25 0Z' />
                      <path
                        fill-rule='evenodd'
                        d='M12 2.25c-5.385 0-9.75 4.365-9.75 9.75s4.365 9.75 9.75 9.75 9.75-4.365 9.75-9.75S17.385 2.25 12 2.25Zm-1.125 4.5a4.125 4.125 0 1 0 2.338 7.524l2.007 2.006a.75.75 0 1 0 1.06-1.06l-2.006-2.007a4.125 4.125 0 0 0-3.399-6.463Z'
                        clip-rule='evenodd'
                      />
                    </svg>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='z-10 lg:w-full flex bg-slate-50 '>
            <div className='w-full  flex justify-center flex-wrap items-center p-4 mb-10'>
              {/* Map products  */}

              {services.map((services, index) => (
                <JobsCards {...services} key={index} />
              ))}
              {/*End  Map products  */}
            </div>
          </div>
          <div className='justify-end items-center flex mb-10 w-full'>
            <div className='flex justify-center mt-4 '>
              <nav
                className='relative z-0 inline-flex rounded-md shadow-sm -space-x-px'
                aria-label='Pagination'>
                <a
                  href='#'
                  className='relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50'>
                  Previous
                </a>
                <a
                  href='#'
                  className='relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50'>
                  1
                </a>
                <a
                  href='#'
                  className='relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50'>
                  2
                </a>
                <a
                  href='#'
                  className='relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50'>
                  3
                </a>
                <span className='relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700'>
                  ...
                </span>
                <a
                  href='#'
                  className='relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50'>
                  8
                </a>
                <a
                  href='#'
                  className='relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50'>
                  9
                </a>
                <a
                  href='#'
                  className='relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50'>
                  10
                </a>
                <a
                  href='#'
                  className='relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50'>
                  Next
                </a>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
