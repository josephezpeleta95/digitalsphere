"use client";
import Header from "@header/header";
import Footer from "@footer/footer";
import TopFooter from "@topfooter/topfooter";
import Jobs from "./services/listofservices";
import { SessionProvider } from "next-auth/react";
export default function Services() {
  return (
    <main className='flex flex-col items-center justify-between bg-white'>
      <SessionProvider>
        <Header />
      </SessionProvider>
      <Jobs />
      <TopFooter />
      <Footer />
    </main>
  );
}
