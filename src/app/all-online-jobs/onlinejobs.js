import React, { useState, useEffect, useRef } from "react";
import { c_getJobListings } from "@/app/controller/(main)/c_main";
import Loading from "@/app/(profile)/profile/components/loading";
import Link from "next/link";

export default function Jobs() {
  const [jobListings, setJoblistings] = useState([]);
  const [filterQuery, setFilterQuery] = useState(""); // State to manage the filter query
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const contentRef = useRef(null);
  const [currentPage, setCurrentPage] = useState(1);
  const listingsPerPage = 5;

  // Logic for displaying job listings on current page
  const indexOfLastListing = currentPage * listingsPerPage;
  const indexOfFirstListing = indexOfLastListing - listingsPerPage;
  const currentListings = jobListings.slice(
    indexOfFirstListing,
    indexOfLastListing
  );

  // Logic for page navigation
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  const toggleScroll = () => {
    const content = contentRef.current;
    if (content && content.scrollHeight > content.clientHeight) {
      // Check if content exists before accessing scrollHeight
      content.classList.add("scrollable");
      content.classList.remove("no-scroll");
    } else if (content) {
      content.classList.add("no-scroll");
      content.classList.remove("scrollable");
    }
  };

  const handleFetchJoblistings = () => {
    c_getJobListings()
      .then((value) => {
        setJoblistings(value.rows);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const filteredJobs = currentListings.filter((job) =>
    job.title.toLowerCase().includes(filterQuery.toLowerCase())
  );

  useEffect(() => {
    toggleScroll();
    handleFetchJoblistings();
  }, []);

  const JobsCards = ({
    title,
    company_name,
    description,
    salary_range,
    job_id,
    industry,
    job_location,
    date_posted,
    closed_date,
    skills,
    setup,
    work_arrangement,
  }) => (
    <>
      {company_name ? (
        <div key={job_id} className='w-full py-1 mr-2 mb-2'>
          <div key={job_id} className='border p-4 rounded-md shadow-md'>
            <h2 className='text-xl font-semibold mb-2 text-slate-600'>
              {title}
            </h2>
            <p className='text-orange-600 mb-2 text-sm'>
              <span> {company_name}</span> <span>({industry})</span>
            </p>
            <div
              ref={contentRef}
              className='text-sm font-sans text-slate-700 h-52 overflow-auto'
              dangerouslySetInnerHTML={{ __html: description }}
            />
            <div className='sm:flex justify-between items-center mt-10'>
              <div className='sm:w-6/12 w-full'>
                <p className='text-gray-700 mb-1'>
                  <span className='font-semibold'>Location: </span>
                  <span className='font-light'> {job_location}</span>
                </p>
                <p className='text-gray-700 mb-1'>
                  <span className='font-semibold'>Posted: </span>
                  <span className='font-light'>
                    {new Date(date_posted).toLocaleDateString()}
                  </span>
                </p>
              </div>

              <div className='sm:w-6/12 w-full'>
                <p className='text-gray-700 mb-1'>
                  <span className='font-semibold'> Salary: </span>
                  <span className='font-light'> {salary_range}</span>
                </p>
                <p className='text-gray-700 mb-1'>
                  <span className='font-semibold'>Closed Date: </span>

                  <span className='font-light'>
                    {new Date(closed_date).toLocaleDateString()}
                  </span>
                </p>
              </div>
            </div>
            <div>
              <h3 className='font-semibold mt-4 text-slate-600'>
                Skills Required:
              </h3>
              <ul className='flex flex-wrap gap-2 mt-2'>
                {JSON.parse(skills).map((skill, index) => (
                  <li
                    key={index}
                    className='text-gray-700 border border-pink-500 px-2 py-1 rounded-full'>
                    {skill}
                  </li>
                ))}
              </ul>
            </div>
            <div>
              <p className='mb-1 mt-5 text-lg'>
                <span className='font-semibold text-sky-600'>{setup}</span>
                {" - "}
                <span className='font-light'>{work_arrangement}</span>
              </p>
            </div>
            <div className='w-full flex px-1'>
              <div className='w-full flex justify-end pr-4'>
                <Link
                  href={`/jobs/${job_id}`}
                  target='_blank'
                  className='sm:w-3/12 text-white  flex justify-end rounded text-xs'>
                  {" "}
                  <button className=' bg-blue-500 text-white px-4 py-2 rounded text-xs'>
                    View
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    //   Header Navbar
    <div className='bg-white w-9/12 mt-4'>
      <div className='w-full p-5 text-2xl'>
        <h1 className='font-roboto leading-normal text-sky-700 font-semibold'>
          Job Posting
        </h1>
      </div>
      <div className='w-full p-5 text-2xl'>
        <input
          type='text'
          className='w-full border border-slate-200 p-1 text-slate-400'
          placeholder='Search job...'
          value={filterQuery}
          onChange={(e) => setFilterQuery(e.target.value)}
        />
      </div>

      <div className='z-10 lg:w-full flex'>
        <div className='w-full  flex justify-center flex-wrap items-center p-4 mb-10'>
          {/* Map products  */}

          {filteredJobs.map((jobslists, index) => (
            <JobsCards {...jobslists} key={index} />
          ))}
          {/*End  Map products  */}
        </div>
      </div>
      {/* Pagination */}
      <div className='mt-8 flex justify-center'>
        <nav className='flex'>
          {[
            ...Array(Math.ceil(jobListings.length / listingsPerPage)).keys(),
          ].map((pageNumber) => (
            <button
              key={pageNumber}
              onClick={() => paginate(pageNumber + 1)}
              className={`mx-1 px-3 py-1 rounded-md ${
                pageNumber + 1 === currentPage
                  ? "bg-blue-500 text-white"
                  : "bg-gray-200 text-gray-700 hover:bg-gray-300"
              }`}>
              {pageNumber + 1}
            </button>
          ))}
        </nav>
      </div>
    </div>
  );
}
