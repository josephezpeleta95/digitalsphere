"use client";
import Header from "@header/header";
import Footer from "@footer/footer";
import TopFooter from "@topfooter/topfooter";
import Content from "./onlinejobs";
import { SessionProvider } from "next-auth/react";
export default function OnlineJobs() {
  return (
    <main className='flex flex-col items-center justify-between bg-white'>
      <SessionProvider>
        <Header />
      </SessionProvider>
      <Content />
      <TopFooter />
      <Footer />
    </main>
  );
}
