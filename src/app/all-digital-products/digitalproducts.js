import React, { useEffect, useState } from "react";
import { c_products } from "@/app/controller/(main)/c_main";
import Link from "next/link";
import Loading from "@/app/(profile)/profile/components/loading";

export default function DigitalProducts() {
  const [products, setProducts] = useState([]);
  const [filterQuery, setFilterQuery] = useState("");
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  function addCommas(numString) {
    return Number(numString).toLocaleString();
  }

  const ProductCard = ({
    username,
    userlname,
    covers,
    userprofilepic,
    price,
    total_sold,
    product_name,
    total_likes,
    product_id,
  }) => (
    <Link
      href={`/product/${product_id}`}
      target='_blank'
      className='w-full sm:w-1/2 md:w-1/3 lg:w-1/4 px-2 py-4'>
      <div className='bg-white shadow-lg rounded-lg overflow-hidden p-2'>
        <div
          className='bg-cover bg-center h-48'
          style={{
            backgroundImage: `url(/${covers[0].product_cover.replace(
              /^\.\/public\//,
              ""
            )})`,
          }}></div>
        <div className='p-4 bg-slate-50 h-48'>
          <div className='bg-slate-600 bg-opacity-90 w-full p-1 border rounded-full flex mb-1'>
            <div
              className='w-8 h-8 rounded-full bg-sky-300 bg-cover bg-center border'
              style={{ backgroundImage: `url(${userprofilepic})` }}></div>
            <div className='items-center flex'>
              <h5 className='text-xs ml-1 text-white truncate overflow-hidden'>
                {username} {userlname}
              </h5>
            </div>
          </div>
          <div className='h-10'>
            <h5 className='font-semibold text-sm text-gray-800 line-clamp-2'>
              {product_name}
            </h5>
          </div>
          <div className='w-full pt-4'>
            <div className='flex justify-between mt-2'>
              <span className='text-gray-600'>
                Sold: {total_sold ? total_sold : 0}
              </span>
              <span className='text-sky-600 font-bold'>
                ₱ {addCommas(price)}
              </span>
            </div>
            <div className='flex justify-between mt-1'>
              <span className='text-gray-600'>Likes: {total_likes}</span>
            </div>
          </div>
        </div>
      </div>
    </Link>
  );

  const fetchProducts = () => {
    c_products()
      .then((value) => {
        setProducts(value.rows);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  // Filter products based on filterQuery
  const filteredProducts = products.filter((product) =>
    product.product_name.toLowerCase().includes(filterQuery.toLowerCase())
  );

  useEffect(() => {
    fetchProducts();
  }, []);

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div className='bg-white w-9/12 px-4 mt-4'>
      <div className='w-full text-2xl mb-4'>
        <h1 className='font-roboto leading-normal text-sky-700 font-semibold'>
          Digital Products
        </h1>
      </div>
      <div className='w-full mb-4'>
        <input
          type='text'
          className='w-full border border-slate-200 p-2 text-gray-700'
          placeholder='Search products...'
          value={filterQuery}
          onChange={(e) => setFilterQuery(e.target.value)}
        />
      </div>
      <div className='flex flex-wrap -mx-2'>
        {/* Map filtered products */}
        {filteredProducts.map((productprofile, index) => (
          <ProductCard {...productprofile} key={index} />
        ))}
        {/* End Map filtered products */}
      </div>
    </div>
  );
}
