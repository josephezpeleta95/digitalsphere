import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function GET(request, { params }) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  const userId = params.user_id;
  try {
    // Perform a SELECT query to retrieve product data

    const result = await pool.query(
      `WITH ProductDetails AS (
            SELECT 
                p.product_id,
                p.product_name,
                p.product_description,
                p.price,
                p.date_added,
                p.user_id,
                (
                    SELECT 
                        json_agg(json_build_object('product_c_id', pc.product_c_id, 'product_cover', pc.product_cover))
                    FROM 
                        tbl_product_cover pc
                    WHERE 
                        pc.product_id = p.product_id
                    LIMIT 1
                ) AS covers,
                (
                    SELECT 
                        json_agg(json_build_object('product_f_id', pf.product_f_id, 'product_file', pf.product_file))
                    FROM 
                        tbl_product_file pf
                    WHERE 
                        pf.product_id = p.product_id
                ) AS files
            FROM 
                tbl_product p
            WHERE 
                p.user_id = $1
        )
        SELECT 
            pd.product_id,
            pd.product_name,
            pd.product_description,
            pd.price,
            pd.date_added,
            pd.user_id,
            pd.covers,
            pd.files,
            (
                SELECT 
                    json_agg(json_build_object('sold_id', s.sold_id, 'quantity', s.quantity, 'price', s.price, 'date_sold', s.date_sold))
                FROM 
                    tbl_sold s
                WHERE 
                    s.product_id = pd.product_id
            ) AS sold_info,
            (
                SELECT 
                    SUM(s.quantity)  -- Sum of quantity of sold items
                FROM 
                    tbl_sold s
                WHERE 
                    s.product_id = pd.product_id
            ) AS total_sold,  -- Total quantity of sold items
            (
                SELECT 
                    COUNT(*)
                FROM 
                    tbl_likes l
                WHERE 
                    l.product_id = pd.product_id
            ) AS total_likes
        FROM 
            ProductDetails pd;`,
      [userId]
    );
    const rows = result.rows;

    // Return the data along with a 200 OK response
    return NextResponse.json({ rows }, { status: 200 });
  } catch (error) {
    await logAudit(
      sesData.sessiondata.email,
      "SELECT product [user_id]",
      false,
      error.message
    );
    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error retrieving product data: " + error.message,
      },
      { status: 500 }
    );
  }
}
