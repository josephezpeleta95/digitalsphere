import { pipeline } from "stream/promises";
import { NextResponse } from "next/server";
import fs from "fs";
import { v4 as uuidv4 } from "uuid";
import { pool } from "../../../config/db";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function POST(request) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  let productDirPath;

  try {
    const formData = await request.formData();
    const userId = formData.get("user_id");
    const directoryPath = `./public/photos/${userId}`;
    const productName = formData.get("productName");
    const productDescription = formData.get("productDescription");
    const price = formData.get("price");

    // Generate a UUID for the product directory
    const productUuid = uuidv4();
    productDirPath = `${directoryPath}/${productUuid}`; // Assign value to productDirPath

    if (!fs.existsSync(productDirPath)) {
      fs.mkdirSync(productDirPath, { recursive: true });
    }

    // Execute the insert query
    const results = await pool.query(
      `INSERT INTO tbl_product (product_id, product_name, product_description, price, user_id)
       VALUES ($1, $2, $3, $4, $5)`,
      [productUuid, productName, productDescription, price, userId]
    );

    // Check if the insertion was successful
    if (results.rowCount >= 1) {
      // Save cover files

      const newFileName = new Date()
        .toISOString()
        .replace(/:/g, "-")
        .replace(/\..+/, "")
        .replace("T", "_");

      const coverFiles = formData.getAll("coverfile[]");
      for (let i = 0; i < coverFiles.length; i++) {
        // const coverFileName = `cover_${i + 1}.jpg`;
        const coverFileName = `cover_${newFileName + i}.jpg`;
        const filePath = `${productDirPath}/${coverFileName}`;
        await pipeline(coverFiles[i].stream(), fs.createWriteStream(filePath));
        // Save the formatted path into the array

        // Execute the insert query
        const results_cover = await pool.query(
          `INSERT INTO tbl_product_cover (product_cover, product_id)
       VALUES ($1, $2)`,
          [filePath, productUuid]
        );

        if (results_cover.rowCount >= 1) {
          await logAudit(
            sesData.sessiondata.email,
            "INSERT INTO tbl_product_cover ",
            true,
            "Succesfully save product cover.."
          );
        } else {
          await logAudit(
            sesData.sessiondata.email,
            "INSERT INTO tbl_product_cover ",
            false,
            "Error on saving product cover.."
          );
        }
      }
    } else {
      await logAudit(
        sesData.sessiondata.email,
        "INSERT INTO tbl_product_cover ",
        false,
        "Failed to insert data into tbl_product."
      );
    }

    // Save product file
    const productFile = formData.get("productfile");
    const productFileName = productFile.name; // Get the original filename
    const product = `${productDirPath}/${productFileName}`;
    await pipeline(
      productFile.stream(),
      fs.createWriteStream(`${productDirPath}/${productFileName}`)
    );

    const results_file = await pool.query(
      `INSERT INTO tbl_product_file (product_file, product_id)
        VALUES ($1, $2)`,
      [product, productUuid]
    );
    if (results_file.rowCount >= 1) {
      await logAudit(
        sesData.sessiondata.email,
        "INSERT INTO tbl_product_file ",
        true,
        "Succesfully save product file.."
      );
    } else {
      await logAudit(
        sesData.sessiondata.email,
        "INSERT INTO tbl_product_file ",
        false,
        "Failed to insert data into tbl_product."
      );
    }

    await logAudit(
      sesData.sessiondata.email,
      "INSERT INTO tbl_product_file ",
      true,
      "Data Successfully Saved"
    );
    return NextResponse.json({
      success: true,
      message: "Data Successfully Saved",
    });
  } catch (error) {
    console.error("Error uploading file");

    // If an error occurs, delete the product directory
    if (productDirPath) {
      try {
        await fs.rm(productDirPath, { recursive: true });

        await logAudit(
          sesData.sessiondata.email,
          "INSERT INTO tbl_product_file ",
          true,
          "Product directory deleted due to error."
        );
      } catch (error) {
        await logAudit(
          sesData.sessiondata.email,
          "INSERT INTO tbl_product_file ",
          false,
          error.message
        );
      }
    }
    await logAudit(
      sesData.sessiondata.email,
      "INSERT INTO tbl_product_file ",
      false,
      error.message
    );
    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error uploading file: " + error.message,
      },
      { status: 401 }
    );
  }
}
