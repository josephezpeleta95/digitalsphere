import fs from "fs";
import { pipeline } from "stream/promises";
import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function POST(request) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  try {
    const formData = await request.formData();
    const file = formData.get("resumeFile");
    const userId = formData.get("userId");
    const userFname = formData.get("userFname");
    const userLname = formData.get("userLname");
    if (!file) {
      await logAudit(
        sesData.sessiondata.email,
        "CHEKING ATTACHED FILE",
        false,
        "No file found in the request."
      );
      throw new Error("No file found in the request.");
    }

    // Define the new file name
    const newFileName = `${userFname}_${userLname}.pdf`;
    const directoryPath = `./trx_files/bx_jobs/resume/${userId}`;
    const filePath = `${directoryPath}/${newFileName}`;
    const filePathDb = `./trx_files/bx_jobs/resume/${userId}/${newFileName}`;

    // Create the directory if it doesn't exist
    if (!fs.existsSync(directoryPath)) {
      fs.mkdirSync(directoryPath, { recursive: true });
    }

    // Using pipeline to efficiently pipe data from readable stream to writable stream
    await pipeline(file.stream(), fs.createWriteStream(filePath));

    // UPSERT query to insert if not exist, otherwise update
    const result = await pool.query(
      `INSERT INTO tbl_resume_file (user_id, file_path, uploaded_at)
      VALUES ($1, $2, NOW())
      ON CONFLICT (user_id)
      DO UPDATE SET file_path = EXCLUDED.file_path, uploaded_at = EXCLUDED.uploaded_at;
      `,
      [userId, filePathDb]
    );

    if (result.rowCount >= 1) {
      await logAudit(
        sesData.sessiondata.email,
        "INSERT INTO tbl_resume_file ",
        true,
        "Data Successfully Saved"
      );
      return NextResponse.json(
        {
          success: true,
          message: "Data Successfully Saved",
        },
        { status: 200 }
      );
    } else {
      await logAudit(
        sesData.sessiondata.email,
        "INSERT INTO tbl_resume_file ",
        false,
        "Failed to update or insert record."
      );
      throw new Error("Failed to update or insert record.");
    }
  } catch (error) {
    console.error("Error uploading file:", error);

    await logAudit(
      sesData.sessiondata.email,
      "INSERT INTO tbl_resume_file ",
      false,
      error.message
    );
    return NextResponse.json(
      {
        success: false,
        message: "Error uploading file",
        error: error.message,
      },
      { status: 401 }
    );
  }
}
