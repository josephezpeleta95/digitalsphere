import { NextResponse } from "next/server";
import { pool } from "../../../../../config/db";
import fs from "fs";
import path from "path";

export async function GET(request, { params }) {
  const userId = params.user_id;

  if (!userId) {
    return NextResponse.json(
      { success: false, message: "Product ID is required" },
      { status: 400 }
    );
  }

  try {
    // Perform a SELECT query to retrieve product file data
    const result = await pool.query(
      `SELECT * FROM tbl_resume_file WHERE user_id = $1`,
      [userId]
    );

    if (result.rowCount === 0) {
      return NextResponse.json(
        { success: false, message: "File not found for the given product ID" },
        { status: 404 }
      );
    }
    const resumeFile = result.rows[0].file_path;
    const filePath = path.join(process.cwd(), resumeFile);

    // Check if the file exists
    if (!fs.existsSync(filePath)) {
      return NextResponse.json(
        { success: false, message: "File not found" },
        { status: 404 }
      );
    }

    // Create a file stream
    const fileStream = fs.createReadStream(filePath);

    // Update headers to indicate inline content and set appropriate content type
    const headers = new Headers({
      "Content-Disposition": `inline; filename=${path.basename(filePath)}`, // Display file inline
      "Content-Type": "application/pdf", // Adjust content type based on file type
    });

    return new Response(fileStream, {
      headers,
    });
  } catch (error) {
    console.error("Error on retrieving data", error);

    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error on retrieving data: " + error.message,
      },
      { status: 500 }
    );
  }
}
