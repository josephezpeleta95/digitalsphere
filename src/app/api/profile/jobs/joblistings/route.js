import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function GET(request) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation
  try {
    // Perform a SELECT query to retrieve user data including certifications
    const result = await pool.query(
      `SELECT 
        j.job_id,
        j.title,
        j.description,
        j.location AS job_location,
        j.date_posted,
        j.salary_range,
        j.closed_date,
        j.setup,
        j.skills,
        j.work_arrangement,
        c.company_id,
        c.company_name,
        c.location AS company_location,
        c.company_email,
        c.contact_number,
        c.contact_person,
        c.website,
        c.industry,
        c.size
    FROM 
        tbl_job_listings j
    INNER JOIN 
        tbl_company_profile c ON j.company_id = c.company_id ORDER BY j.date_posted DESC;
      `
    );
    const rows = result.rows;
    if (result.rowCount > 0) {
      return NextResponse.json(
        { rows },
        {
          success: true,
        },
        { status: 200 }
      );
    } else {
      return NextResponse.json(
        { rows },
        {
          success: false,
          message: "No result.",
        },
        { status: 204 }
      );
    }
  } catch (error) {
    console.error("Error on retrieving data", error);
    await logAudit(
      sesData.sessiondata.email,
      "SELECT FROM tbl_job_listings",
      false,
      error.message
    );
    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error on retrieving data: " + error.message,
      },
      { status: 500 }
    );
  }
}
