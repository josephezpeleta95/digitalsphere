import { NextResponse } from "next/server";
import { pool } from "../../../../../config/db";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function GET(request, { params }) {
  const userId = params.user_id;

  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  try {
    // Perform a SELECT query to retrieve product data

    const result = await pool.query(
      `SELECT 
            ja.application_id,
            ja.application_date,
            ja.status,
            ja.resume_link,
            u.user_id,
            u.username,  -- Assume tbl_users has a user_name column
            u.userlname,  -- Assume tbl_users has a user_name column
            u.useremail,      -- Assume tbl_users has an email column
            jl.job_id,
            jl.title,
            jl.description,
            jl.location AS job_location,
            jl.date_posted,
            jl.salary_range,
            jl.closed_date,
            jl.setup,
            jl.skills,
            cp.company_id,
            cp.company_name,
            cp.location AS company_location,
            cp.company_email,
            cp.contact_number,
            cp.contact_person,
            cp.website,
            cp.industry,
            cp.size
        FROM 
            tbl_job_applications ja
        JOIN 
            tbl_users u ON ja.user_id = u.user_id
        JOIN 
            tbl_job_listings jl ON ja.job_id = jl.job_id
        JOIN 
            tbl_company_profile cp ON ja.company_id = cp.company_id
        WHERE 
            ja.user_id =$1;
`,
      [userId]
    );
    const rows = result.rows;
    if (result.rowCount > 0) {
      return NextResponse.json(
        { rows },
        {
          success: false,
        },
        { status: 200 }
      );
    } else {
      return NextResponse.json(
        { rows },
        {
          success: false,
          message: "No result.",
        },
        { status: 204 }
      );
    }
  } catch (error) {
    console.error("Error retrieving product data:", error);
    await logAudit(
      sesData.sessiondata.email,
      "SELECT tbl_job_applications - [user_id]",
      false,
      error.message
    );
    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error retrieving product data: " + error.message,
      },
      { status: 500 }
    );
  }
}
