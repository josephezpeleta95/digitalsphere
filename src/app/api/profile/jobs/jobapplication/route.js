"use server";
import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";
import { logAudit } from "@/app/lib/logAudit";
import { validateSession } from "@/app/lib/sessionUtils";

export async function POST(request) {
  const res = await request.json();
  const { jobId, userId, companyId } = res;

  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  try {
    // Insert data into tbl_users
    const results = await pool.query(
      `INSERT INTO tbl_job_applications (job_id, user_id, company_id)
        VALUES ($1,$2,$3)`,
      [jobId, userId, companyId]
    );

    if (results.rowCount >= 1) {
      await logAudit(
        sesData.sessiondata.email,
        "INSERT INTO tbl_job_applications ",
        true,
        "Data saved for job application"
      );
      return NextResponse.json(
        {
          success: true,
        },
        { status: 200 },
        { message: "Data Successfully Saved" }
      );
    } else {
      await logAudit(
        sesData.sessiondata.email,
        "INSERT INTO tbl_job_applications ",
        false,
        "Error on saving data for job application"
      );
      return NextResponse.json(
        {
          success: false,
        },
        { status: 401 },
        { message: "Error on saving data" }
      );
    }
  } catch (error) {
    console.log(error.message);
    await logAudit(
      sesData.sessiondata.email,
      "INSERT INTO tbl_job_applications ",
      false,
      error.message
    );
    return NextResponse.json(
      {
        success: false,
      },
      { status: 401 },
      { message: error }
    );
  }
}
