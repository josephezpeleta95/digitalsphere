import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";
import { logAudit } from "@/app/lib/logAudit";
import { validateSession } from "@/app/lib/sessionUtils";

export async function POST(request) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  try {
    const {
      personalInfo,
      educations,
      workExperiences,
      projects,
      certifications,
    } = await request.json();

    // Update tbl_users with personal info and skills
    const updateUserQuery = `
      UPDATE tbl_users
      SET
        complete_address = $1,
        phone = $2,
        zip_code = $3,
        expected_salary = $4,
        work_setup = $5,
        languages = $6,
        dateupdated = CURRENT_TIMESTAMP
      WHERE user_id = $7
    `;
    const updateUserValues = [
      personalInfo.address,
      personalInfo.phone,
      personalInfo.zip,
      personalInfo.expectedSalary,
      personalInfo.workSetUp,
      personalInfo.languages,
      personalInfo.userId,
    ];

    await pool.query(updateUserQuery, updateUserValues);

    // // Insert into tbl_education and Project
    const deleteEducationQuery = `
  DELETE FROM tbl_education WHERE user_id = $1`;

    const insertEducationQuery = `
  INSERT INTO tbl_education (user_id, school, degree, field_of_study, start_date, end_date)
  VALUES ($1, $2, $3, $4, $5, $6)`;

    const deleteProjectsQuery = `
  DELETE FROM tbl_projects WHERE user_id = $1`;

    const insertProjectsQuery = `
  INSERT INTO tbl_projects (user_id, title, description, link)
  VALUES ($1, $2, $3, $4)`;

    const deleteCertificationsQuery = `
  DELETE FROM tbl_certifications WHERE user_id = $1`;

    const insertCertificationsQuery = `
  INSERT INTO tbl_certifications (user_id, certification_name, issuing_organization, issue_date, expiration_date, credential_id, credential_url)
  VALUES ($1, $2, $3, $4, $5, $6, $7)`;

    const deleteExperienceQuery = `
  DELETE FROM tbl_experience WHERE user_id = $1`;

    const insertExperienceQuery = `
  INSERT INTO tbl_experience (user_id, company, position, start_date, end_date, responsibilities, skills)
  VALUES ($1, $2, $3, $4, $5, $6, $7)`;

    try {
      // Begin a transaction
      await pool.query("BEGIN");

      // Delete existing education records for the user
      await pool.query(deleteEducationQuery, [personalInfo.userId]);

      // Insert new education records
      for (const edu of educations) {
        const educationValues = [
          personalInfo.userId,
          edu.schoolName,
          edu.degree,
          edu.fieldOfStudy,
          edu.startDate,
          edu.endDate,
        ];
        await pool.query(insertEducationQuery, educationValues);
      }

      // Delete existing project records for the user
      await pool.query(deleteProjectsQuery, [personalInfo.userId]);

      // Insert new project records
      for (const project of projects) {
        const projectValues = [
          personalInfo.userId,
          project.projectTitle,
          project.projectDescription,
          project.projectLink,
        ];
        await pool.query(insertProjectsQuery, projectValues);
      }

      // Delete existing certification records for the user
      await pool.query(deleteCertificationsQuery, [personalInfo.userId]);

      // Insert new certification records
      for (const cert of certifications) {
        const certificationValues = [
          personalInfo.userId,
          cert.certificationName,
          cert.issuingOrganization,
          cert.issueDate,
          cert.expirationDate,
          cert.credentialID,
          cert.credentialURL,
        ];
        await pool.query(insertCertificationsQuery, certificationValues);
      }

      // Delete existing experience records for the user
      await pool.query(deleteExperienceQuery, [personalInfo.userId]);

      // Insert new experience records with skills
      for (const exp of workExperiences) {
        const experienceValues = [
          personalInfo.userId,
          exp.companyName,
          exp.jobTitle,
          exp.startDate,
          exp.endDate,
          exp.responsibilities,
          exp.skills, // This should be an array of skills
        ];
        await pool.query(insertExperienceQuery, experienceValues);
      }

      // Commit the transaction
      await pool.query("COMMIT");
      await logAudit(
        sesData.sessiondata.email,
        "UPDATE, INSERT, DELETE",
        true,
        "Data save for resume builder"
      );
      return NextResponse.json(
        {
          success: true,
          message: "Data Successfully Saved",
        },
        { status: 200 }
      );
    } catch (error) {
      // Rollback the transaction in case of error
      await pool.query("ROLLBACK");
      console.error("Error inserting records:", error);
      await logAudit(
        sesData.sessiondata.email,
        "UPDATE, INSERT, DELETE",
        false,
        error.message
      );
      return NextResponse.json(
        {
          success: false,
          message: "Error",
        },
        { status: 404 }
      );

      // Handle error as needed
    }
  } catch (error) {
    console.error("Error saving:", error);
    await logAudit(
      sesData.sessiondata.email,
      "UPDATE, INSERT, DELETE",
      false,
      error.message
    );
    return NextResponse.json(
      {
        success: false,
        message: "Error",
        error: error.message,
      },
      { status: 500 }
    );
  }
}
