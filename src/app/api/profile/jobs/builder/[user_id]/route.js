import { NextResponse } from "next/server";
import { pool } from "../../../../../config/db";

export async function GET(request, { params }) {
  const userId = params.user_id;

  if (!userId) {
    return NextResponse.json(
      { success: false, message: "User ID is required" },
      { status: 400 }
    );
  }

  try {
    // Perform a SELECT query to retrieve user data including certifications
    const result = await pool.query(
      `SELECT
        jsonb_build_object(
            'personalInfo', jsonb_build_object(
                'firstName', u.username,
                'lastName', u.userlname,
                'email', u.useremail,
                'phone', u.phone,
                'address', u.complete_address,
                'city', u.usermuni,
                'state', u.userprov,
                'zip', u.zip_code,
                'expected_salary', u.expected_salary,
                'work_setup', u.work_setup,
                'languages', u.languages
            ),
            'education', (
                SELECT jsonb_agg(
                    jsonb_build_object(
                        'schoolName', e.school,
                        'degree', e.degree,
                        'fieldOfStudy', e.field_of_study,
                        'startDate', e.start_date,
                        'endDate', e.end_date
                    )
                )
                FROM tbl_education e
                WHERE e.user_id = u.user_id
            ),
            'experience', (
                SELECT jsonb_agg(
                    jsonb_build_object(
                        'companyName', ex.company,
                        'jobTitle', ex.position,
                        'startDate', ex.start_date,
                        'endDate', ex.end_date,
                        'responsibilities', ex.responsibilities,
                        'skills', ex.skills
                    )
                )
                FROM tbl_experience ex
                WHERE ex.user_id = u.user_id
            ),
            'projects', (
                SELECT jsonb_agg(
                    jsonb_build_object(
                        'projectTitle', p.title,
                        'projectDescription', p.description,
                        'projectLink', p.link
                    )
                )
                FROM tbl_projects p
                WHERE p.user_id = u.user_id
            ),
            'certifications', (
                SELECT jsonb_agg(
                    jsonb_build_object(
                        'certificationName', c.certification_name,
                        'issuingOrganization', c.issuing_organization,
                        'issueDate', c.issue_date,
                        'expirationDate', c.expiration_date,
                        'credentialID', c.credential_id,
                        'credentialURL', c.credential_url
                    )
                )
                FROM tbl_certifications c
                WHERE c.user_id = u.user_id
            )
        ) AS user_data
      FROM tbl_users u  
      WHERE u.user_id = $1;
      `,
      [userId]
    );
    const rows = result.rows;
    if (result.rowCount > 0) {
      return NextResponse.json({ rows }, { status: 200 });
    } else {
      return NextResponse.json(
        { rows },
        {
          success: false,
          message: "No result.",
        },
        { status: 404 }
      );
    }
  } catch (error) {
    console.error("Error on retrieving data", error);

    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error on retrieving data: " + error.message,
      },
      { status: 500 }
    );
  }
}
