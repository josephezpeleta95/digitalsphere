import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";
import fs from "fs";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function DELETE(request, { params }) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  const product_c_id = params.product_c_id;

  try {
    // Perform a SELECT query to retrieve product data before deleting
    const selectResult = await pool.query(
      `SELECT product_cover FROM tbl_product_cover WHERE product_c_id = $1`,
      [product_c_id]
    );

    // Extract the user_id from the result
    const product_cover = selectResult.rows[0]?.product_cover;

    // Check if the SELECT query retrieved valid data
    if (!product_cover) {
      await logAudit(
        sesData.sessiondata.email,
        "IS product cover exists true - profile/deleteproductfile/[product_f_id]",
        false,
        "Product cover does not exists"
      );
      throw new Error("Product not found");
    }

    // Perform the DELETE query
    const deleteResult = await pool.query(
      `DELETE FROM tbl_product_cover WHERE product_c_id = $1`,
      [product_c_id]
    );

    // Check if the delete query was successful
    if (deleteResult.rowCount > 0) {
      // If successful, proceed with directory deletion
      // Delete the directory associated with the product
      if (fs.existsSync(product_cover)) {
        fs.unlinkSync(product_cover);

        await logAudit(
          sesData.sessiondata.email,
          "DELETE product cover - profile/deleteproductfile/[product_f_id]",
          true,
          `Deleted file: ${product_cover}`
        );
        // Return success response
        return NextResponse.json(
          {
            success: true,
            message: "Product Deleted",
          },
          { status: 200 }
        );
      } else {
        await logAudit(
          sesData.sessiondata.email,
          "DELETE product cover - profile/deleteproductfile/[product_f_id]",
          false,
          `Error deleting product cover`
        );
        return NextResponse.json(
          {
            success: false,
            message: "Error deleting product cover",
          },
          { status: 401 }
        );
      }
    }
  } catch (error) {
    // Return a failure response with the error details
    await logAudit(
      sesData.sessiondata.email,
      "DELETE product cover - profile/deleteproductfile/[product_f_id]",
      false,
      error.message
    );
    return NextResponse.json(
      {
        success: false,
        message: "Error deleting product: " + error.message,
      },
      { status: 500 }
    );
  }
}
