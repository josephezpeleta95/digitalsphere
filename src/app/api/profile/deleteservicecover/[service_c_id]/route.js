import { NextResponse } from "next/server";
import { pool } from "./../../../../config/db";
import fs from "fs";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function DELETE(request, { params }) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  const service_c_id = params.service_c_id;

  try {
    // Perform a SELECT query to retrieve product data before deleting
    const selectResult = await pool.query(
      `SELECT service_cover FROM tbl_service_cover WHERE service_c_id = $1`,
      [service_c_id]
    );

    // Extract the user_id from the result
    const service_cover = selectResult.rows[0]?.service_cover;

    // Check if the SELECT query retrieved valid data
    if (!service_cover) {
      await logAudit(
        sesData.sessiondata.email,
        "IS service_cover true - profile/deleteservicecover/[service_id]",
        false,
        "Service cover not found"
      );
      throw new Error("Service cover not found");
    }

    // Perform the DELETE query
    const deleteResult = await pool.query(
      `DELETE FROM tbl_service_cover WHERE service_c_id = $1`,
      [service_c_id]
    );

    // Check if the delete query was successful
    if (deleteResult.rowCount > 0) {
      // If successful, proceed with directory deletion
      // Delete the directory associated with the product
      if (fs.existsSync(service_cover)) {
        fs.unlinkSync(service_cover);
        console.log(`Deleted file: ${service_cover}`);

        await logAudit(
          sesData.sessiondata.email,
          "DELETE service_cover file - profile/deleteservicecover/[service_id]",
          true,
          `Deleted file: ${service_cover}`
        );
        // Return success response
        return NextResponse.json(
          {
            success: true,
            message: "Product Deleted",
          },
          { status: 200 }
        );
      } else {
        await logAudit(
          sesData.sessiondata.email,
          "DELETE service_cover file - profile/deleteservicecover/[service_id]",
          false,
          `Deleted file: ${service_cover} error`
        );
        return NextResponse.json(
          {
            success: false,
            message: `Deleted file: ${service_cover} error`,
          },
          { status: 401 }
        );
      }
    }
  } catch (error) {
    await logAudit(
      sesData.sessiondata.email,
      "DELETE service_cover file - profile/deleteservicecover/[service_id]",
      false,
      error.message
    );
    return NextResponse.json(
      {
        success: false,
        message: "Error deleting product: " + error.message,
      },
      { status: 500 }
    );
  }
}
