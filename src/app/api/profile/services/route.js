import { pipeline } from "stream/promises";
import { NextResponse } from "next/server";
import fs from "fs";
import { v4 as uuidv4 } from "uuid";
import { pool } from "../../../config/db";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function POST(request) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  let productDirPath;

  try {
    const formData = await request.formData();
    const userId = formData.get("userId");
    const directoryPath = `./public/photos/services/${userId}`;
    const serviceName = formData.get("serviceName");
    const serviceDescription = formData.get("serviceDescription");
    const rate = formData.get("rate");
    const workSetUp = formData.get("workSetUp");
    const workper = formData.get("workper");

    // Generate a UUID for the product directory
    const serviceUuid = uuidv4();
    productDirPath = `${directoryPath}/${serviceUuid}`; // Assign value to productDirPath

    if (!fs.existsSync(productDirPath)) {
      fs.mkdirSync(productDirPath, { recursive: true });
    }

    // Execute the insert query
    const results = await pool.query(
      `INSERT INTO tbl_services (service_id, service_name, service_description, service_rate, service_work_setup, service_per, user_id)
       VALUES ($1, $2, $3, $4, $5, $6, $7)`,
      [
        serviceUuid,
        serviceName,
        serviceDescription,
        rate,
        workSetUp,
        workper,
        userId,
      ]
    );

    // Check if the insertion was successful
    if (results.rowCount >= 1) {
      // Save cover files
      const newFileName = new Date()
        .toISOString()
        .replace(/:/g, "-")
        .replace(/\..+/, "")
        .replace("T", "_");

      const coverFiles = formData.getAll("coverfile[]");
      for (let i = 0; i < coverFiles.length; i++) {
        // const coverFileName = `cover_${i + 1}.jpg`;
        const coverFileName = `cover_${newFileName + i}.jpg`;
        const filePath = `${productDirPath}/${coverFileName}`;
        await pipeline(coverFiles[i].stream(), fs.createWriteStream(filePath));
        // Save the formatted path into the array

        // Execute the insert query
        const results_cover = await pool.query(
          `INSERT INTO tbl_service_cover (service_cover, service_id)
           VALUES ($1, $2)`,
          [filePath, serviceUuid]
        );

        if (results_cover.rowCount >= 1) {
          await logAudit(
            sesData.sessiondata.email,
            "INSERT INTO tbl_service_cover",
            true,
            "Succesfully save product cover.."
          );
        } else {
          await logAudit(
            sesData.sessiondata.email,
            "INSERT INTO tbl_service_cover",
            false,
            "Error on saving product cover.."
          );
        }
      }
    } else {
      await logAudit(
        sesData.sessiondata.email,
        "INSERT INTO tbl_service_cover",
        false,
        "Failed to insert data into tbl_product."
      );
    }

    // Save product file
    await logAudit(
      sesData.sessiondata.email,
      "INSERT INTO tbl_service_cover",
      true,
      "Data Successfully Saved"
    );
    return NextResponse.json({
      success: true,
      message: "Data Successfully Saved",
    });
  } catch (error) {
    await logAudit(
      sesData.sessiondata.email,
      "INSERT INTO tbl_service_cover",
      true,
      error.message
    );
    // If an error occurs, delete the product directory
    if (productDirPath) {
      try {
        await fs.rm(productDirPath, { recursive: true });
        await logAudit(
          sesData.sessiondata.email,
          "INSERT INTO tbl_service_cover",
          true,
          "Product directory deleted due to error."
        );
      } catch (error) {
        await logAudit(
          sesData.sessiondata.email,
          "INSERT INTO tbl_service_cover",
          false,
          error.message
        );
        console.error("Error deleting product directory:", error.message);
      }
    }

    // Return a failure response with the error details
    await logAudit(
      sesData.sessiondata.email,
      "INSERT INTO tbl_service_cover",
      false,
      error.message
    );
    return NextResponse.json(
      {
        success: false,
        message: "Error uploading file: " + error.message,
      },
      { status: 401 }
    );
  }
}
