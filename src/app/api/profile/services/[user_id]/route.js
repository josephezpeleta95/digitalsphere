import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function GET(request, { params }) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation
  const userId = params.user_id;
  try {
    // Perform a SELECT query to retrieve product data

    const result = await pool.query(
      `SELECT 
            s.*,
              (
                    SELECT 
                        json_agg(json_build_object('service_cover', sc.service_cover, 'service_cover_id',  sc.service_c_id))
                    FROM 
                        tbl_service_cover sc
                    WHERE 
                        s.service_id = sc.service_id
                    LIMIT 1
                ) AS covers,
            u.username,
            u.userlname,
            u.useremail,
            u.userskills,
            u.userheadline,
            u.userprofilepic,
            u.datecreated,
            u.status,
            u.dateupdated
        FROM 
            tbl_services s
        LEFT JOIN 
            tbl_users u ON s.user_id = u.user_id
            WHERE s.user_id =$1
        GROUP BY 
            s.service_id, u.user_id`,
      [userId]
    );
    const rows = result.rows;
    // Return the data along with a 200 OK response
    return NextResponse.json({ rows }, { status: 200 });
  } catch (error) {
    await logAudit(
      sesData.sessiondata.email,
      "SELECT services",
      false,
      error.message
    );
    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error retrieving product data: " + error.message,
      },
      { status: 500 }
    );
  }
}
