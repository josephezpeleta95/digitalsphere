import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";
import { logAudit } from "@/app/lib/logAudit";
import { validateSession } from "@/app/lib/sessionUtils";

export async function GET(request, { params }) {
  const threadId = params.thread_id;

  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  // Fetch the user ID based on the session email
  const userResult = await pool.query(
    `SELECT user_id FROM tbl_users WHERE useremail = $1`,
    [sesData.sessiondata.email]
  );

  const userRows = userResult.rows;
  if (userRows.length === 0) {
    await logAudit(
      sesData.sessiondata.email,
      "SELECT user_id FROM tbl_users [thread_id] - profile/message/[thread_id]",
      false,
      "User not found in the database"
    );
    return NextResponse.json({ error: "User not found" }, { status: 404 });
  }
  const userId = userRows[0].user_id;

  try {
    // Update is_read field to true for messages where recipient ID matches user ID and thread ID is the same
    await pool.query(
      `UPDATE tbl_messages 
       SET is_read = true 
       WHERE thread_id = $1 AND recipient_id = $2`,
      [threadId, userId]
    );

    // Fetch updated messages
    const result = await pool.query(
      `SELECT 
        m.thread_id,
        m.sender_id,
        CONCAT(sender.username, ' ', sender.userlname) AS sender_name,
        m.recipient_id,
        CONCAT(recipient.username, ' ', recipient.userlname) AS recipient_name,
        m.subject,
        m.message_text,
        m.sent_at,
        m.is_read
      FROM 
        tbl_messages m
      JOIN 
        tbl_users sender ON m.sender_id = sender.user_id
      JOIN 
        tbl_users recipient ON m.recipient_id = recipient.user_id
      WHERE 
        thread_id = $1
      GROUP BY 
        m.thread_id, m.sender_id, sender_name, m.recipient_id, recipient_name, m.subject, m.message_text, m.sent_at, m.is_read 
      ORDER BY 
        m.sent_at ASC`,
      [threadId]
    );
    const rows = result.rows;

    // Return the updated data along with a 200 OK response
    return NextResponse.json({ rows }, { status: 200 });
  } catch (error) {
    // Handle any errors that occur during the execution
    await logAudit(
      sesData.sessiondata.email,
      "SELECT Messages [thread_id]",
      false,
      `${error.message} - profile/message/[thread_id]`
    );
    return NextResponse.json(
      { error: "Internal Server Error" },
      { status: 500 }
    );
  }
}
