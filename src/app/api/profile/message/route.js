"use server";
import { NextResponse } from "next/server";
import { pool } from "../../../config/db";
import { v4 as uuidv4 } from "uuid";
import { logAudit } from "@/app/lib/logAudit";
import { validateSession } from "@/app/lib/sessionUtils";

export async function POST(request) {
  const res = await request.json();
  const { recipientId, threadId, subject, message } = res;
  const thread_Id = threadId || uuidv4();

  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  try {
    const result = await pool.query(
      `SELECT user_id FROM tbl_users WHERE useremail = $1`,
      [sesData.sessiondata.email]
    );
    const rows = result.rows;
    if (rows.length === 0) {
      await logAudit(
        sesData.sessiondata.email,
        "SELECT user_id FROM tbl_users ",
        false,
        "User not found in the database"
      );
      return NextResponse.json(
        {
          success: false,
          message: "User not found",
        },
        { status: 404 }
      );
    }

    const senderId = rows[0].user_id;
    // Insert data into tbl_messages
    const results = await pool.query(
      `INSERT INTO tbl_messages (thread_id, sender_id, recipient_id, subject, message_text)
        VALUES ($1, $2, $3, $4, $5)`,
      [thread_Id, senderId, recipientId, subject, message]
    );

    if (results.rowCount >= 1) {
      await logAudit(
        sesData.sessiondata.email,
        "INSERT INTO tbl_messages",
        true,
        "Message saved in the database - profile/message"
      );
      return NextResponse.json(
        {
          success: true,
          message: "Message saved in the database",
        },
        { status: 200 }
      );
    } else {
      await logAudit(
        sesData.sessiondata.email,
        "INSERT INTO tbl_messages",
        false,
        "Failed to insert message - profile/message"
      );
      return NextResponse.json(
        {
          success: false,
          message: "Failed to insert message",
        },
        { status: 500 }
      );
    }
  } catch (error) {
    await logAudit(
      sesData.sessiondata.email,
      "INSERT INTO tbl_messages",
      false,
      `${error.message} - profile/message`
    );
    return NextResponse.json(
      {
        success: false,
        message: error.message,
      },
      { status: 500 }
    );
  }
}

export async function GET(request) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  try {
    // Insert data into

    // Fetch the user ID based on the session email
    const userResult = await pool.query(
      `SELECT user_id FROM tbl_users WHERE useremail = $1`,
      [sesData.sessiondata.email]
    );

    const userRows = userResult.rows;
    if (userRows.length === 0) {
      await logAudit(
        sesData.sessiondata.email,
        "SELECT user_id FROM tbl_users - messages",
        false,
        "User not found in the database"
      );
      return NextResponse.json({ error: "User not found" }, { status: 404 });
    }

    const userId = userRows[0].user_id;

    // Fetch messages where the user is either the sender or recipient, grouped by thread_id
    const messagesResult = await pool.query(
      `
       WITH latest_messages AS (
            SELECT 
            thread_id,
            MAX(sent_at) AS latest_sent_at
            FROM 
            tbl_messages
            WHERE 
            sender_id = $1 OR recipient_id = $1
            GROUP BY 
            thread_id
        )
        SELECT 
            m.thread_id,
            m.sender_id,
            CONCAT(sender.username, ' ', sender.userlname) AS sender_name,
            m.recipient_id,
            CONCAT(recipient.username, ' ', recipient.userlname) AS recipient_name,
            m.subject,
            m.message_text,
            m.sent_at,
            m.is_read
        FROM 
            tbl_messages m
        JOIN 
            latest_messages lm ON m.thread_id = lm.thread_id AND m.sent_at = lm.latest_sent_at
        JOIN 
            tbl_users sender ON m.sender_id = sender.user_id
        JOIN 
            tbl_users recipient ON m.recipient_id = recipient.user_id
        WHERE 
            m.sender_id = $1 OR m.recipient_id = $1
        ORDER BY 
            m.sent_at DESC  LIMIT 1
      `,
      [userId]
    );

    const rows = messagesResult.rows;

    // Return the messages along with a 200 OK response
    return NextResponse.json({ rows }, { status: 200 });
  } catch (error) {
    // Handle any errors that occur during the execution
    console.error("Error fetching data:", error);
    await logAudit(
      sesData.sessiondata.email,
      "SELECT messages by user_id",
      false,
      error.message
    );
    return NextResponse.json(
      { error: "Internal Server Error" },
      { status: 500 }
    );
  }
}
