"use server";
import { NextResponse } from "next/server";
import { pool } from "./../../../config/db";
import fs from "fs";
import { pipeline } from "stream/promises";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function PUT(request) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  try {
    const formData = await request.formData();
    const dbserviceId = formData.get("dbserviceId");
    const dbserviceUserId = formData.get("dbserviceUserId");
    const dbserviceName = formData.get("dbserviceName");
    const serviceworkSetUp = formData.get("serviceworkSetUp");
    const serviceWorkPer = formData.get("serviceWorkPer");
    const dbservicerate = formData.get("dbservicerate");
    const dbsnewerviceDescription = formData.get("dbsnewerviceDescription");

    let productDirPath;
    const directoryPath = `./public/photos/services/${dbserviceUserId}`;
    productDirPath = `${directoryPath}/${dbserviceId}`; // Assign value to productDirPath

    const results = await pool.query(
      `UPDATE tbl_services
       SET service_name = $1,
           service_description = $2,
           service_rate = $3,
           service_work_setup = $4,
           service_per = $5
       WHERE service_id = $6;`,
      [
        dbserviceName,
        dbsnewerviceDescription,
        dbservicerate,
        serviceworkSetUp,
        serviceWorkPer,
        dbserviceId,
      ]
    );

    const newFileName = new Date()
      .toISOString()
      .replace(/:/g, "-")
      .replace(/\..+/, "")
      .replace("T", "_");

    if (results.rowCount >= 1) {
      const coverFiles = formData.getAll("coverfile[]");
      if (coverFiles.length > 0) {
        for (let i = 0; i < coverFiles.length; i++) {
          const coverFileName = `cover_${newFileName + i}.jpg`;
          const filePath = `${productDirPath}/${coverFileName}`;
          await pipeline(
            coverFiles[i].stream(),
            fs.createWriteStream(filePath)
          );
          const results_cover = await pool.query(
            `INSERT INTO tbl_service_cover (service_cover, service_id)
                VALUES ($1, $2)`,
            [filePath, dbserviceId]
          );
          if (results_cover.rowCount >= 1) {
            await logAudit(
              sesData.sessiondata.email,
              "INSERT INTO tbl_service_cover",
              true,
              "Succesfully save Service cover.."
            );

            return NextResponse.json(
              {
                success: true,
              },
              { status: 200 },
              { message: "Data Successfully Updated" }
            );
          } else {
            await logAudit(
              sesData.sessiondata.email,
              "INSERT INTO tbl_service_cover",
              false,
              "Error on saving service cover.."
            );
            return NextResponse.json(
              {
                success: false,
              },
              { status: 404 },
              { message: "Error on saving service cover.." }
            );
          }
        }
      } else {
        await logAudit(
          sesData.sessiondata.email,
          "INSERT INTO tbl_service_cover",
          true,
          "Data Successfully Updated"
        );
        return NextResponse.json(
          {
            success: true,
          },
          { status: 200 },
          { message: "Data Successfully Updated" }
        );
      }
    } else {
      await logAudit(
        sesData.sessiondata.email,
        "INSERT INTO tbl_service_cover",
        false,
        "User not found or no data updated"
      );
      return NextResponse.json(
        {
          success: false,
        },
        { status: 404 },
        { message: "User not found or no data updated" }
      );
    }
  } catch (error) {
    await logAudit(
      sesData.sessiondata.email,
      "INSERT INTO tbl_service_cover",
      false,
      error.message
    );
    return NextResponse.json(
      {
        success: false,
      },
      { status: 500 },
      { message: error.message }
    );
  }
}
