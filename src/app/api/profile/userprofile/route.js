"use server";
import { NextResponse } from "next/server";
import { pool } from "./../../../config/db";
import { logAudit } from "@/app/lib/logAudit";
import { validateSession } from "@/app/lib/sessionUtils";
const { cookies } = require("next/headers");
import { decrypt } from "./../../jwt";

export async function POST(request) {
  const res = await request.json();
  const {
    username,
    userlname,
    userprov,
    usermuni,
    useremail,
    userskills,
    userheadline,
    userabout,
    userprofilepic,
  } = res;

  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  try {
    // Insert data into
    const results = await pool.query(
      `INSERT INTO tbl_users (username, userlname, userprov, usermuni,useremail, userskills, userheadline, userabout, userprofilepic, user_link)
        VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,)`,
      [
        username,
        userlname,
        userprov,
        usermuni,
        useremail,
        userskills,
        userheadline,
        userabout,
        userprofilepic,
      ]
    );

    if (results.rowCount >= 1) {
      await logAudit(
        sesData.sessiondata.email,
        "INSERT",
        true,
        "Data successfully saved"
      );
      return NextResponse.json(
        {
          success: true,
        },
        { status: 200 },
        { message: "Data Successfully Saved" }
      );
    }
  } catch (error) {
    await logAudit(sesData.sessiondata.email, "INSERT", false, error.message);
    return NextResponse.json(
      {
        success: false,
      },
      { status: 401 },
      { message: error }
    );
  }
}

export async function GET(request) {
  const session = cookies().get("session")?.value;
  if (!session) {
    const rows = [];
    return NextResponse.json(
      { rows },
      {
        success: false,
        message: "No Content.",
      },
      { status: 204 }
    );
  }

  const sesData = await decrypt(session);

  // // Session Validation
  // const sessionDataOrResponse = await validateSession();
  // // If the result is a NextResponse, return it (indicating an error)
  // if (sessionDataOrResponse instanceof NextResponse) {
  //   return sessionDataOrResponse;
  // }
  // // If session data is valid, proceed
  // const sesData = sessionDataOrResponse;
  // // End Session Validation
  try {
    // If session is found, proceed with your database query
    const result = await pool.query(
      `SELECT * FROM tbl_users WHERE useremail = $1`,
      [sesData.sessiondata.email]
    );
    const rows = result.rows;
    if (result.rowCount > 0) {
      return NextResponse.json(
        { rows },
        {
          success: true,
        },
        { status: 200 }
      );
    } else {
      return NextResponse.json(
        { rows },
        {
          success: false,
          message: "No Content.",
        },
        { status: 204 }
      );
    }
  } catch (error) {
    const rows = [];
    return NextResponse.json(
      { rows },
      {
        success: false,
        message: "No Content.",
      },
      { status: 204 }
    );
  }
}

export async function PUT(request) {
  const res = await request.json();
  const {
    username,
    userlname,
    userprov,
    usermuni,
    useremail,
    userskills,
    userheadline,
    userabout,
    userLink,
  } = res;

  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  try {
    // Update data in tbl_users
    const results = await pool.query(
      `UPDATE tbl_users
       SET username = $1,
           userlname = $2,
           userprov = $3,
           usermuni = $4,
           userskills = $5,
           userheadline = $6,
           userabout = $7,
           user_link = $8,
           dateupdated = CURRENT_TIMESTAMP
       WHERE useremail = $9`,
      [
        username,
        userlname,
        userprov,
        usermuni,
        userskills,
        userheadline,
        userabout,
        userLink,
        useremail,
      ]
    );

    if (results.rowCount >= 1) {
      await logAudit(
        sesData.sessiondata.email,
        "PUT",
        true,
        "Data Successfully Updated"
      );
      return NextResponse.json(
        {
          success: true,
        },
        { status: 200 },
        { message: "Data Successfully Updated" }
      );
    } else {
      await logAudit(
        sesData.sessiondata.email,
        "PUT",
        false,
        "User not found or no data updated"
      );
      return NextResponse.json(
        {
          success: false,
        },
        { status: 404 },
        { message: "User not found or no data updated" }
      );
    }
  } catch (error) {
    await logAudit(sesData.sessiondata.email, "PUT", false, error.message);
    return NextResponse.json(
      {
        success: false,
      },
      { status: 500 },
      { message: error.message }
    );
  }
}
