"use server";
import { NextResponse } from "next/server";
import { pool } from "../../../config/db";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function GET(request) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  try {
    // If session is found, proceed with your database query
    const result = await pool.query(`SELECT * FROM refprovince Order by id`);
    const rows = result.rows;

    // Return the data along with a 200 OK response
    return NextResponse.json({ rows }, { status: 200 });
  } catch (error) {
    // Handle any errors that occur during the execution
    await logAudit(
      sesData.sessiondata.email,
      "SELECT provinces - profile/provinces",
      false,
      error.message
    );
    return NextResponse.json(
      { error: "Internal Server Error" },
      { status: 500 }
    );
  }
}
