"use server";

import { NextResponse } from "next/server";
import { pool } from "../../../config/db";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function GET(request) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  // const sesData = sessionDataOrResponse;
  // End Session Validation

  try {
    // Proceed with the database query if session is valid
    const result = await pool.query("SELECT * FROM refcitymun ORDER BY id");
    const rows = result.rows;

    // Return the data along with a 200 OK response
    return NextResponse.json({ rows }, { status: 200 });
  } catch (error) {
    await logAudit(
      sesData.sessiondata.email,
      "SELECT refcitymun - profile/municipality",
      false,
      error.message
    );
    return NextResponse.json(
      {
        success: false,
        message: "Internal Server Error",
      },
      { status: 500 }
    );
  }
}
