import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";
import fs from "fs";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function DELETE(request, { params }) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  const product_id = params.product_id;
  try {
    // Perform a SELECT query to retrieve product data before deleting
    const selectResult = await pool.query(
      `SELECT user_id FROM tbl_product WHERE product_id = $1`,
      [product_id]
    );

    // Extract the user_id from the result
    const user_id = selectResult.rows[0]?.user_id;

    // Check if the SELECT query retrieved valid data
    if (!user_id) {
      await logAudit(
        sesData.sessiondata.email,
        "IS user_id true - profile/deleteproducts/[product_id]",
        false,
        "user_id does not exists"
      );
      throw new Error("Product not found");
    }

    // Perform the DELETE query
    const deleteResult = await pool.query(
      `DELETE FROM tbl_product WHERE product_id = $1`,
      [product_id]
    );

    // Check if the delete query was successful
    if (deleteResult.rowCount > 0) {
      // If successful, proceed with directory deletion

      // Delete the directory associated with the product
      const directoryPath = `./public/photos/${user_id}/${product_id}`;
      if (fs.existsSync(directoryPath)) {
        fs.rmdirSync(directoryPath, { recursive: true });

        await logAudit(
          sesData.sessiondata.email,
          "DELETE product - profile/deleteproducts/[product_id]",
          true,
          `Deleted directory: ${directoryPath}`
        );
      }
    }

    // Return success response

    await logAudit(
      sesData.sessiondata.email,
      "DELETE product file - profile/deleteproducts/[product_id]",
      true,
      `Product Deleted`
    );
    return NextResponse.json(
      {
        success: true,
        message: "Product Deleted",
      },
      { status: 200 }
    );
  } catch (error) {
    await logAudit(
      sesData.sessiondata.email,
      "DELETE product file - profile/deleteproducts/[product_id]",
      true,
      error.message
    );
    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error deleting product: " + error.message,
      },
      { status: 500 }
    );
  }
}
