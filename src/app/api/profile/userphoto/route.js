import fs from "fs";
import { pipeline } from "stream/promises";
import { NextResponse } from "next/server";
import { pool } from "../../../config/db";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function POST(request) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  try {
    const formData = await request.formData();
    const file = formData.get("photo");
    const userId = formData.get("userid");
    if (!file) {
      throw new Error("No file found in the request.");
    }

    // Define the new file name
    const newFileName = "avatar.jpg";

    const directoryPath = `./public/photos/${userId}`;
    const filePath = `${directoryPath}/${newFileName}`;
    const filePathDb = `/photos/${userId}/${newFileName}`;

    // Create the directory if it doesn't exist
    if (!fs.existsSync(directoryPath)) {
      fs.mkdirSync(directoryPath, { recursive: true });
    }
    // Using pipeline to efficiently pipe data from readable stream to writable stream
    await pipeline(file.stream(), fs.createWriteStream(filePath));

    // Update userprofilepic field in the database
    const result = await pool.query(
      `UPDATE tbl_users SET userprofilepic = $1 WHERE user_id = $2`,
      [filePathDb, userId]
    );

    if (result.rowCount >= 1) {
      await logAudit(
        sesData.sessiondata.email,
        "UPDATE tbl_users ",
        true,
        "Data successfully updated"
      );
      return NextResponse.json(
        {
          success: true,
        },
        { status: 200 },
        { message: "Data successfully updated" }
      );
    } else {
      await logAudit(
        sesData.sessiondata.email,
        "UPDATE tbl_users ",
        false,
        "Failed to update userprofilepic field."
      );
      throw new Error("Failed to update userprofilepic field.");
    }
  } catch (error) {
    console.error("Error uploading file:", error);
    await logAudit(
      sesData.sessiondata.email,
      "UPDATE tbl_users ",
      false,
      error.message
    );
    return NextResponse.json(
      {
        success: false,
      },
      { status: 401 },
      { message: "Error uploading file:", error }
    );
  }
}
