import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";
import fs from "fs";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function DELETE(request, { params }) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  const product_f_id = params.product_f_id;

  try {
    // Perform a SELECT query to retrieve product data before deleting
    const selectResult = await pool.query(
      `SELECT product_file FROM tbl_product_file WHERE product_f_id = $1`,
      [product_f_id]
    );

    // Extract the user_id from the result
    const product_file = selectResult.rows[0]?.product_file;

    // Check if the SELECT query retrieved valid data
    if (!product_file) {
      await logAudit(
        sesData.sessiondata.email,
        "IS product file exists true - profile/deleteproductfile/[product_f_id]",
        false,
        "Product file does not exists"
      );
      throw new Error("Product not found");
    }

    // Perform the DELETE query
    const deleteResult = await pool.query(
      `DELETE FROM tbl_product_file WHERE product_f_id = $1`,
      [product_f_id]
    );

    // Check if the delete query was successful
    if (deleteResult.rowCount > 0) {
      // If successful, proceed with directory deletion
      // Delete the directory associated with the product
      if (fs.existsSync(product_file)) {
        fs.unlinkSync(product_file);
        await logAudit(
          sesData.sessiondata.email,
          "DELETE product file - profile/deleteproductfile/[product_f_id]",
          true,
          `Deleted file: ${product_file}`
        );
        // Return success response
        return NextResponse.json(
          {
            success: true,
            message: "Product File Deleted",
          },
          { status: 200 }
        );
      } else {
        await logAudit(
          sesData.sessiondata.email,
          "DELETE product file - profile/deleteproductfile/[product_f_id]",
          false,
          `Error deleting product file`
        );
        return NextResponse.json(
          {
            success: false,
            message: "Error deleting product file",
          },
          { status: 401 }
        );
      }
    }
  } catch (error) {
    // Return a failure response with the error details
    await logAudit(
      sesData.sessiondata.email,
      "DELETE product file - profile/deleteproductfile/[product_f_id]",
      false,
      error.message
    );
    return NextResponse.json(
      {
        success: false,
        message: "Error deleting product: " + error.message,
      },
      { status: 500 }
    );
  }
}
