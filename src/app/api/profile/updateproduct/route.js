"use server";
import { NextResponse } from "next/server";
import { pool } from "../../../config/db";
import fs from "fs";
import { pipeline } from "stream/promises";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function PUT(request) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  try {
    const formData = await request.formData();
    const dbproductId = formData.get("dbproductId");
    const dbproductName = formData.get("dbproductName");
    const dbproductDescription = formData.get("dbproductDescription");
    const dbprice = formData.get("dbprice");
    const dbuserId = formData.get("dbuserId");
    const dbproductFileId = formData.get("dbproductFileId");
    const productFile = formData.get("productfile");

    let productDirPath;
    const directoryPath = `./public/photos/${dbuserId}`;
    productDirPath = `${directoryPath}/${dbproductId}`; // Assign value to productDirPath

    const results = await pool.query(
      `UPDATE tbl_product
       SET product_name = $1,
           product_description = $2,
           price = $3
       WHERE product_id = $4`,
      [dbproductName, dbproductDescription, dbprice, dbproductId]
    );

    const newFileName = new Date()
      .toISOString()
      .replace(/:/g, "-")
      .replace(/\..+/, "")
      .replace("T", "_");

    if (results.rowCount >= 1) {
      const coverFiles = formData.getAll("coverfile[]");
      if (coverFiles.length > 0) {
        for (let i = 0; i < coverFiles.length; i++) {
          const coverFileName = `cover_${newFileName + i}.jpg`;
          const filePath = `${productDirPath}/${coverFileName}`;
          await pipeline(
            coverFiles[i].stream(),
            fs.createWriteStream(filePath)
          );
          const results_cover = await pool.query(
            `INSERT INTO tbl_product_cover (product_cover, product_id)
                VALUES ($1, $2)`,
            [filePath, dbproductId]
          );
          if (results_cover.rowCount >= 1) {
            await logAudit(
              sesData.sessiondata.email,
              "INSERT INTO tbl_product_cover",
              true,
              "Succesfully save product cover.."
            );
            return NextResponse.json(
              {
                success: true,
              },
              { status: 200 },
              { message: "Succesfully saved product cover" }
            );
          } else {
            await logAudit(
              sesData.sessiondata.email,
              "INSERT INTO tbl_product_cover",
              false,
              "Error on saving service cover.."
            );
            return NextResponse.json(
              {
                success: false,
              },
              { status: 404 },
              { message: "Error on saving service cover.." }
            );
          }
        }
      }

      if (productFile) {
        // Perform a SELECT query to retrieve product data before deleting
        const selectResult = await pool.query(
          `SELECT product_file FROM tbl_product_file WHERE product_f_id = $1`,
          [dbproductFileId]
        );

        // Extract the user_id from the result
        const product_file = selectResult.rows[0]?.product_file;

        // Check if the SELECT query retrieved valid data
        if (!product_file) {
          throw new Error("Product not found");
        }

        // Perform the DELETE query
        const deleteResult = await pool.query(
          `DELETE FROM tbl_product_file WHERE product_f_id = $1`,
          [dbproductFileId]
        );

        // Check if the delete query was successful
        if (deleteResult.rowCount > 0) {
          // If successful, proceed with directory deletion
          // Delete the directory associated with the product
          if (fs.existsSync(product_file)) {
            fs.unlinkSync(product_file);
            console.log(`Deleted file: ${product_file}`);
            // Return success response
            //

            const productFileName = productFile.name; // Get the original filename
            const product = `${productDirPath}/${productFileName}`;
            await pipeline(
              productFile.stream(),
              fs.createWriteStream(`${productDirPath}/${productFileName}`)
            );
            const results_file = await pool.query(
              `INSERT INTO tbl_product_file (product_file, product_id)
                VALUES ($1, $2)`,
              [product, dbproductId]
            );
            if (results_file.rowCount >= 1) {
              console.log("Succesfully save product file..");
            } else {
              await logAudit(
                sesData.sessiondata.email,
                "INSERT INTO tbl_product_file",
                false,
                "Error Updating"
              );
              return NextResponse.json(
                {
                  success: false,
                  message: "Error Updating",
                },
                { status: 401 }
              );
            }
            //
            //
          } else {
            await logAudit(
              sesData.sessiondata.email,
              "INSERT INTO tbl_product_file",
              false,
              "Error Updating"
            );
            return NextResponse.json(
              {
                success: false,
                message: "Error Updating",
              },
              { status: 401 }
            );
          }
        }
      }
      await logAudit(
        sesData.sessiondata.email,
        "INSERT INTO tbl_product_file",
        true,
        "Data Successfully Updated"
      );
      return NextResponse.json(
        {
          success: true,
        },
        { status: 200 },
        { message: "Data Successfully Updated" }
      );
    } else {
      await logAudit(
        sesData.sessiondata.email,
        "INSERT INTO tbl_product_file",
        false,
        "User not found or no data updated"
      );
      return NextResponse.json(
        {
          success: false,
        },
        { status: 404 },
        { message: "User not found or no data updated" }
      );
    }
  } catch (error) {
    await logAudit(
      sesData.sessiondata.email,
      "INSERT INTO tbl_product_file",
      false,
      error.message
    );
    return NextResponse.json(
      {
        success: false,
      },
      { status: 500 },
      { message: error.message }
    );
  }
}
