import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";
import fs from "fs";
import path from "path";
import { validateSession } from "@/app/lib/sessionUtils";
import { logAudit } from "@/app/lib/logAudit";

export async function GET(request, { params }) {
  // Session Validation
  const sessionDataOrResponse = await validateSession();
  // If the result is a NextResponse, return it (indicating an error)
  if (sessionDataOrResponse instanceof NextResponse) {
    return sessionDataOrResponse;
  }
  // If session data is valid, proceed
  const sesData = sessionDataOrResponse;
  // End Session Validation

  const product_id = params.product_id;

  if (!product_id) {
    await logAudit(
      sesData.sessiondata.email,
      "IS product_id true",
      false,
      "Product ID is required"
    );
    return NextResponse.json(
      { success: false, message: "Product ID is required" },
      { status: 400 }
    );
  }

  try {
    // Perform a SELECT query to retrieve product file data
    const result = await pool.query(
      `SELECT product_file FROM tbl_product_file WHERE product_f_id = $1`,
      [product_id]
    );

    if (result.rowCount === 0) {
      await logAudit(
        sesData.sessiondata.email,
        "SELECT product_file",
        false,
        "File not found for the given product ID"
      );
      return NextResponse.json(
        { success: false, message: "File not found for the given product ID" },
        { status: 404 }
      );
    }

    const productFile = result.rows[0].product_file;
    const filePath = path.join(process.cwd(), productFile);

    // Check if the file exists
    if (!fs.existsSync(filePath)) {
      await logAudit(
        sesData.sessiondata.email,
        "IS file_path exist",
        false,
        "File not found"
      );
      return NextResponse.json(
        { success: false, message: "File not found" },
        { status: 404 }
      );
    }

    // Create a file stream
    const fileStream = fs.createReadStream(filePath);

    const headers = new Headers({
      "Content-Disposition": `attachment; filename=${path.basename(filePath)}`,
      "Content-Type": "application/octet-stream",
    });

    return new Response(fileStream, {
      headers,
    });
  } catch (error) {
    await logAudit(
      sesData.sessiondata.email,
      "Error on retrieving data",
      error.message
    );
    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error on retrieving data: " + error.message,
      },
      { status: 500 }
    );
  }
}
