import NextAuth from "next-auth";
import GoogleProvider from "next-auth/providers/google";

const handler = NextAuth({
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_ID,
      clientSecret: process.env.GOOGLE_SECRET,
      state: true,
    }),
  ],

  secret: "SkA+E82g87Y6HhAolJxFn1rW7ocJd3uYmCjcY1Biagk=",

  session: {
    jwt: false,
    maxAge: 30 * 24 * 60 * 60,
    strategy: "jwt",
    // Choose how you want to save the user session.
    // The default is `"jwt"`, an encrypted JWT (JWE) stored in the session cookie.
    // If you use an `adapter` however, we default it to `"database"` instead.
    // You can still force a JWT session by explicitly defining `"jwt"`.
    // When using `"database"`, the session cookie will only contain a `sessionToken` value,
    // which is used to look up the session in the database.

    // Seconds - How long until an idle session expires and is no longer valid.

    // Seconds - Throttle how frequently to write to database to extend a session.
    // Use it to limit write operations. Set to 0 to always update the database.
    // Note: This option is ignored if using JSON Web Tokens
    updateAge: 24 * 60 * 60, // 24 hours

    // The session token is usually either a random UUID or string, however if you
    // need a more customized session token string, you can define your own generate function.
    generateSessionToken: () => {
      return randomUUID?.() ?? randomBytes(32).toString("hex");
    },
  },
  cookies: {
    secure: true,
    maxAge: 365 * 24 * 60 * 60, // 1 year
    sameSite: "lax",
    name: "_nextauth.session-token", // Use the default cookie name
  },
});

export { handler as GET, handler as POST };
