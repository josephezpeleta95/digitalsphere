import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";

export async function GET(request, { params }) {
  const product_id = params.product_id;
  try {
    // Perform a SELECT query to retrieve product data before deleting
    const result = await pool.query(
      `SELECT 
          p.*,
          u.username,
          u.userlname,
          u.userprofilepic,
          COALESCE(s.total_sold, 0) AS total_sold,
          COALESCE(l.total_likes, 0) AS total_likes
      FROM 
          tbl_product p
      LEFT JOIN tbl_users u ON p.user_id = u.user_id
      LEFT JOIN (
          SELECT 
              product_id, 
              SUM(quantity) AS total_sold
          FROM 
              tbl_sold
          GROUP BY 
              product_id
      ) s ON p.product_id = s.product_id
      LEFT JOIN (
          SELECT 
              product_id, 
              COUNT(*) AS total_likes
          FROM 
              tbl_likes
          GROUP BY 
              product_id
      ) l ON p.product_id = l.product_id
      WHERE 
          p.product_id = $1;`,
      [product_id]
    );

    const rows = result.rows;
    return NextResponse.json({ rows }, { status: 200 });
  } catch (error) {
    console.error("Error on retrieving data", error);

    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error on retrieving data:" + error.message,
      },
      { status: 500 }
    );
  }
}
