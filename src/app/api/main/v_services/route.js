"use server";
import { NextResponse } from "next/server";
import { pool } from "../../../config/db";

export async function GET(request) {
  try {
    const result = await pool.query(`
    WITH ServiceDetails AS (
                SELECT 
                    s.service_id,
                    s.service_name,
                    s.service_description,
                    s.service_rate,
                    s.service_work_setup,
                    s.service_per,
                    s.user_id,
                    (
                        SELECT 
                            json_agg(json_build_object('service_c_id', sc.service_c_id, 'service_cover', sc.service_cover))
                        FROM 
                            tbl_service_cover sc
                        WHERE 
                            sc.service_id = s.service_id
                    ) AS covers
                FROM 
                    tbl_services s
            )
            SELECT 
                u.user_id,
                u.username,
                u.userlname,
                u.userprofilepic,
                sd.service_id,
                sd.service_name,
                sd.service_description,
                sd.service_rate,
                sd.service_work_setup,
                sd.service_per,
                sd.covers
            FROM 
                tbl_users u
            LEFT JOIN 
                ServiceDetails sd ON u.user_id = sd.user_id`);
    const rows = result.rows;

    // Return the data along with a 200 OK response
    return NextResponse.json({ rows }, { status: 200 });
  } catch (error) {
    // Handle any errors that occur during the execution
    console.error("Error fetching data:", error);
    return NextResponse.json(
      { error: "Internal Server Error" },
      { status: 500 }
    );
  }
}
