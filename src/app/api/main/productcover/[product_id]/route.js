import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";

export async function GET(request, { params }) {
  const product_id = params.product_id;
  try {
    // Perform a SELECT query to retrieve product data before deleting
    const result = await pool.query(
      `SELECT * FROM tbl_product_cover WHERE product_id = $1`,
      [product_id]
    );

    const rows = result.rows;
    return NextResponse.json({ rows }, { status: 200 });
  } catch (error) {
    console.log(error.message);
    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error on retrieving data:" + error.message,
      },
      { status: 500 }
    );
  }
}
