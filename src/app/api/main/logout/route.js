import { NextResponse } from "next/server";
const { cookies } = require("next/headers");
import { decrypt } from "./../../jwt";
import { logAuthentication } from "@/app/lib/logAuthentication";

export async function POST(request) {
  const session = cookies().get("session")?.value;
  const valuesession = await decrypt(session);
  try {
    cookies().set("session", "", { expires: new Date(0), httpOnly: true });
    await logAuthentication(
      true,
      "Logout authentication successful",
      valuesession.sessiondata.email
    );
    return NextResponse.json(
      {
        success: true,
        message: "Logout Success.",
      },
      { status: 200 }
    );
  } catch (error) {
    console.error("Error during logout:", error);
    await logAuthentication(
      false,
      `Logout authentication failed ${error.message}`,
      valuesession.sessiondata.email
    );
    return NextResponse.json(
      {
        success: false,
        message: "Internal Server Error.",
      },
      { status: 500 }
    );
  }
}
