"use server";
import { NextResponse } from "next/server";
import { pool } from "../../../config/db";

export async function GET(request) {
  try {
    const result = await pool.query(`SELECT * FROM tbl_users`);
    const rows = result.rows;

    // Return the data along with a 200 OK response
    return NextResponse.json({ rows }, { status: 200 });
  } catch (error) {
    // Handle any errors that occur during the execution
    console.error("Error fetching data:", error);
    return NextResponse.json(
      { error: "Internal Server Error" },
      { status: 500 }
    );
  }
}
