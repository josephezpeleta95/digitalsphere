import { NextResponse } from "next/server";
import { pool } from "../../../config/db";

export async function POST(request) {
  const res = await request.json();
  var serviceId = res.serviceId;
  var userId = res.userId;
  var newReview = res.newReview;
  var newRating = res.newRating;

  try {
    const results = await pool.query(
      `INSERT INTO tbl_service_reviews (user_id, service_id, review_text, rating)
        VALUES ($1,$2,$3,$4)`,
      [userId, serviceId, newReview, newRating]
    );
    if (results.rowCount >= 1) {
      return NextResponse.json(
        {
          success: true,
        },
        { status: 200 }
      );
    } else {
      return NextResponse.json(
        {
          success: false,
        },
        { status: 401 }
      );
    }
  } catch (error) {
    console.log(error);
  }
}
