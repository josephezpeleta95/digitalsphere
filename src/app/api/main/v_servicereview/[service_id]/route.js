import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";

export async function GET(request, { params }) {
  const service_id = params.service_id;
  try {
    // Perform a SELECT query to retrieve product data before deleting
    const result = await pool.query(
      `
        SELECT 
        u.username,
        u.userlname,
        u.userprofilepic,
        sr.review_text,
        sr.rating
        FROM 
            tbl_users u
        JOIN 
            tbl_service_reviews sr ON u.user_id = sr.user_id
        JOIN 
            tbl_services s ON sr.service_id = s.service_id
        WHERE 
            s.service_id = $1;
    `,
      [service_id]
    );

    const rows = result.rows;
    return NextResponse.json({ rows }, { status: 200 });
  } catch (error) {
    console.error("Error on retrieving data", error);

    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error on retrieving data:" + error.message,
      },
      { status: 500 }
    );
  }
}
