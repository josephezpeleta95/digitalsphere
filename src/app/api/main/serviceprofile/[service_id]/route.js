import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";

export async function GET(request, { params }) {
  const service_id = params.service_id;
  try {
    // Perform a SELECT query to retrieve product data before deleting
    const result = await pool.query(
      `SELECT 
            s.*,
              (
                    SELECT 
                        json_agg(json_build_object('service_cover', sc.service_cover, 'service_cover_id',  sc.service_c_id))
                    FROM 
                        tbl_service_cover sc
                    WHERE 
                        s.service_id = sc.service_id
                    LIMIT 1
                ) AS covers,
            u.username,
            u.userlname,
            u.useremail,
            u.userskills,
            u.userheadline,
            u.userprofilepic,
            u.datecreated,
            u.status,
            u.dateupdated
        FROM 
            tbl_services s
        LEFT JOIN 
            tbl_users u ON s.user_id = u.user_id
            WHERE s.service_id =$1
        GROUP BY 
            s.service_id, u.user_id;`,
      [service_id]
    );

    const rows = result.rows;
    return NextResponse.json({ rows }, { status: 200 });
  } catch (error) {
    console.log(error.message);
    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error on retrieving data:" + error.message,
      },
      { status: 500 }
    );
  }
}
