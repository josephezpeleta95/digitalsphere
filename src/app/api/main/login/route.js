import { NextResponse } from "next/server";
import { cookies } from "next/headers";
import { logAuthentication } from "@/app/lib/logAuthentication";

const { SignJWT } = require("jose");
const secretKey = process.env.JWT_KEY;
const key = new TextEncoder().encode(secretKey);
async function encrypt(payload) {
  return new SignJWT(payload)
    .setProtectedHeader({ alg: "HS256" })
    .setIssuedAt()
    .sign(key);
}

export async function POST(request) {
  const res = await request.json();
  const name = res.name;
  const email = res.email;

  const sessiondata = {
    email: email,
    name: name,
  };

  try {
    const expires = new Date(Date.now() + 1000 * 60 * 60 * 24 * 30);
    const session = await encrypt({ sessiondata, expires });
    cookies().set("session", session, { expires, httpOnly: true });

    await logAuthentication(true, "Login authentication successful", email);
    return NextResponse.json(
      {
        success: true,
        message: "Update Success.",
      },
      { status: 200 }
    );
  } catch (error) {
    console.error("Error during login:", error);

    // Log authentication failure
    await logAuthentication(
      false,
      `Login authentication failed ${error.message}`,
      email
    );

    return NextResponse.json(
      {
        success: false,
        message: "Internal Server Error.",
      },
      { status: 500 }
    );
  }
}
