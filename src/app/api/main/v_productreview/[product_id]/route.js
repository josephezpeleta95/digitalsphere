import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";

export async function GET(request, { params }) {
  const product_id = params.product_id;
  try {
    // Perform a SELECT query to retrieve product data before deleting
    const result = await pool.query(
      `SELECT
        u.username,
        u.userprofilepic,
        u.userlname,
        p.product_name,
        p.price,
        r.review_text,
        r.rating
    FROM
        tbl_users u
    JOIN
        tbl_product_reviews r ON u.user_id = r.user_id
    JOIN
        tbl_product p ON r.product_id = p.product_id
    WHERE
        p.product_id = $1;`,
      [product_id]
    );

    const rows = result.rows;
    return NextResponse.json({ rows }, { status: 200 });
  } catch (error) {
    console.error("Error on retrieving data", error);

    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error on retrieving data:" + error.message,
      },
      { status: 500 }
    );
  }
}
