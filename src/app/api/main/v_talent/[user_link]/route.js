"use server";
import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";

export async function GET(request, { params }) {
  const user_link = params.user_link;
  try {
    const result = await pool.query(
      `
            WITH ProductDetails AS (
                SELECT 
                    p.product_id,
                    p.product_name,
                    p.product_description,
                    p.price,
                    p.date_added,
                    p.user_id,
                    (
                        SELECT 
                            json_agg(json_build_object('product_c_id', pc.product_c_id, 'product_cover', pc.product_cover))
                        FROM 
                            tbl_product_cover pc
                        WHERE 
                            pc.product_id = p.product_id
                        LIMIT 1
                    ) AS covers,
                    (
                        SELECT 
                            json_agg(json_build_object('product_f_id', pf.product_f_id, 'product_file', pf.product_file))
                        FROM 
                            tbl_product_file pf
                        WHERE 
                            pf.product_id = p.product_id
                    ) AS files
                FROM 
                    tbl_product p
            )
            SELECT 
                u.user_id,
                u.username,
                u.userlname,
                u.userprov,
                u.usermuni,
                u.useremail,
                u.userskills,
                u.userheadline,
                u.userabout,
                u.userprofilepic,
                u.datecreated,
                u.status,
                u.dateupdated,
                u.user_link,
                pd.product_id,
                pd.product_name,
                pd.product_description,
                pd.price,
                pd.date_added,
                pd.covers,
                pd.files,
                (
                    SELECT 
                        json_agg(json_build_object('sold_id', s.sold_id, 'quantity', s.quantity, 'price', s.price, 'date_sold', s.date_sold))
                    FROM 
                        tbl_sold s
                    WHERE 
                        s.product_id = pd.product_id
                ) AS sold_info,
                (
                    SELECT 
                        SUM(s.quantity)  -- Sum of quantity of sold items
                    FROM 
                        tbl_sold s
                    WHERE 
                        s.product_id = pd.product_id
                ) AS total_sold,  -- Total quantity of sold items
                (
                    SELECT 
                        COUNT(*)
                    FROM 
                        tbl_likes l
                    WHERE 
                        l.product_id = pd.product_id
                ) AS total_likes
            FROM 
                tbl_users u
            LEFT JOIN 
                ProductDetails pd ON u.user_id = pd.user_id
            WHERE
                u.user_link = $1
        `,
      [user_link]
    );
    const rows = result.rows;

    // Return the data along with a 200 OK response
    return NextResponse.json({ rows }, { status: 200 });
  } catch (error) {
    // Handle any errors that occur during the execution
    console.error("Error fetching data:", error);
    return NextResponse.json(
      { error: "Internal Server Error" },
      { status: 500 }
    );
  }
}
