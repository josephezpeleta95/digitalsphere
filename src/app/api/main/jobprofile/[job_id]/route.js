import { NextResponse } from "next/server";
import { pool } from "../../../../config/db";
export async function GET(request, { params }) {
  const job_id = params.job_id;
  try {
    // Perform a SELECT query to retrieve product data before deleting
    const result = await pool.query(
      `SELECT 
        j.job_id,
        j.title,
        j.description,
        j.location AS job_location,
        j.date_posted,
        j.salary_range,
        j.closed_date,
        j.setup,
        j.skills,
        j.work_arrangement,
        c.company_id,
        c.company_name,
        c.location AS company_location,
        c.company_email,
        c.contact_number,
        c.contact_person,
        c.website,
        c.industry,
        c.size
    FROM 
        tbl_job_listings j
    INNER JOIN 
        tbl_company_profile c ON j.company_id = c.company_id  WHERE j.job_id= $1;`,
      [job_id]
    );

    const rows = result.rows;
    return NextResponse.json({ rows }, { status: 200 });
  } catch (error) {
    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error on retrieving data:" + error.message,
      },
      { status: 500 }
    );
  }
}
