import { NextResponse } from "next/server";
import { pool } from "../../../config/db";
import fs from "fs";

export async function DELETE(request, { params }) {
  const product_c_id = params.product_c_id;

  try {
    // Perform a SELECT query to retrieve product data before deleting
    const selectResult = await pool.query(
      `SELECT product_cover FROM tbl_product_cover WHERE product_c_id = $1`,
      [product_c_id]
    );

    // Extract the user_id from the result
    const product_cover = selectResult.rows[0]?.product_cover;

    // Check if the SELECT query retrieved valid data
    if (!product_cover) {
      throw new Error("Product not found");
    }

    // Perform the DELETE query
    const deleteResult = await pool.query(
      `DELETE FROM tbl_product_cover WHERE product_c_id = $1`,
      [product_c_id]
    );

    // Check if the delete query was successful
    if (deleteResult.rowCount > 0) {
      // If successful, proceed with directory deletion
      // Delete the directory associated with the product
      if (fs.existsSync(product_cover)) {
        fs.unlinkSync(product_cover);
        console.log(`Deleted file: ${product_cover}`);
        // Return success response
        return NextResponse.json(
          {
            success: true,
            message: "Product Deleted",
          },
          { status: 200 }
        );
      } else {
        return NextResponse.json(
          {
            success: false,
            message: "Error deleting: " + error.message,
          },
          { status: 401 }
        );
      }
    }
  } catch (error) {
    console.error("Error deleting product:", error);
    // Return a failure response with the error details
    return NextResponse.json(
      {
        success: false,
        message: "Error deleting product: " + error.message,
      },
      { status: 500 }
    );
  }
}
