const { jwtVerify } = require("jose");

const secretKey = process.env.JWT_KEY;
const key = new TextEncoder().encode(secretKey);

async function decrypt(input) {
  const { payload } = await jwtVerify(input, key, {
    algorithms: ["HS256"],
  });
  return payload;
}

module.exports = { decrypt };
