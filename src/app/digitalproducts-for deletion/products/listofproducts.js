export default function Products() {
  const product_profile = [
    {
      name: "Daniel Ricciardo",
      image: "/images/digitalproducts/flyers.jpg",
      talentimage: "/images/profile/profile-1.jpg",
      price: "121.00",
      sold: "23",
      product_name: "Flyers and Cards",
      rating: "54",
      product_desciption: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
      Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa.
Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur  Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur  Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur  Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur  Vestibulum lacinia arcu eget nulla. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor.
Maecenas mattis. Sed convallis tristique sem. Proin ut ligula vel nunc egestas porttitor. Morbi lectus risus, iaculis vel, suscipit quis, luctus non, massa. Fusce ac turpis quis ligula lacinia aliquet. Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh.`,
    },
    {
      name: "Loue Castronuevo",
      image: "/images/digitalproducts/ebook.jpg",
      talentimage: "/images/profile/profile-2.jpg",
      price: "121.00",
      sold: "23",
      product_name: "E-book for Kids",
      rating: "54",
      product_desciption:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed accumsan justo non ligula consequat, id lobortis enim luctus. Nullam sed nulla eget nisi tempor commodo. Fusce vestibulum, velit sit amet tincidunt sollicitudin",
    },
    {
      name: "Loui Hamilton",
      image: "/images/digitalproducts/flyers.jpg",
      talentimage: "/images/profile/profile-3.jpg",
      price: "121.00",
      sold: "23",
      product_name: "Daily Budget Tracker",
      rating: "54",
      product_desciption:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed accumsan justo non ligula consequat, id lobortis enim luctus. Nullam sed nulla eget nisi tempor commodo. Fusce vestibulum, velit sit amet tincidunt sollicitudin",
    },
    {
      name: "Tonny Vetel",
      image: "/images/digitalproducts/flyers.jpg",
      talentimage: "/images/profile/profile-4.jpg",
      price: "121.00",
      sold: "23",
      product_name: "Logo Design",
      rating: "54",
      product_desciption:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    },
    {
      name: "Mila Jovovich",
      image: "/images/digitalproducts/flyers.jpg",
      talentimage: "/images/profile/profile-5.jpg",
      price: "121.00",
      sold: "23",
      product_name: "Web Design",
      rating: "54",
      product_desciption:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed accumsan justo non ligula consequat, id lobortis enim luctus. Nullam sed nulla eget nisi tempor commodo. Fusce vestibulum, velit sit amet tincidunt sollicitudin",
    },
    {
      name: "Kory Kong",
      image: "/images/digitalproducts/flyers.jpg",
      talentimage: "/images/profile/profile-6.jpg",
      price: "121.00",
      sold: "23",
      product_name: "Ultimate Wedding Planner Spreadsheet",
      rating: "54",
      product_desciption:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed accumsan justo non ligula consequat, id lobortis enim luctus. Nullam sed nulla eget nisi tempor commodo. Fusce vestibulum, velit sit amet tincidunt sollicitudin",
    },
    {
      name: "Luka Snatos",
      image: "/images/digitalproducts/flyers.jpg",
      talentimage: "/images/profile/profile-7.jpg",
      price: "121.00",
      sold: "23",
      product_name: "Instagram Stories Template",
      rating: "54",
      product_desciption:
        "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt malesuada tellus, nec ultricies ligula finibus non.</p><p>Nullam efficitur sapien ac leo consectetur, sed tristique dolor consectetur. Mauris nec diam nec magna condimentum fermentum.</p><p>Integer hendrerit justo sit amet elit euismod efficitur. Pellentesque lacinia justo sit amet est placerat, nec ultricies lacus rhoncus.</p>",
    },
    {
      name: "Luka Snatos",
      image: "/images/digitalproducts/flyers.jpg",
      talentimage: "/images/profile/profile-7.jpg",
      price: "121.00",
      sold: "23",
      product_name: "Instagram Stories Template",
      rating: "54",
      product_desciption:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed accumsan justo non ligula consequat, id lobortis enim luctus. Nullam sed nulla eget nisi tempor commodo. Fusce vestibulum, velit sit amet tincidunt sollicitudin",
    },
  ];

  const JobsCards = (
    { name, image, product_desciption, price, sold, product_name, rating },
    index
  ) => (
    <>
      <div className='w-10/12 px-5 py-1 border border-dashed  mr-5 mb-5'>
        <div className='w-full h-50 flex justify-between '>
          <div className='w-1/4 h-50 p-1 flex justify-between items-center transform transition-transform hover:scale-105 cursor-pointer'>
            <img
              src={image}
              alt={`Image Cayap${index}`}
              className='w-full h-full object-cover rounded-lg'
            />
          </div>
          <div className='w-3/4'>
            <div className='w-full flex items-start p-1'>
              <div className='w-full text-lg px-4 text-sky-700 font-semibold'>
                {product_name}
              </div>
            </div>
            <div className='w-full flex flex-grow  h-48 px-1 mt-2'>
              <div className='px-1 text-ellipsis overflow-hidden px-4 '>
                <p className='text-slate-500 text-xs'>
                  {truncateString(product_desciption, 700)}
                </p>
              </div>
            </div>
            <div className='w-full flex items-center px-1 pt-6'>
              <div className='w-3/12 text-center '>
                <p className='text-sky-700 text-sm font-bold'>P {price}</p>
              </div>
              <div className='w-3/12 text-center'>
                <p className='text-sky-700 text-sm font-bold'>Sold: {sold}</p>
              </div>
              <div className='w-3/12 text-center'>
                <p className='text-sky-700 text-sm font-bold'>Likes {rating}</p>
              </div>
              <div className='w-3/12 flex justify-end pr-4 mb-2'>
                <button className='bg-pink-500 rounded-full px-3 py-2 text-white text-xs'>
                  Buy now
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );

  function truncateString(str, maxLength) {
    if (str.length <= maxLength) {
      return str;
    } else {
      return str.slice(0, maxLength - 3) + "...";
    }
  }

  return (
    //   Header Navbar
    <div>
      <div className='bg-slate-50 w-full items-center flex justify-center'>
        <div className='w-11/12  mt-4'>
          <div className='w-full p-5 text-2xl justify-center flex'>
            <div className='w-10/12 flex'>
              <div className='w-5/12 items-center flex'>
                <h1 className='font-roboto leading-normal text-pink-500 font-normal'>
                  Digital Products
                </h1>
              </div>

              <div className='w-7/12 items-center flex'>
                <div class='relative  w-full'>
                  <input
                    class='rounded-lg w-full pl-8 pr-4 py-1 text-slate-500 text-lg border border-gray-300 focus:border-blue-500 focus:outline-none ml-2'
                    type='search'
                    placeholder='Search'></input>
                  <div class='absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none'>
                    <svg
                      xmlns='http://www.w3.org/2000/svg'
                      viewBox='0 0 24 24'
                      fill='currentColor'
                      className='w-6 h-6'>
                      <path d='M8.25 10.875a2.625 2.625 0 1 1 5.25 0 2.625 2.625 0 0 1-5.25 0Z' />
                      <path
                        fill-rule='evenodd'
                        d='M12 2.25c-5.385 0-9.75 4.365-9.75 9.75s4.365 9.75 9.75 9.75 9.75-4.365 9.75-9.75S17.385 2.25 12 2.25Zm-1.125 4.5a4.125 4.125 0 1 0 2.338 7.524l2.007 2.006a.75.75 0 1 0 1.06-1.06l-2.006-2.007a4.125 4.125 0 0 0-3.399-6.463Z'
                      />
                    </svg>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className='z-10 lg:w-full flex bg-slate-50 '>
            <div className='w-full flex justify-center flex-wrap items-center p-4 mb-10'>
              {/* Map products  */}

              {product_profile.map((product_profile, index) => (
                <JobsCards {...product_profile} key={index} />
              ))}
              {/*End  Map products  */}
            </div>
          </div>
          <div className='justify-end items-center flex mb-10 w-full'>
            <div className='flex justify-center mt-4 '>
              <nav
                className='relative z-0 inline-flex rounded-md shadow-sm -space-x-px'
                aria-label='Pagination'>
                <a
                  href='#'
                  className='relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50'>
                  Previous
                </a>
                <a
                  href='#'
                  className='relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50'>
                  1
                </a>
                <a
                  href='#'
                  className='relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50'>
                  2
                </a>
                <a
                  href='#'
                  className='relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50'>
                  3
                </a>
                <span className='relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700'>
                  ...
                </span>
                <a
                  href='#'
                  className='relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50'>
                  8
                </a>
                <a
                  href='#'
                  className='relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50'>
                  9
                </a>
                <a
                  href='#'
                  className='relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50'>
                  10
                </a>
                <a
                  href='#'
                  className='relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50'>
                  Next
                </a>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
