"use client";
import Header from "@header/header";
import Footer from "@footer/footer";
import TopFooter from "@topfooter/topfooter";
import DigitalProducts from "./products/listofproducts";
import { SessionProvider } from "next-auth/react";
export default function Products() {
  return (
    <main className='flex flex-col items-center justify-between bg-white'>
      <SessionProvider>
        <Header />
      </SessionProvider>
      <DigitalProducts />
      <TopFooter />
      <Footer />
    </main>
  );
}
