import { NextResponse } from "next/server";
import { cookies } from "next/headers";
import { decrypt } from "./../api/jwt"; // Adjust the path to where `decrypt` is defined

/**
 * Validate and decrypt the session cookie.
 * @returns {Object|NextResponse} Decrypted session data or a NextResponse object indicating unauthorized access.
 */
export async function validateSession() {
  // Get the session cookie
  const sessionCookie = cookies().get("session")?.value;

  // Check if session cookie is present
  if (!sessionCookie) {
    return NextResponse.json(
      {
        success: false,
        message: "Unauthorized access",
      },
      { status: 401 }
    );
  }

  // Decrypt session data
  const sesData = await decrypt(sessionCookie);

  // Check if decryption was successful
  if (!sesData) {
    return NextResponse.json(
      {
        success: false,
        message: "Unauthorized access",
      },
      { status: 401 }
    );
  }

  // Return decrypted session data
  return sesData;
}
