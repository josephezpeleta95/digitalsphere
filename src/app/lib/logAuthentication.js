// logAuthentication.js
import { pool } from "../config/db";

export async function logAuthentication(success, message, email) {
  const level = success ? "info" : "error";
  const timestamp = new Date().toISOString();
  const query = {
    text: "INSERT INTO authetication_logs (timestamp, level, message, email_address) VALUES ($1, $2, $3, $4)",
    values: [timestamp, level, message, email],
  };

  try {
    await pool.query(query);
  } catch (error) {
    console.error("Error inserting log into database:", error);
  }
}
