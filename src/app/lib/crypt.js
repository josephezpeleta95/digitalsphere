import crypto from "crypto";

const secret = "hg6TG6Foguggg8gbG88hHIH78";

const algorithm = "aes-256-cbc";
const key = crypto
  .createHash("sha256")
  .update(secret)
  .digest("base64")
  .substr(0, 32);

export const encrypt = (text) => {
  const iv = crypto.randomBytes(16);
  const cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return `${iv.toString("hex")}:${encrypted.toString("hex")}`;
};

export const decrypt = (encryptedText) => {
  try {
    const decodedText = decodeURIComponent(encryptedText);
    const [ivHex, encryptedHex] = decodedText.split(":");
    const iv = Buffer.from(ivHex, "hex");
    const encryptedTextBuffer = Buffer.from(encryptedHex, "hex");
    const decipher = crypto.createDecipheriv(algorithm, Buffer.from(key), iv);
    let decrypted = decipher.update(encryptedTextBuffer);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString("utf-8");
  } catch (error) {
    console.error("Error during decryption:", error);
    throw new Error("Decryption failed");
  }
};
