// logAuthentication.js
import { pool } from "../config/db";

export async function logAudit(email, operation, success, message) {
  const query = {
    text: "INSERT INTO audit_logs (email_address, operation, success, message) VALUES ($1, $2, $3, $4)",
    values: [email, operation, success, message],
  };

  try {
    await pool.query(query);
  } catch (error) {
    console.error("Error inserting audit log into database:", error);
  }
}
