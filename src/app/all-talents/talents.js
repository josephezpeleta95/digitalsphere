import React, { useEffect, useState } from "react";
import { c_talents } from "@/app/controller/(main)/c_main";
import Link from "next/link";
import Loading from "@/app/(profile)/profile/components/loading";
import { TalentsPulseLoading } from "./../(main)/components/pulseloading";

export default function Talents() {
  const [talents, setTalents] = useState([]);
  const [filterQuery, setFilterQuery] = useState(""); // State to manage the filter query
  const [loadingPulse, setLoadingPulse] = useState(true);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  const fetchTalents = () => {
    c_talents()
      .then((value) => {
        setTalents(value.rows);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
        setLoadingPulse(false);
      });
  };

  useEffect(() => {
    fetchTalents();
  }, []);

  const talentsProducts = talents.filter((talent) =>
    talent.username.toLowerCase().includes(filterQuery.toLowerCase())
  );

  const ProfileCard = ({
    username,
    userlname,
    userskills,
    userprofilepic,
    user_link,
  }) => (
    <Link
      href={`/talent/${user_link}`}
      target='_blank'
      className='cursor-pointer w-full sm:w-1/2 md:w-1/3 lg:w-1/4 p-2'>
      <div className='bg-white rounded-lg shadow-lg overflow-hidden'>
        <div
          className='bg-slate-200 h-48 bg-cover bg-center'
          style={{ backgroundImage: `url(${userprofilepic})` }}></div>
        <div className='p-4'>
          <div className='flex justify-between items-center mb-2'>
            <h3 className='text-lg font-bold'>
              {username} {userlname}
            </h3>
            <img src='/icons/check.svg' alt='check' className='w-6 h-6' />
          </div>
          <div className='flex flex-wrap'>
            {userskills.slice(0, 1).map((role, index) => (
              <span
                key={index}
                className='border p-1 rounded-full border-pink-500 text-xs mr-1 mb-1'>
                {role}
              </span>
            ))}
            {userskills.length > 1 && (
              <span className='border p-1 rounded-full border-pink-500 text-xs mr-1 mb-1'>
                {userskills.length - 1}+
              </span>
            )}
          </div>
        </div>
      </div>
    </Link>
  );

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div className='bg-white w-9/12 px-4 mt-4'>
      <div className='w-full text-2xl mb-4'>
        <h1 className='font-roboto leading-normal text-sky-700 font-semibold'>
          Talents
        </h1>
      </div>
      <div className='w-full mb-4'>
        <input
          type='text'
          className='w-full border border-slate-200 p-2 text-gray-700'
          placeholder='Search products...'
          value={filterQuery}
          onChange={(e) => setFilterQuery(e.target.value)}
        />
      </div>
      <div className='flex flex-wrap -mx-2'>
        {/* Map filtered products */}
        {loadingPulse ? (
          <TalentsPulseLoading />
        ) : (
          talentsProducts.map((talents_profile, index) => (
            <ProfileCard {...talents_profile} key={index} />
          ))
        )}
        {/* End Map filtered products */}
      </div>
    </div>
  );
}
