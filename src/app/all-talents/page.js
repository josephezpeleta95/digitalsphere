"use client";
import Header from "@header/header";
import Talents from "./talents";
import TopFooter from "@topfooter/topfooter";
import Footer from "@footer/footer";
import { SessionProvider } from "next-auth/react";
import Protected from "../controller/protected";

export default function DigitalProduct() {
  return (
    <div className='flex flex-col items-center justify-between bg-white'>
      <SessionProvider>
        <Header />
      </SessionProvider>
      <Talents />
      <TopFooter />
      <Footer />
    </div>
  );
}
