"use client";
import Header from "@header/header";
import Footer from "@footer/footer";
import { SessionProvider } from "next-auth/react";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import Loading from "../../components/loading";
import { c_getserviceprofile } from "@/app/controller/(profile)/c_services";

export default function ProductProfile({ params }) {
  const service_id = params.service_id;
  const [serviceName, setServiceName] = useState("");
  const [serviceDescription, setServiceDescription] = useState("");
  const [serviceRate, setServiceRate] = useState("");
  const [serviceCover, setServiceCover] = useState([]);
  const [productlikes, setProductLikes] = useState("");
  const [serviceWorkSetUp, setServiceWorkSetUp] = useState("");
  const [serviceWorkPer, setServiceWorkPer] = useState("");
  const [userName, setUserName] = useState("");
  const [userimage, setUserImage] = useState("");
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const fetchServiceProfile = async () => {
    c_getserviceprofile(service_id)
      .then((value) => {
        if (value.rows.length > 0) {
          const services = value.rows[0];
          setServiceName(services.service_name);
          setServiceDescription(services.service_description);
          setServiceCover(services.covers);
          setServiceRate(services.service_rate);
          setServiceWorkSetUp(services.service_work_setup);
          setServiceWorkPer(services.service_per);
          setUserName(`${services.username} ${services.userlname}`);
          setUserImage(services.userprofilepic);
        }
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  function addCommas(numString) {
    return Number(numString).toLocaleString();
  }

  useEffect(() => {
    fetchServiceProfile();
  }, []);

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <main className='flex flex-col items-center bg-white'>
      <SessionProvider>
        <Header />

        <div className='w-8/12 min-h-screen mt-5'>
          <div className='w-full p-2 mt-5 flex flex-wrap rounded-lg relative'>
            {/* Background with opacity */}
            <div className='absolute inset-0 bg-gradient-to-r from-sky-500 via-orange-500 to-pink-500 rounded-lg opacity-50'></div>
            {/* Content */}
            {serviceCover.map((url, index) => (
              <div
                key={index}
                className='w-2/6 max-h-80 p-1 flex items-center transform transition-transform hover:scale-105 cursor-pointer'>
                <Image
                  src={`/${url.service_cover.replace(/^\.\/public\//, "")}`}
                  alt={`${url.service_cover_id} Image ${index}`}
                  className='w-80 h-80 object-cover rounded-lg border-2 border-white'
                  width={500}
                  height={500}
                  priority
                />
              </div>
            ))}
          </div>

          <div className='w-full p-2 mt-5 flex'>
            <div className='w-8/12 '>
              <div className='w-full flex '>
                <div>
                  <Image
                    src={`${userimage}`}
                    alt={`Image`}
                    className='w-10 h-10 object-cover rounded-full'
                    width={500}
                    height={500}
                    priority
                  />
                </div>
                <div className='items-center flex px-5'>
                  <h1 className='text-slate-700 text-lg font-normal'>
                    {userName}
                  </h1>
                </div>
              </div>

              <h1 className='text-slate-700 text-2xl font-bold mt-5'>
                {serviceName}
              </h1>

              <div
                className='w-full p-2 mt-5'
                dangerouslySetInnerHTML={{
                  __html: serviceDescription,
                }}></div>
            </div>
            <div className='w-4/12 min-h-96 max-h-96 rounded-lg border border-dashed border-orange-500 justify-center items-center flex flex-wrap'>
              <div className='w-10/12 pt-2 justify-start items-center flex'>
                <h1 className='text-slate-700 text-2xl font-normal'>Rate</h1>
              </div>
              <div className='w-10/12 pt-2 justify-center items-center flex border-b-2'>
                <h1 className='text-slate-700 text-5xl font-normsl mb-5'>
                  ₱{addCommas(serviceRate)}
                </h1>
              </div>
              <div className='w-10/12 pt-2 justify-center items-center flex'>
                <h1 className='text-slate-700 text-2xl font-normsl mb-5'>
                  {serviceWorkSetUp}/ {serviceWorkPer}
                </h1>
              </div>
              <div className='w-4/12 bg-orange-400 h-8 items-center justify-center flex px-2 font-semibold text-white mr-1 rounded'>
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  fill='none'
                  viewBox='0 0 24 24'
                  strokeWidth='1.5'
                  stroke='currentColor'
                  className='w-4 h-4 mr-2'>
                  <path
                    stroke-linecap='round'
                    stroke-linejoin='round'
                    d='M6.633 10.25c.806 0 1.533-.446 2.031-1.08a9.041 9.041 0 0 1 2.861-2.4c.723-.384 1.35-.956 1.653-1.715a4.498 4.498 0 0 0 .322-1.672V2.75a.75.75 0 0 1 .75-.75 2.25 2.25 0 0 1 2.25 2.25c0 1.152-.26 2.243-.723 3.218-.266.558.107 1.282.725 1.282m0 0h3.126c1.026 0 1.945.694 2.054 1.715.045.422.068.85.068 1.285a11.95 11.95 0 0 1-2.649 7.521c-.388.482-.987.729-1.605.729H13.48c-.483 0-.964-.078-1.423-.23l-3.114-1.04a4.501 4.501 0 0 0-1.423-.23H5.904m10.598-9.75H14.25M5.904 18.5c.083.205.173.405.27.602.197.4-.078.898-.523.898h-.908c-.889 0-1.713-.518-1.972-1.368a12 12 0 0 1-.521-3.507c0-1.553.295-3.036.831-4.398C3.387 9.953 4.167 9.5 5 9.5h1.053c.472 0 .745.556.5.96a8.958 8.958 0 0 0-1.302 4.665c0 1.194.232 2.333.654 3.375Z'
                  />
                </svg>
                <h3 className='text-xs font-normal'>Likes {productlikes}</h3>
              </div>
            </div>
          </div>

          <div className='w-full p-2 mt-5 justify-between flex flex-wrap'></div>
        </div>

        <Footer />
      </SessionProvider>
    </main>
  );
}
