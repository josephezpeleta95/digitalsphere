import React, { useEffect, useState, useRef } from "react";
import {
  c_getservices,
  c_deleteservices,
} from "@/app/controller/(profile)/c_services";
import Link from "next/link";
import Image from "next/image";

export default function ServicesCard({
  service_name,
  service_description,
  service_rate,
  service_work_setup,
  service_per,
  service_id,
  covers,
  userId,
  editServices,
  fetchServices,
}) {
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const handleDeleteServices = (service_id, user_id) => {
    c_deleteservices(service_id)
      .then((response) => {
        if (response.code == "1111") {
          fetchServices(user_id);
          alert("Service removed.");
        } else {
          alert("Error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  function addCommas(numString) {
    return Number(numString).toLocaleString();
  }

  const handleEditServices = (
    service_name,
    service_description,
    service_rate,
    service_work_setup,
    service_per,
    service_id,
    covers,
    userId
  ) => {
    editServices(
      service_name,
      service_description,
      service_rate,
      service_work_setup,
      service_per,
      service_id,
      covers,
      userId
    );
  };

  function truncateString(str, maxLength) {
    if (str.length <= maxLength) {
      return str;
    } else {
      return str.slice(0, maxLength - 3) + "...";
    }
  }
  useEffect(() => {
    try {
      c_getservices(userId);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }, []);
  useEffect(() => {}, [
    service_name,
    service_description,
    service_rate,
    service_work_setup,
    service_per,
    service_id,
    covers,
    userId,
  ]);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }
  return (
    <>
      <>
        <div className='w-full px-5 py-1 border border-dashed  mr-2 mb-2'>
          <div className='w-full h-50 flex justify-between '>
            <div className='w-1/4 h-50  p-1 flex justify-between items-center transform transition-transform hover:scale-105 cursor-pointer'>
              <Link href={`/profile/services/${service_id}`} target='_blank'>
                {covers ? (
                  <>
                    <Image
                      src={`/${covers[0].service_cover.replace(
                        /^\.\/public\//,
                        ""
                      )}`}
                      alt={`Image Cayap${service_id}`}
                      className='w-full h-full object-cover rounded-lg'
                      width={500}
                      height={500}
                      priority
                    />
                  </>
                ) : (
                  ""
                )}
              </Link>
            </div>
            <div className='w-3/4'>
              <Link
                href={`/profile/services/${service_id}`}
                target='_blank'
                className='w-full flex items-start p-1'>
                <div className='w-6/12 text-lg px-4 text-sky-700 font-semibold'>
                  {service_name}
                </div>

                <div className='w-6/12 text-sm p-1 flex justify-end text-slate-500 pr-4'>
                  {service_work_setup}
                </div>
              </Link>

              <div className='w-full flex flex-grow h-32 px-1 mt-2'>
                <div
                  className='px-1 px-4 h-30 overflow-auto'
                  style={{ fontSize: "small" }}
                  dangerouslySetInnerHTML={{
                    __html: service_description,
                  }}></div>
              </div>
              <div className='w-full flex px-1'>
                <div className='w-6/12 px-4  items-center'>
                  <p className='text-sky-700 text-sm'>
                    Salary: {addCommas(service_rate)} /{service_per}
                  </p>
                </div>
                <div className='w-6/12 flex justify-end pr-4 mb-2'>
                  <button
                    className='bg-red-500 rounded-full px-3 py-2 text-white text-xs mr-2'
                    onClick={() => handleDeleteServices(service_id, userId)}>
                    Delete
                  </button>
                  <button
                    className='bg-orange-500 rounded-full px-3 py-2 text-white text-xs'
                    onClick={() =>
                      handleEditServices(
                        service_name,
                        service_description,
                        service_rate,
                        service_work_setup,
                        service_per,
                        service_id,
                        covers,
                        userId
                      )
                    }>
                    Edit
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    </>
  );
}
