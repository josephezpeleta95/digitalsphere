import React, { useEffect, useState, useRef } from "react";
import {
  c_deleteservice_cover,
  c_updateservices,
} from "@/app/controller/(profile)/c_services";
import Wysiwyg from "../components/wysiwyg";
import Select from "../components/tailwind-select";

export default function ProductsEdit({
  dbServiceId,
  dbServiceName,
  dbServiceDescription,
  dbServiceRate,
  hideEdit,
  dbCoverFile,
  dbWorkSetUp,
  dbWorkPer,
  dbUserId,
}) {
  const fileInputRef = useRef(null);

  //Cover Photo Variable
  const [previewcover, setPreviewCover] = useState([]);
  const [coverfile, setCoverFile] = useState([]);

  // Service Variable
  const [serviceworkSetUp, setWorkSetUp] = useState("");
  const [serviceWorkPer, setWorkPer] = useState("");

  //Data from Database

  const [dbserviceId, setdbServiceId] = useState("");
  const [dbserviceName, setdbServiceName] = useState("");
  const [dbserviceDescription, setdbServiceDescription] = useState("");
  const [dbservicerate, setdbServiceRate] = useState("");
  const [dbserviceCoverFile, setdbServiceCoverFile] = useState([]);
  const [dbserviceWorkSetUp, setdbServiceWorkSetUp] = useState("");
  const [dbserviceWorkPer, setdbServiceWorkPer] = useState("");
  const [dbserviceUserId, setdbUserId] = useState("");

  //For Edit
  const [dbsnewerviceDescription, setdbNewServiceDescription] = useState("");

  //   const changeContent = (value) => {
  //     getButtonValue(value);
  //   };

  // Cover Photo
  const handleUploadClickCover = () => {
    fileInputRef.current.click();
  };

  const handleFileChangeCover = (e) => {
    const file = e.target.files[0]; // Get the first file from the selected files
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        // Check if the number of previews is less than six
        if (previewcover.length < 3) {
          // Use functional update to ensure state is updated correctly
          setPreviewCover((prevPreviews) => [...prevPreviews, reader.result]);
          setCoverFile((prevPreviews) => [...prevPreviews, file]);
        } else {
          // Alert the user or handle the case where the limit is exceeded
          alert("You can only upload up to six images.");
        }
      };
      reader.readAsDataURL(file);
    }
  };

  const handleClearCover = (index) => {
    setPreviewCover((prevPreviews) => {
      const updatedPreviews = [...prevPreviews];
      updatedPreviews.splice(index, 1);
      return updatedPreviews;
    });

    setCoverFile((prevPreviews) => {
      const updatedPreviews = [...prevPreviews];
      updatedPreviews.splice(index, 1);
      return updatedPreviews;
    });
  };

  // Add Delete Query
  const handleClearDbCover = (index, service_c_id) => {
    c_deleteservice_cover(service_c_id)
      .then((response) => {
        if (response.code == "1111") {
          setdbServiceCoverFile((prevPreviews) => {
            const updatedPreviews = [...prevPreviews];
            updatedPreviews.splice(index, 1);
            return updatedPreviews;
          });
          alert("Deleted");
        } else {
          alert(response.message);
        }
      })
      .catch((error) => {
        alert("Error in deleting cover photo:", error);
      });
  };

  const handleContentWysiwyg = (value) => {
    setdbNewServiceDescription(value);
  };

  const handleSubmitProduct = () => {
    c_updateservices(
      dbserviceId,
      dbserviceUserId,
      dbserviceName,
      serviceworkSetUp,
      serviceWorkPer,
      dbservicerate,
      coverfile,
      dbsnewerviceDescription
    )
      .then((response) => {
        if (response.code == "1111") {
          alert(response.message);
          hideEdit(false);
        } else {
          alert(response.message);
        }
      })
      .catch((error) => {
        console.log("Error", error);
      });
  };

  const handleHideEdit = () => {
    hideEdit(false);
  };
  const work_SetUp = [
    { type: "Freelance" },
    { type: "Full-time" },
    { type: "Part-time" },
  ];

  const workSetup = work_SetUp.map(({ type }, index) => ({
    id: index,
    value: type,
    label: type,
  }));
  const servicePer = [
    { type: "Project" },
    { type: "Hr" },
    { type: "Day" },
    { type: "Week" },
    { type: "Month" },
  ];

  const workPer = servicePer.map(({ type }, index) => ({
    id: index,
    value: type,
    label: type,
  }));

  const dataWorkSetup = (data) => {
    if (data && data.length > 0) {
      setWorkSetUp(data[0].value);
      return;
    }
  };

  const dataWorkPer = (data) => {
    if (data && data.length > 0) {
      setWorkPer(data[0].value);
      return;
    }
  };

  useEffect(() => {
    setdbServiceId(dbServiceId);
    setdbServiceName(dbServiceName);
    setdbServiceDescription(dbServiceDescription);
    setdbServiceRate(dbServiceRate);
    setdbServiceCoverFile(dbCoverFile);
    setdbServiceWorkSetUp(dbWorkSetUp);
    setdbServiceWorkPer(dbWorkPer);
    setdbUserId(dbUserId);
  }, [dbServiceName, dbServiceDescription, dbServiceRate, dbCoverFile]);

  return (
    <>
      <div className={`w-full pt-4 px-10`}>
        <div className='w-full flex'>
          <div className='w-6/12'>
            <h1 className='text-2xl text-slate-400'>Edit Service</h1>
          </div>
          <div className='w-6/12 justify-end flex'>
            <button
              className='p-1 bg-orange-600 px-2 rounded-full text-white'
              onClick={() => handleHideEdit()}>
              Cancel Edit
            </button>
          </div>
        </div>
        <div className='w-full mr-2 pt-4'>
          <label className='text-sm font-sans text-slate-500'>
            Service Name
          </label>
          <input
            type='text'
            className='w-full rounded border p-1 outline-none text-slate-500'
            value={dbserviceName}
            onChange={(e) => setdbServiceName(e.target.value)}></input>
        </div>
        <div className='w-full pt-4'>
          <Wysiwyg
            content={handleContentWysiwyg}
            label={"Service Description"}
            returnData={dbserviceDescription}
          />
        </div>
        <div className='w-full pt-4'>
          <label className='text-sm font-sans text-slate-500'>
            Service Cover
          </label>
          <div className='w-full min-h-60 rounded border p-1 border border-slate-200 items-center'>
            <div className='w-full min-h-60 justify-center p-1 flex flex-wrap'>
              {dbserviceCoverFile && dbserviceCoverFile.length > 0 ? (
                <>
                  {dbserviceCoverFile.map((cover_file, index) => (
                    <div className='relative' key={index}>
                      <img
                        src={`/${cover_file.service_cover.replace(
                          /^\.\/public\//,
                          ""
                        )}`}
                        alt={`Photo ${index + 1}`}
                        className='h-48 w-48 object-cover relative mr-1 mt-1'
                      />
                      <button
                        className='absolute top-0 right-0 m-2 p-2 bg-red-500 text-white rounded-full z-10'
                        onClick={() =>
                          handleClearDbCover(index, cover_file.service_cover_id)
                        }>
                        <p className='text-xs'>Remove</p>
                      </button>
                    </div>
                  ))}
                </>
              ) : (
                ""
              )}
              {previewcover && previewcover.length > 0 ? (
                <>
                  {previewcover.map((cover_file, index) => (
                    <div className='relative' key={index}>
                      <img
                        src={`${cover_file.replace(/^\.\/public\//, "")}`}
                        alt={`Photo ${index + 1}`}
                        className='h-48 w-48 object-cover relative mr-1 mt-1'
                      />
                      <button
                        className='absolute top-0 right-0 m-2 p-2 bg-red-500 text-white rounded-full z-10'
                        onClick={() => handleClearCover(index)}>
                        <p className='text-xs'>Remove</p>
                      </button>
                    </div>
                  ))}
                </>
              ) : (
                ""
              )}
            </div>

            <div className='w-full justify-center items-center flex'>
              <button
                className={`py-1 px-4 border ${
                  previewcover.length >= 3
                    ? "border-slate-500 text-slate-500"
                    : "border-pink-500 text-pink-500"
                } border-pink-500 rounded-full `}
                disabled={previewcover.length >= 3}
                onClick={handleUploadClickCover}>
                {previewcover.length >= 3
                  ? "You are only Allowed 3 Items"
                  : " Add Cover Gallery"}
              </button>
            </div>
            <input
              type='file'
              accept='image/*'
              multiple
              onChange={handleFileChangeCover}
              className='hidden'
              ref={fileInputRef}
            />
          </div>
        </div>

        <div className='w-full mr-2 pt-4'>
          <Select
            data={workSetup}
            type={"single"}
            searchresult={dataWorkSetup}
            textfieldlabel={"Work Set-up"}
            returnData={[
              {
                value: dbserviceWorkSetUp,
                label: dbserviceWorkSetUp,
              },
            ]}
          />
        </div>
        <div className='w-full mr-2 pt-4 justify-between flex'>
          <div className='w-5/12'>
            <label className='text-sm font-sans text-slate-500'>
              Service Rate
            </label>
            <input
              type='number'
              className='w-full rounded border p-1 outline-none text-slate-500'
              value={dbservicerate}
              onChange={(e) => setdbServiceRate(e.target.value)}></input>
          </div>
          <div className='w-5/12'>
            <Select
              data={workPer}
              type={"single"}
              searchresult={dataWorkPer}
              textfieldlabel={"Per"}
              returnData={[
                {
                  value: dbserviceWorkPer,
                  label: dbserviceWorkPer,
                },
              ]}
            />
          </div>
        </div>

        {/* <div className={`w-full pt-4 px-10`}>
        <div className='w-full flex'>
          <div className='w-6/12'>
            <h1 className='text-2xl text-slate-400'>Edit Product</h1>
          </div>
          <div className='w-6/12 justify-end flex'>
            <button
              className='p-1 bg-orange-600 px-2 rounded-full text-white'
              onClick={() => handleHideEdit()}>
              Cancel Edit
            </button>
          </div>
        </div>



        <div className='w-full mr-2 pt-4'>
          <label className='text-sm font-sans text-slate-500'>
            Product Name
          </label>
          <input
            type='text'
            className='w-full rounded border p-1 outline-none text-slate-500'
            value={dbproductName}
            onChange={(e) => setdbProductName(e.target.value)}></input>
        </div>
        <div className='w-full pt-4'>
          <Wysiwyg
            content={handleContentWysiwyg}
            label={"Product Description"}
            returnData={productDescription}
          />
        </div>
        <div className='w-full pt-4'>
          <label className='text-sm font-sans text-slate-500'>
            Product Cover
          </label>
          <div className='w-full min-h-60 rounded border p-1 border border-slate-200 items-center'>
            <div className='w-full min-h-48 justify-center p-1 flex flex-wrap'>
              {dbproductCover && dbproductCover.length > 0 ? (
                <>
                  {dbproductCover.map((cover_file, index) => (
                    <div className='relative' key={index}>
                      <img
                        src={`/${cover_file.product_cover.replace(
                          /^\.\/public\//,
                          ""
                        )}`}
                        alt={`Photo ${index + 1}`}
                        className='h-48 w-48 object-cover relative mr-1 mt-1'
                      />
                      <button
                        className='absolute top-0 right-0 m-2 p-2 bg-red-500 text-white rounded-full z-10'
                        onClick={() =>
                          handleClearDbCover(index, cover_file.product_c_id)
                        }>
                        <p className='text-xs'>Remove</p>
                      </button>
                    </div>
                  ))}
                </>
              ) : (
                ""
              )}
              {previewcover && previewcover.length > 0 ? (
                <>
                  {previewcover.map((cover_file, index) => (
                    <div className='relative' key={index}>
                      <img
                        src={`${cover_file.replace(/^\.\/public\//, "")}`}
                        alt={`Photo ${index + 1}`}
                        className='h-48 w-48 object-cover relative mr-1 mt-1'
                      />
                      <button
                        className='absolute top-0 right-0 m-2 p-2 bg-red-500 text-white rounded-full z-10'
                        onClick={() => handleClearCover(index)}>
                        <p className='text-xs'>Remove</p>
                      </button>
                    </div>
                  ))}
                </>
              ) : (
                ""
              )}
            </div>

            <div className='w-full justify-center items-center flex'>
              <button
                className={`py-1 px-4 border ${
                  dbproductCover.length + previewcover.length >= 3
                    ? "border-slate-500 text-slate-500"
                    : "border-pink-500 text-pink-500"
                } border-pink-500 rounded-full `}
                disabled={dbproductCover.length >= 3}
                onClick={handleUploadClickCover}>
                {dbproductCover.length >= 3
                  ? "You are only Allowed 3 Items"
                  : " Add Cover Gallery"}
              </button>
            </div>
            <input
              type='file'
              accept='image/*'
              multiple
              onChange={handleFileChangeCover}
              className='hidden'
              ref={fileInputRef}
            />
          </div>
        </div>

        <div className='w-full pt-4'>
          <label className='text-sm font-sans text-slate-500'>Product</label>
          <div className='w-full min-h-24 rounded border p-1 border border-slate-200 items-center'>
            <div className='w-full min-h-20 justify-center flex flex-wrap'>
              {dbproductFileName ? (
                <>
                  <div className='justify-center items-center flex pt-5 pb-5'>
                    <h1 className='font-bold'>
                      {dbproductFileName
                        ? dbproductFileName
                        : "No file exist.."}
                    </h1>
                  </div>
                  <div className='w-full justify-center items-center flex'>
                    <button
                      className='py-1 px-4 border border-pink-500 rounded-full text-pink-500 mb-5'
                      onClick={() =>
                        handleClearDbProduct(dbProductFile[0].product_f_id)
                      }>
                      Remove
                    </button>
                  </div>
                </>
              ) : (
                <>
                  <div className='justify-center items-center flex pt-5 pb-5'>
                    <h1 className='font-bold'>
                      {productfileName ? productfileName : "Please select file"}
                    </h1>
                  </div>

                  {productfileName ? (
                    <div className='w-full justify-center items-center flex'>
                      <button
                        className='py-1 px-4 border border-pink-500 rounded-full text-pink-500 mb-5'
                        onClick={() => handleClearProduct()}>
                        Remove
                      </button>
                    </div>
                  ) : (
                    <>
                      <div className='w-full justify-center items-center flex'>
                        <button
                          className='py-1 px-4 border border-pink-500 rounded-full text-pink-500 mb-5'
                          onClick={handleUploadClickProduct}>
                          Upload Product
                        </button>
                      </div>
                      <input
                        type='file'
                        accept='*'
                        multiple
                        onChange={handleFileChangeProduct}
                        className='hidden'
                        ref={fileInputRefPrdouct}
                      />
                    </>
                  )}
                </>
              )}
            </div>
          </div>
        </div>
        <div className='w-full mr-2 pt-4'>
          <label className='text-sm font-sans text-slate-500'>Price</label>
          <input
            type='number'
            className='w-full rounded border p-1 outline-none text-slate-500'
            value={dbprice}
            onChange={(e) => setdbPrice(e.target.value)}></input>
        </div>
      </div>

       */}

        <div
          className={`w-full items-center justify-center flex pt-4 justify-between flex px-10`}>
          <div className={`w-full mr-2 `}>
            <button
              className='border w-1/5 bg-orange-500 rounded-full px-3 py-2 text-white'
              onClick={() => handleSubmitProduct()}>
              Update
            </button>
          </div>
        </div>

        {/* List of Products */}
      </div>
    </>
  );
}
