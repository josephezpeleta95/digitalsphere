import React, { useEffect, useState, useRef } from "react";
import Wysiwyg from "../components/wysiwyg";
import Select from "../components/tailwind-select";
import {
  c_saveservices,
  c_getservices,
  c_deleteservices,
} from "@/app/controller/(profile)/c_services";
import Link from "next/link";
import Image from "next/image";
import ServiceCard from "./servicescard";
import EditService from "./servicesedit";

export default function Services({ userdata }) {
  const fileInputRef = useRef(null);
  const [buttonValue, getButtonValue] = useState("");

  const [serviceName, setServiceName] = useState("");
  const [serviceDescription, setServiceDescription] = useState("");
  const [buttonEdit, getButtonEdit] = useState(false);
  //Cover Photo Variable
  const [previewcover, setPreviewCover] = useState([]);
  const [coverfile, setCoverFile] = useState([]);
  const [workSetUp, setWorkSetUp] = useState("");
  const [rate, setRate] = useState("");
  const [workper, setWorkPer] = useState("");
  const [service, setServices] = useState("");

  //Well be used on update
  // Edit Variable
  const [editServiceId, setEditServiceId] = useState("");
  const [editServiceName, setEditServicetName] = useState("");
  const [editServiceDescription, setEditServiceDescription] = useState("");
  const [editServiceRate, setEditRate] = useState("");
  const [editCoverFile, setEditCoverFile] = useState([]);
  const [editworkSetUp, setDditWorkSetUp] = useState([]);
  const [editworkper, setEditWorkPer] = useState([]);
  const [editUserId, setEditUserId] = useState("");
  const [editServiceCoverId, setServiceCoverId] = useState("");

  const [userId, setUserId] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const changeContent = (value) => {
    getButtonValue(value);
  };

  const handleContentWysiwyg = (value) => {
    setServiceDescription(value);
  };

  // Cover Photo
  const handleUploadClickCover = () => {
    fileInputRef.current.click();
  };

  const handleFileChangeCover = (e) => {
    const file = e.target.files[0]; // Get the first file from the selected files

    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        // Check if the number of previews is less than six
        if (previewcover.length < 3) {
          // Use functional update to ensure state is updated correctly
          setPreviewCover((prevPreviews) => [...prevPreviews, reader.result]);
          setCoverFile((prevPreviews) => [...prevPreviews, file]);
        } else {
          // Alert the user or handle the case where the limit is exceeded
          alert("You can only upload up to six images.");
        }
      };
      reader.readAsDataURL(file);
    }
  };

  const handleClearCover = (index) => {
    setPreviewCover((prevPreviews) => {
      const updatedPreviews = [...prevPreviews];
      updatedPreviews.splice(index, 1);
      return updatedPreviews;
    });

    setCoverFile((prevPreviews) => {
      const updatedPreviews = [...prevPreviews];
      updatedPreviews.splice(index, 1);
      return updatedPreviews;
    });
  };

  const work_SetUp = [
    {
      type: "Freelance",
    },
    {
      type: "Full-time",
    },
    {
      type: "Part-time",
    },
  ];

  const workSetup = work_SetUp.map(({ type }, index) => ({
    id: index,
    value: type,
    label: type,
  }));

  const per_SetUp = [
    {
      type: "Project",
    },
    {
      type: "Hr",
    },
    {
      type: "Day",
    },
    {
      type: "Week",
    },
    {
      type: "Month",
    },
  ];

  const workPer = per_SetUp.map(({ type }, index) => ({
    id: index,
    value: type,
    label: type,
  }));

  const dataWorkSetup = (data) => {
    if (data && data.length > 0) {
      setWorkSetUp(data[0].value);
      return;
    }
  };

  const dataWorkPer = (data) => {
    if (data && data.length > 0) {
      setWorkPer(data[0].value);
      return;
    }
  };

  const handleSubmitServices = () => {
    c_saveservices(
      serviceName,
      serviceDescription,
      coverfile,
      rate,
      workSetUp,
      workper,
      userdata.user_id
    )
      .then((response) => {
        if (response.code === "1111") {
          alert("Saved");
          window.location.reload();
        } else {
          alert("Not Saved");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const fetchServices = async (user_Id) => {
    try {
      const value = await c_getservices(user_Id);
      setServices(value.rows);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const handleEditService = (
    service_name,
    service_description,
    service_rate,
    service_work_setup,
    service_per,
    service_id,
    covers,
    userId,
    service_c_id
  ) => {
    changeContent("addProd");
    getButtonEdit(true);
    setEditServiceId(service_id);
    setEditServicetName(service_name);
    setEditServiceDescription(service_description);
    setEditRate(service_rate);
    setEditCoverFile(covers);
    setDditWorkSetUp(service_work_setup);
    setEditWorkPer(service_per);
    setEditUserId(userId);
    setServiceCoverId(service_c_id);
  };

  const handleHideEdit = (value) => {
    getButtonEdit(value);
    fetchServices(userdata.user_id);
    getButtonValue("");
  };

  useEffect(() => {
    fetchServices(userdata.user_id);
    setUserId(userdata.user_id);
  }, []);

  if (loading) {
    return (
      <div className='flex items-center justify-center w-full mt-10'>
        <div className='relative flex flex-col items-center space-y-1'>
          <div className='w-36 h-36 border-4 border-t-4 border-r-4 border-b-4 border-l-4 border-t-red-500 border-r-blue-500 border-b-green-500 border-l-yellow-500 rounded-full animate-spin'></div>
          <div className='text-lg text-gray-500'>Loading...</div>
        </div>
      </div>
    );
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <>
      {buttonEdit === true ? (
        <EditService
          dbServiceId={editServiceId}
          dbServiceName={editServiceName}
          dbServiceDescription={editServiceDescription}
          dbServiceRate={editServiceRate}
          hideEdit={handleHideEdit}
          dbCoverFile={editCoverFile}
          dbWorkSetUp={editworkSetUp}
          dbWorkPer={editworkper}
          dbUserId={editUserId}
          dbServiceCoverId={editServiceCoverId}
          // fetchservice={fetchServices}
        />
      ) : (
        <>
          <div className={`w-full px-10 mt-10`}>
            <div
              className={`w-40 justify-center flex p-2 rounded-full bg-cyan-500 text-white mr-2 border ${
                buttonValue === "addProd" ? "hidden" : ""
              }`}>
              <button onClick={() => changeContent("addProd")}>
                Add Service
              </button>
            </div>
            <div
              className={`w-40 justify-center flex p-2 rounded-full bg-cyan-500 text-white mr-2  border ${
                buttonValue === "addProd" ? "" : "hidden"
              }`}>
              <button onClick={() => changeContent("")}>My Services</button>
            </div>
          </div>

          <div
            className={`w-full pt-4 px-10 ${
              buttonValue === "addProd" ? "" : "hidden"
            }`}>
            <div className='w-full mr-2 pt-4'>
              <label className='text-sm font-sans text-slate-500'>
                Service Name
              </label>
              <input
                type='text'
                className='w-full rounded border p-1 outline-none text-slate-500'
                value={serviceName}
                onChange={(e) => setServiceName(e.target.value)}></input>
            </div>
            <div className='w-full pt-4'>
              <Wysiwyg
                content={handleContentWysiwyg}
                label={"Service Description"}
                returnData={""}
              />
            </div>
            <div className='w-full pt-4'>
              <label className='text-sm font-sans text-slate-500'>
                Service Cover
              </label>
              <div className='w-full min-h-60 rounded border p-1 border border-slate-200 items-center'>
                <div className='w-full min-h-48 justify-center p-1 flex flex-wrap'>
                  {previewcover && previewcover.length > 0 ? (
                    <>
                      {previewcover.map((previewImage, index) => (
                        <div className='relative' key={index}>
                          <img
                            src={previewImage}
                            alt={`Photo ${index + 1}`}
                            className='h-48 w-48 object-cover relative mr-1 mt-1'
                          />
                          <button
                            className='absolute top-0 right-0 m-2 p-2 bg-red-500 text-white rounded-full z-10'
                            onClick={() => handleClearCover(index)}>
                            <p className='text-xs'>Remove</p>
                          </button>
                        </div>
                      ))}
                    </>
                  ) : (
                    <div className='w-full min-h-48 justify-start items-center flex'></div>
                  )}
                </div>

                <div className='w-full justify-center items-center flex'>
                  <button
                    className={`py-1 px-4 border ${
                      previewcover.length >= 3
                        ? "border-slate-500 text-slate-500"
                        : "border-pink-500 text-pink-500"
                    } border-pink-500 rounded-full `}
                    disabled={previewcover.length >= 3}
                    onClick={handleUploadClickCover}>
                    {previewcover.length >= 3
                      ? "You are only Allowed 3 Items"
                      : " Add Cover Gallery"}
                  </button>
                </div>
                <input
                  type='file'
                  accept='image/*'
                  multiple
                  onChange={handleFileChangeCover}
                  className='hidden'
                  ref={fileInputRef}
                />
              </div>
            </div>

            <div className='w-full mr-2 pt-4'>
              <Select
                data={workSetup}
                type={"single"}
                searchresult={dataWorkSetup}
                textfieldlabel={"Work Set-up"}
                returnData={[]}
              />
            </div>
            <div className='w-full mr-2 pt-4 justify-between flex'>
              <div className='w-5/12'>
                <label className='text-sm font-sans text-slate-500'>
                  Price
                </label>
                <input
                  type='number'
                  className='w-full rounded border p-1 outline-none text-slate-500'
                  value={rate}
                  onChange={(e) => setRate(e.target.value)}></input>
              </div>
              <div className='w-5/12'>
                <Select
                  data={workPer}
                  type={"single"}
                  searchresult={dataWorkPer}
                  textfieldlabel={"Per"}
                  returnData={[]}
                />
              </div>
            </div>
          </div>

          <div
            className={`w-full items-center justify-center flex pt-4 justify-between flex px-10 ${
              buttonValue === "addProd" ? "" : "hidden"
            }}`}>
            <div
              className={`w-full mr-2 ${
                buttonValue === "addProd" ? "" : "hidden"
              }`}>
              <button
                className='border w-1/5 bg-pink-500 rounded-full px-3 py-2 text-white  shadow-lg shadow-pink-500/30'
                onClick={() => handleSubmitServices()}>
                Submit
              </button>
            </div>
          </div>
        </>
      )}
      {/* List of Services */}
      <div
        className={`w-full flex flex-wrap items-center p-4 mb-10 ${
          buttonValue === "addProd" ? "hidden" : ""
        }`}>
        <div className='w-full'>
          <input
            type='text'
            className='w-full border rounded-full border-slate-300 py-1 px-3 mb-3 outline-none'
            placeholder='Search'></input>
        </div>
        {/* Map products  */}
        {service.length > 0 ? (
          service.map((services, index) => (
            <ServiceCard
              {...services}
              userId={userId}
              editServices={handleEditService}
              fetchServices={fetchServices}
              key={index}
            />
          ))
        ) : (
          <div className='flex flex-col w-full items-center justify-center h-full p-6 bg-gray-50 border border-gray-200 rounded-lg shadow-md'>
            <div className='flex flex-col items-center'>
              <svg
                className='w-12 h-12 mb-4 text-gray-400'
                fill='none'
                stroke='currentColor'
                viewBox='0 0 24 24'
                xmlns='http://www.w3.org/2000/svg'>
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  strokeWidth='2'
                  d='M7 8h10M7 12h4m-4 4h10M5 6h14a2 2 0 012 2v12a2 2 0 01-2 2H5a2 2 0 01-2-2V8a2 2 0 012-2z'></path>
              </svg>
              <h2 className='mb-2 text-lg font-semibold text-gray-600'>
                No Services Available
              </h2>
              <p className='text-sm text-gray-500'>
                You have not added any services yet.
              </p>
            </div>
          </div>
        )}

        {/*End  Map products  */}
      </div>
    </>
  );
}
