import React, { useEffect, useState, useRef } from "react";
import {
  c_getproducts,
  c_deleteproducts,
} from "@/app/controller/(profile)/c_products";
import Link from "next/link";
import Image from "next/image";

export default function Products({
  product_id,
  product_name,
  product_description,
  covers,
  price,
  files,
  total_sold,
  total_likes,
  userId,
  editProduct,
  fetchProduct,
}) {
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const handleDeleteProduct = (product_id, user_id) => {
    c_deleteproducts(product_id)
      .then((response) => {
        if (response.code == "1111") {
          fetchProduct(user_id);
          alert("Product removed.");
        } else {
          alert("Error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  function addCommas(numString) {
    return Number(numString).toLocaleString();
  }

  const handleEditProduct = (
    product_id,
    product_name,
    product_description,
    price,
    product_cover,
    product,
    user_id
  ) => {
    editProduct(
      product_id,
      product_name,
      product_description,
      price,
      product_cover,
      product,
      user_id
    );
  };

  function truncateString(str, maxLength) {
    if (str.length <= maxLength) {
      return str;
    } else {
      return str.slice(0, maxLength - 3) + "...";
    }
  }
  useEffect(() => {
    try {
      c_getproducts(userId);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }, []);
  useEffect(() => {}, [
    product_id,
    product_name,
    product_description,
    covers,
    price,
    files,
    total_sold,
    total_likes,
    userId,
  ]);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }
  return (
    <>
      <>
        <div className='w-full px-5 py-1 border border-dashed  mr-2 mb-2'>
          <div className='w-full h-50 flex justify-between '>
            <div className='w-1/4 h-50 p-1 flex justify-between items-center transform transition-transform hover:scale-105 cursor-pointer'>
              <Link href={`/profile/products/${product_id}`} target='_blank'>
                {covers ? (
                  <>
                    <Image
                      src={`/${covers[0].product_cover.replace(
                        /^\.\/public\//,
                        ""
                      )}`}
                      alt={`Image Cayap${product_id}`}
                      className='w-full h-full object-cover rounded-lg'
                      width={500}
                      height={500}
                      priority
                    />
                  </>
                ) : (
                  ""
                )}
              </Link>
            </div>

            <div className='w-3/4'>
              <div className='w-full flex items-start p-1'>
                <div className='w-full text-lg px-4 text-sky-700 font-semibold'>
                  <Link
                    href={`/profile/products/${product_id}`}
                    target='_blank'>
                    {product_name}
                  </Link>
                </div>
              </div>
              <div className='w-full flex flex-grow h-32 px-1'>
                <div
                  className='px-1 text-ellipsis overflow-hidden px-4'
                  style={{ fontSize: "small" }}
                  dangerouslySetInnerHTML={{
                    __html: truncateString(product_description, 300),
                  }}></div>
              </div>
              <div className='w-full flex items-center px-1 pt-6'>
                <div className='w-3/12 text-center '>
                  <p className='text-sky-700 text-sm font-bold'>
                    P {addCommas(price)}
                  </p>
                </div>
                <div className='w-3/12 text-center'>
                  <p className='text-sky-700 text-sm font-bold'>
                    Sold: {total_sold}
                  </p>
                </div>
                <div className='w-3/12 text-center'>
                  <p className='text-sky-700 text-sm font-bold'>
                    Likes {total_likes}
                  </p>
                </div>
                <div className='w-3/12 flex justify-end pr-4 mb-2'>
                  <button
                    className='bg-red-500 rounded-full px-3 py-2 text-white text-xs mr-2'
                    onClick={() => handleDeleteProduct(product_id, userId)}>
                    Delete
                  </button>
                  <button
                    className='bg-orange-500 rounded-full px-3 py-2 text-white text-xs'
                    onClick={() =>
                      handleEditProduct(
                        product_id,
                        product_name,
                        product_description,
                        price,
                        covers,
                        files,
                        userId
                      )
                    }>
                    Edit
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    </>
  );
}
