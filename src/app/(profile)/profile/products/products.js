import React, { useEffect, useState, useRef } from "react";
import {
  c_saveproducts,
  c_getproducts,
} from "@/app/controller/(profile)/c_products";
import Wysiwyg from "../components/wysiwyg";
import EditProduct from "./productsedit";
import ProductCard from "./productscard";

export default function Products(userdata) {
  const fileInputRef = useRef(null);
  const fileInputRefPrdouct = useRef(null);
  const [buttonValue, getButtonValue] = useState("");
  const [buttonEdit, getButtonEdit] = useState(false);
  const [productName, setProductName] = useState("");
  const [productDescription, setProductDescription] = useState("");
  const [productDescriptionReturnData, setProductDescriptionReturnData] =
    useState("");
  const [price, setPrice] = useState("");

  //Cover Photo Variable
  const [previewcover, setPreviewCover] = useState([]);
  const [coverfile, setCoverFile] = useState([]);

  // Product Variable
  const [previewproduct, setPreviewProduct] = useState(null);
  const [productfileName, setPrdouctFileName] = useState("");
  const [productfile, setPrdouctFile] = useState("");
  const [productgalery, setProductGalery] = useState([]);
  const [product, setProduct] = useState([]);
  const [searchvalue, setSearchValue] = useState("");

  // Edit Variable
  const [editProductId, setEditProductId] = useState("");
  const [editProductName, setEditProductName] = useState("");
  const [editProductDescription, setEditProductDescription] = useState("");
  const [editPrice, setEditPrice] = useState("");
  const [editPreviewproduct, setEditPreviewProduct] = useState([]);
  const [editProductfile, setEditProductFile] = useState("");
  const [editUserId, setEditUserId] = useState("");
  //Cover Photo Variable
  // const [previewcover, setPreviewCover] = useState([]);
  // const [coverfile, setCoverFile] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const changeContent = (value) => {
    getButtonValue(value);
  };

  // Cover Photo
  const handleUploadClickCover = () => {
    fileInputRef.current.click();
  };

  const handleFileChangeCover = (e) => {
    const file = e.target.files[0]; // Get the first file from the selected files

    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        // Check if the number of previews is less than six
        if (previewcover.length < 3) {
          // Use functional update to ensure state is updated correctly
          setPreviewCover((prevPreviews) => [...prevPreviews, reader.result]);
          setCoverFile((prevPreviews) => [...prevPreviews, file]);
        } else {
          // Alert the user or handle the case where the limit is exceeded
          alert("You can only upload up to six images.");
        }
      };
      reader.readAsDataURL(file);
    }
  };

  const handleClearCover = (index) => {
    setPreviewCover((prevPreviews) => {
      const updatedPreviews = [...prevPreviews];
      updatedPreviews.splice(index, 1);
      return updatedPreviews;
    });

    setCoverFile((prevPreviews) => {
      const updatedPreviews = [...prevPreviews];
      updatedPreviews.splice(index, 1);
      return updatedPreviews;
    });
  };

  // Product
  const handleUploadClickProduct = () => {
    fileInputRefPrdouct.current.click();
  };

  const handleFileChangeProduct = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        // setImage(file);
        const fileName = file.name;
        setPrdouctFileName(fileName);
        setPrdouctFile(file);
        setPreviewProduct(reader.result);
      };
      reader.readAsDataURL(file);
    }
  };

  const handleClearProduct = () => {
    setPrdouctFileName("");
  };

  const fetchUsersProduct = async (userId) => {
    try {
      const value = await c_getproducts(userId);
      setProductGalery(value.rows);
      setProduct(value.rows);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  };

  const handleContentWysiwyg = (value) => {
    setProductDescription(value);
  };

  // Function to handle filtering products
  const handleFilterProduct = (value) => {
    setSearchValue(value);
    const term = value.toLowerCase();
    if (term === "") {
      // If search value is empty, reset product gallery to original array
      setProductGalery(product);
    } else {
      // Otherwise, filter the product gallery based on the search term
      const filteredProducts = productgalery.filter((datum) =>
        datum.product_name.toLowerCase().includes(term)
      );
      setProductGalery(filteredProducts);
    }
  };

  const handleEditProduct = (
    product_id,
    product_name,
    product_description,
    price,
    product_cover,
    product,
    user_id
  ) => {
    setEditProductId(product_id);
    changeContent("addProd");
    getButtonEdit(true);
    setEditProductName(product_name);
    setEditProductDescription(product_description);
    setEditPrice(price);
    setEditPreviewProduct(product_cover);
    setEditProductFile(product);
    setEditUserId(user_id);
  };

  const handleSubmitProduct = () => {
    c_saveproducts(
      productName,
      productDescription,
      price,
      coverfile,
      productfile,
      userdata.userdata.user_id
    )
      .then((response) => {
        if (response.code == "1111") {
          setProductName("");
          setProductDescriptionReturnData("");
          setPrice("");
          setPreviewCover([]);
          setCoverFile([]);
          setPrdouctFile("");
          window.location.reload();
          alert(response.message);
        } else {
          alert(response.message);
        }
      })
      .catch((error) => {
        console.log("Error", error);
      });
  };

  const handleChangeContent = () => {
    fetchUsersProduct(userdata.userdata.user_id);
    changeContent("");
  };

  const handleHideEdit = (value) => {
    getButtonEdit(value);
    fetchUsersProduct(userdata.userdata.user_id);
    getButtonValue("");
  };

  useEffect(() => {
    fetchUsersProduct(userdata.userdata.user_id);
  }, []);

  if (loading) {
    return (
      <div className='flex items-center justify-center w-full mt-10'>
        <div className='relative flex flex-col items-center space-y-1'>
          <div className='w-36 h-36 border-4 border-t-4 border-r-4 border-b-4 border-l-4 border-t-red-500 border-r-blue-500 border-b-green-500 border-l-yellow-500 rounded-full animate-spin'></div>
          <div className='text-lg text-gray-500'>Loading...</div>
        </div>
      </div>
    );
  }

  if (error) {
    return <div>Error: {error}</div>;
  }
  return (
    <>
      {buttonEdit === true ? (
        <EditProduct
          dbProductId={editProductId}
          dbProductName={editProductName}
          dbProductDescription={editProductDescription}
          dbPrice={editPrice}
          hideEdit={handleHideEdit}
          dbProductCover={editPreviewproduct}
          dbProductFile={editProductfile}
          dbUserId={editUserId}
          fetchProduct={fetchUsersProduct}
        />
      ) : (
        <>
          <div className={`w-full px-10 mt-10`}>
            <div
              className={`w-40 justify-center flex p-2 rounded-full bg-cyan-500 text-white mr-2 border ${
                buttonValue === "addProd" ? "hidden" : ""
              }`}>
              <button onClick={() => changeContent("addProd")}>
                Add Product
              </button>
            </div>
            <div
              className={`w-40 justify-center flex p-2 rounded-full bg-cyan-500 text-white mr-2  border ${
                buttonValue === "addProd" ? "" : "hidden"
              }`}>
              <button onClick={() => handleChangeContent()}>My Products</button>
            </div>
          </div>
          <div
            className={`w-full pt-4 px-10 ${
              buttonValue === "addProd" ? "" : "hidden"
            }`}>
            <div className='w-full mr-2 pt-4'>
              <label className='text-sm font-sans text-slate-500'>
                Product Name
              </label>
              <input
                type='text'
                className='w-full rounded border p-1 outline-none text-slate-500'
                value={productName}
                onChange={(e) => setProductName(e.target.value)}></input>
            </div>
            <div className='w-full pt-4'>
              <Wysiwyg
                content={handleContentWysiwyg}
                label={"Product Description"}
                returnData={productDescriptionReturnData}
              />
            </div>
            <div className='w-full pt-4'>
              <label className='text-sm font-sans text-slate-500'>
                Product Cover
              </label>
              <div className='w-full min-h-60 rounded border p-1 border border-slate-200 items-center'>
                <div className='w-full min-h-48 justify-center p-1 flex flex-wrap'>
                  {previewcover && previewcover.length > 0 ? (
                    <>
                      {previewcover.map((previewImage, index) => (
                        <div className='relative' key={index}>
                          <img
                            src={previewImage}
                            alt={`Photo ${index + 1}`}
                            className='h-48 w-48 object-cover relative mr-1 mt-1'
                          />
                          <button
                            className='absolute top-0 right-0 m-2 p-2 bg-red-500 text-white rounded-full z-10'
                            onClick={() => handleClearCover(index)}>
                            <p className='text-xs'>Remove</p>
                          </button>
                        </div>
                      ))}
                    </>
                  ) : (
                    <div className='w-full min-h-48 justify-start items-center flex'></div>
                  )}
                </div>

                <div className='w-full justify-center items-center flex'>
                  <button
                    className={`py-1 px-4 border ${
                      previewcover.length >= 3
                        ? "border-slate-500 text-slate-500"
                        : "border-pink-500 text-pink-500"
                    } border-pink-500 rounded-full `}
                    disabled={previewcover.length >= 3}
                    onClick={handleUploadClickCover}>
                    {previewcover.length >= 3
                      ? "You are only Allowed 3 Items"
                      : " Add Cover Gallery"}
                  </button>
                </div>
                <input
                  type='file'
                  accept='image/*'
                  multiple
                  onChange={handleFileChangeCover}
                  className='hidden'
                  ref={fileInputRef}
                />
              </div>
            </div>

            <div className='w-full pt-4'>
              <label className='text-sm font-sans text-slate-500'>
                Product
              </label>
              <div className='w-full min-h-24 rounded border p-1 border border-slate-200 items-center'>
                <div className='w-full min-h-20 justify-center flex flex-wrap'>
                  {previewproduct ? (
                    <>
                      <div className='justify-center items-center flex'>
                        <h1 className='font-bold'>
                          {productfileName
                            ? productfileName
                            : "Please select file"}
                        </h1>
                      </div>
                    </>
                  ) : (
                    <div className='w-full min-h-20 justify-start items-center flex'></div>
                  )}
                </div>

                {productfileName ? (
                  <div className='w-full justify-center items-center flex'>
                    <button
                      className='py-1 px-4 border border-pink-500 rounded-full text-pink-500'
                      onClick={() => handleClearProduct()}>
                      Remove
                    </button>
                  </div>
                ) : (
                  <>
                    <div className='w-full justify-center items-center flex'>
                      <button
                        className='py-1 px-4 border border-pink-500 rounded-full text-pink-500'
                        onClick={handleUploadClickProduct}>
                        Upload Product
                      </button>
                    </div>
                    <input
                      type='file'
                      accept='*'
                      multiple
                      onChange={handleFileChangeProduct}
                      className='hidden'
                      ref={fileInputRefPrdouct}
                    />
                  </>
                )}
              </div>
            </div>
            <div className='w-full mr-2 pt-4'>
              <label className='text-sm font-sans text-slate-500'>Price</label>
              <input
                type='number'
                className='w-full rounded border p-1 outline-none text-slate-500'
                value={price}
                onChange={(e) => setPrice(e.target.value)}></input>
            </div>
          </div>
          <div
            className={`w-full items-center justify-center flex pt-4 justify-between flex px-10 ${
              buttonValue === "addProd" ? "" : "hidden"
            }}`}>
            <div
              className={`w-full mr-2 ${
                buttonValue === "addProd" ? "" : "hidden"
              }`}>
              <button
                className='border w-1/5 bg-pink-500 rounded-full px-3 py-2 text-white'
                onClick={() => handleSubmitProduct()}>
                Submit
              </button>
            </div>
          </div>
        </>
      )}

      {/* List of Products */}

      <div
        className={`w-full flex flex-wrap items-center p-4 mb-10 ${
          buttonValue === "addProd" ? "hidden" : ""
        }`}>
        <div className='w-full'>
          <input
            type='text'
            className='w-full border rounded-full border-slate-300 py-1 px-3 mb-3 outline-none'
            value={searchvalue}
            placeholder='Search'
            onChange={(e) => handleFilterProduct(e.target.value)}></input>
        </div>
        {/* Map products  */}
        {productgalery.length > 0 ? (
          productgalery.map((product_profile, index) => (
            <ProductCard
              {...product_profile}
              userId={userdata.userdata.user_id}
              editProduct={handleEditProduct}
              fetchProduct={fetchUsersProduct}
              key={index}
            />
          ))
        ) : (
          <div className='w-full h-96 mr-2 mt-10 mb-2 bg-pink-500 flex items-center justify-center'>
            <div className='text-center'>
              <h1 className='text-4xl text-white font-bold'>
                Start your Journey with us
              </h1>
              <h1 className='text-4xl text-white font-normal mt-5'>
                Build your Products
              </h1>
            </div>
          </div>
        )}
      </div>
    </>
  );
}
