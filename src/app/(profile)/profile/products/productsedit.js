import React, { useEffect, useState, useRef } from "react";
import {
  c_deleteproduct_cover,
  c_updateproduct_file,
} from "@/app/controller/(profile)/c_products";
import Wysiwyg from "../components/wysiwyg";

export default function ProductsEdit({
  dbProductId,
  dbProductName,
  dbProductDescription,
  dbPrice,
  hideEdit,
  dbProductCover,
  dbProductFile,
  dbUserId,
}) {
  const fileInputRef = useRef(null);
  const fileInputRefPrdouct = useRef(null);
  const [productDescription, setProductDescription] = useState("");

  //Cover Photo Variable
  const [previewcover, setPreviewCover] = useState([]);
  const [coverfile, setCoverFile] = useState([]);

  // Product Variable
  const [productfileName, setProductFileName] = useState("");
  const [productfile, setProductFile] = useState("");

  //Data from Database

  const [dbproductId, setdbProductId] = useState("");
  const [dbproductName, setdbProductName] = useState("");
  const [dbproductDescription, setdbProductDescription] = useState("");
  const [dbprice, setdbPrice] = useState("");
  const [dbproductCover, setdbProductCover] = useState([]);
  const [dbproductFileName, setdbProductFileName] = useState("");
  const [dbuserId, setdbUserId] = useState("");
  const [dbproductFileId, setdbProductFileId] = useState("");

  //   const changeContent = (value) => {
  //     getButtonValue(value);
  //   };

  // Cover Photo
  const handleUploadClickCover = () => {
    fileInputRef.current.click();
  };

  const handleFileChangeCover = (e) => {
    const file = e.target.files[0]; // Get the first file from the selected files
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        // Check if the number of previews is less than six
        if (previewcover.length < 3) {
          // Use functional update to ensure state is updated correctly
          setPreviewCover((prevPreviews) => [...prevPreviews, reader.result]);
          setCoverFile((prevPreviews) => [...prevPreviews, file]);
        } else {
          // Alert the user or handle the case where the limit is exceeded
          alert("You can only upload up to six images.");
        }
      };
      reader.readAsDataURL(file);
    }
  };

  const handleClearCover = (index) => {
    setPreviewCover((prevPreviews) => {
      const updatedPreviews = [...prevPreviews];
      updatedPreviews.splice(index, 1);
      return updatedPreviews;
    });

    setCoverFile((prevPreviews) => {
      const updatedPreviews = [...prevPreviews];
      updatedPreviews.splice(index, 1);
      return updatedPreviews;
    });
  };

  // Add Delete Query
  const handleClearDbCover = (index, product_c_id) => {
    c_deleteproduct_cover(product_c_id)
      .then((response) => {
        if (response.code == "1111") {
          setdbProductCover((prevPreviews) => {
            const updatedPreviews = [...prevPreviews];
            updatedPreviews.splice(index, 1);
            return updatedPreviews;
          });
          alert("Deleted");
        } else {
          alert(response.message);
        }
      })
      .catch((error) => {
        alert("Error in deleting cover photo:", error);
      });
  };

  // Product
  const handleUploadClickProduct = () => {
    setdbProductFileName("");
    fileInputRefPrdouct.current.click();
  };

  const handleFileChangeProduct = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        const fileName = file.name;
        setProductFileName(fileName);
        setProductFile(file);
      };
      reader.readAsDataURL(file);
    }
  };

  const handleClearDbProduct = (product_f_id) => {
    setdbProductFileName("");
    setdbProductFileId(product_f_id);
  };
  const handleClearProduct = () => {
    setProductFileName("");
  };

  const handleContentWysiwyg = (value) => {
    setdbProductDescription(value);
  };

  const handleSubmitProduct = () => {
    c_updateproduct_file(
      dbproductId,
      dbproductName,
      dbproductDescription,
      dbprice,
      coverfile,
      productfile,
      dbuserId,
      dbproductFileId
    )
      .then((response) => {
        if (response.code == "1111") {
          alert(response.message);
          hideEdit(false);
        } else {
          alert(response.message);
        }
      })
      .catch((error) => {
        console.log("Error", error);
      });
  };

  const handleHideEdit = () => {
    hideEdit(false);
  };

  useEffect(() => {
    setdbUserId(dbUserId);
    setdbProductId(dbProductId);
    setdbProductName(dbProductName);
    setProductDescription(dbProductDescription);
    setdbPrice(dbPrice);
    setdbProductCover(dbProductCover);
    if (dbProductFile) {
      const pathParts = dbProductFile[0].product_file.split("/");
      const fileNameWithExtension = pathParts[pathParts.length - 1];
      setdbProductFileName(fileNameWithExtension);
    } else {
      setdbProductFileName("");
    }
  }, [dbProductName, dbProductDescription, dbPrice, dbProductCover]);

  return (
    <>
      <div className={`w-full pt-4 px-10`}>
        <div className='w-full flex'>
          <div className='w-6/12'>
            <h1 className='text-2xl text-slate-400'>Edit Product</h1>
          </div>
          <div className='w-6/12 justify-end flex'>
            <button
              className='p-1 bg-orange-600 px-2 rounded-full text-white'
              onClick={() => handleHideEdit()}>
              Cancel Edit
            </button>
          </div>
        </div>
        <div className='w-full mr-2 pt-4'>
          <label className='text-sm font-sans text-slate-500'>
            Product Name
          </label>
          <input
            type='text'
            className='w-full rounded border p-1 outline-none text-slate-500'
            value={dbproductName}
            onChange={(e) => setdbProductName(e.target.value)}></input>
        </div>
        <div className='w-full pt-4'>
          <Wysiwyg
            content={handleContentWysiwyg}
            label={"Product Description"}
            returnData={productDescription}
          />
        </div>
        <div className='w-full pt-4'>
          <label className='text-sm font-sans text-slate-500'>
            Product Cover
          </label>
          <div className='w-full min-h-60 rounded border p-1 border border-slate-200 items-center'>
            <div className='w-full min-h-48 justify-center p-1 flex flex-wrap'>
              {dbproductCover && dbproductCover.length > 0 ? (
                <>
                  {dbproductCover.map((cover_file, index) => (
                    <div className='relative' key={index}>
                      <img
                        src={`/${cover_file.product_cover.replace(
                          /^\.\/public\//,
                          ""
                        )}`}
                        alt={`Photo ${index + 1}`}
                        className='h-48 w-48 object-cover relative mr-1 mt-1'
                      />
                      <button
                        className='absolute top-0 right-0 m-2 p-2 bg-red-500 text-white rounded-full z-10'
                        onClick={() =>
                          handleClearDbCover(index, cover_file.product_c_id)
                        }>
                        <p className='text-xs'>Remove</p>
                      </button>
                    </div>
                  ))}
                </>
              ) : (
                ""
              )}
              {previewcover && previewcover.length > 0 ? (
                <>
                  {previewcover.map((cover_file, index) => (
                    <div className='relative' key={index}>
                      <img
                        src={`${cover_file.replace(/^\.\/public\//, "")}`}
                        alt={`Photo ${index + 1}`}
                        className='h-48 w-48 object-cover relative mr-1 mt-1'
                      />
                      <button
                        className='absolute top-0 right-0 m-2 p-2 bg-red-500 text-white rounded-full z-10'
                        onClick={() => handleClearCover(index)}>
                        <p className='text-xs'>Remove</p>
                      </button>
                    </div>
                  ))}
                </>
              ) : (
                ""
              )}
            </div>

            <div className='w-full justify-center items-center flex'>
              <button
                className={`py-1 px-4 border ${
                  dbproductCover.length + previewcover.length >= 3
                    ? "border-slate-500 text-slate-500"
                    : "border-pink-500 text-pink-500"
                } border-pink-500 rounded-full `}
                disabled={dbproductCover.length >= 3}
                onClick={handleUploadClickCover}>
                {dbproductCover.length >= 3
                  ? "You are only Allowed 3 Items"
                  : " Add Cover Gallery"}
              </button>
            </div>
            <input
              type='file'
              accept='image/*'
              multiple
              onChange={handleFileChangeCover}
              className='hidden'
              ref={fileInputRef}
            />
          </div>
        </div>

        <div className='w-full pt-4'>
          <label className='text-sm font-sans text-slate-500'>Product</label>
          <div className='w-full min-h-24 rounded border p-1 border border-slate-200 items-center'>
            <div className='w-full min-h-20 justify-center flex flex-wrap'>
              {dbproductFileName ? (
                <>
                  <div className='justify-center items-center flex pt-5 pb-5'>
                    <h1 className='font-bold'>
                      {dbproductFileName
                        ? dbproductFileName
                        : "No file exist.."}
                    </h1>
                  </div>
                  <div className='w-full justify-center items-center flex'>
                    <button
                      className='py-1 px-4 border border-pink-500 rounded-full text-pink-500 mb-5'
                      onClick={() =>
                        handleClearDbProduct(dbProductFile[0].product_f_id)
                      }>
                      Remove
                    </button>
                  </div>
                </>
              ) : (
                <>
                  <div className='justify-center items-center flex pt-5 pb-5'>
                    <h1 className='font-bold'>
                      {productfileName ? productfileName : "Please select file"}
                    </h1>
                  </div>

                  {productfileName ? (
                    <div className='w-full justify-center items-center flex'>
                      <button
                        className='py-1 px-4 border border-pink-500 rounded-full text-pink-500 mb-5'
                        onClick={() => handleClearProduct()}>
                        Remove
                      </button>
                    </div>
                  ) : (
                    <>
                      <div className='w-full justify-center items-center flex'>
                        <button
                          className='py-1 px-4 border border-pink-500 rounded-full text-pink-500 mb-5'
                          onClick={handleUploadClickProduct}>
                          Upload Product
                        </button>
                      </div>
                      <input
                        type='file'
                        accept='*'
                        multiple
                        onChange={handleFileChangeProduct}
                        className='hidden'
                        ref={fileInputRefPrdouct}
                      />
                    </>
                  )}
                </>
              )}
            </div>
          </div>
        </div>
        <div className='w-full mr-2 pt-4'>
          <label className='text-sm font-sans text-slate-500'>Price</label>
          <input
            type='number'
            className='w-full rounded border p-1 outline-none text-slate-500'
            value={dbprice}
            onChange={(e) => setdbPrice(e.target.value)}></input>
        </div>
      </div>

      <div
        className={`w-full items-center justify-center flex pt-4 justify-between flex px-10`}>
        <div className={`w-full mr-2 `}>
          <button
            className='border w-1/5 bg-orange-500 rounded-full px-3 py-2 text-white'
            onClick={() => handleSubmitProduct()}>
            Update
          </button>
        </div>
      </div>

      {/* List of Products */}
    </>
  );
}
