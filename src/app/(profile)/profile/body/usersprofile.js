import Link from "next/link";
import { useEffect } from "react";

export default function UsersDetails({
  userFullName,
  userLocation,
  userHeadline,
  userSkills,
  userAbout,
  userLink,
}) {
  const SkillsCard = ({ skill }, index) => (
    <>
      <button
        key={index}
        className='p-2 border border-pink-500 rounded rounded-full text-sm m-1'>
        {skill}
      </button>
    </>
  );
  useEffect(() => {}, []);
  return (
    <>
      <div className='w-full items-center justify-center flex pt-4'>
        <h5 className='text-2xl font-sans'>{userFullName}</h5>
      </div>
      <div className='w-full items-center justify-center flex mt-2'>
        <h5 className='text-xs font-sans text-slate-500'>
          <Link
            className='text-orange-500'
            href={`${window.location.origin}/talent/${userLink}`}>
            {`${window.location.origin}/talent/${userLink}`}
          </Link>
        </h5>
      </div>
      <div className='w-full items-center justify-center flex'>
        <h5 className='text-xs font-sans text-slate-500'>
          {userLocation}, Philippines
        </h5>
      </div>
      <div className='w-full items-center justify-center flex pt-4'>
        <p className='text-sm font-sans text-center text-slate-700'>
          {userHeadline}
        </p>
      </div>
      <div className='w-full items-center justify-center flex pt-12'>
        <h2 className='text-lg font-sans text-center text-slate-700'>Skills</h2>
      </div>
      <div className='w-full items-center justify-center text-wrap pt-5'>
        {userSkills.map((skill, index) => (
          <SkillsCard skill={skill} key={index} />
        ))}
      </div>
      <div className='w-full items-center justify-center flex pt-12'>
        <h2 className='text-lg font-sans text-center text-slate-700'>About</h2>
      </div>
      <div className='w-full items-center justify-center flex pt-4'>
        <div
          className='text-sm font-sans text-center text-slate-700'
          dangerouslySetInnerHTML={{ __html: userAbout }}
        />
      </div>
    </>
  );
}
