"use client";
import React, { useEffect, useState, useRef } from "react";
import UsersProfileFields from "../myprofilefields/myprofilefields";
import UsersProfileDetails from "./usersprofile";
import UsersProducts from "../products/products";
import UsersServices from "../services/services";
import UsersMessages from "../messages/messages";
import UsersJobs from "../jobs/jobs";
import { c_getprofile } from "@/app/controller/(profile)/c_userprofile";
import UploadUserPhoto from "../components/uploaduserphoto";
import { useRouter } from "next/navigation";
import { useSearchParams } from "next/navigation";
import Image from "next/image";

export default function BodyProfile({ prop }) {
  const router = useRouter();
  const searchParams = useSearchParams();
  const page = searchParams.get("page");
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [location, setLocation] = useState("");
  const [headline, setHeadLine] = useState("");
  const [skills, setSkills] = useState([]);
  const [about, setAbout] = useState("");
  const [userid, setUserId] = useState("");
  const [photopath, setPhotoPath] = useState("");
  const [userLink, setUserLink] = useState("");
  const [userdata, setUsersData] = useState([]);
  const [isPanelOpen, setIsPanelOpen] = useState(true);
  const [hideShowButton, setHideShowButton] = useState(false);
  const panelContentRef = useRef(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const scrollDown = () => {
    setTimeout(() => {
      if (panelContentRef.current) {
        panelContentRef.current.scrollTo({
          top: panelContentRef.current.scrollHeight,
          behavior: "smooth",
        });
      }
    }, 100);
  };
  const scrollUp = () => {
    setTimeout(() => {
      if (panelContentRef.current) {
        panelContentRef.current.scrollTo({
          top: 0,
          behavior: "smooth",
        });
      }
    }, 100);
  };
  const getButtonValue = (value) => {
    router.push(`/profile?page=${value}`);
    if (value === "pfle") {
      setIsPanelOpen(true);
    } else {
      setIsPanelOpen(false);
    }
  };
  const fetchUsersProfile = async () => {
    c_getprofile()
      .then((value) => {
        setUserId(value.rows[0].user_id);
        setEmail(value.rows[0].useremail);
        setName(`${value.rows[0].username} ${value.rows[0].userlname}`);
        setLocation(
          `${JSON.parse(value.rows[0].usermuni).label}, ${
            JSON.parse(value.rows[0].userprov).label
          }, Philippines`
        );
        setHeadLine(value.rows[0].userheadline);
        setSkills(value.rows[0].userskills);
        setAbout(value.rows[0].userabout);
        setPhotoPath(value.rows[0].userprofilepic);
        setUserLink(value.rows[0].user_link);
        setUsersData(value.rows[0]);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };
  const handleShowPanel = () => {
    setIsPanelOpen(!isPanelOpen);
    getButtonValue("pfle");
  };
  const handleClosePanel = () => {
    setIsPanelOpen(!isPanelOpen);
    getButtonValue("mypdct");
  };
  const handleHideShowButton = (value) => {
    setHideShowButton(value);
  };

  useEffect(() => {
    fetchUsersProfile();
  }, [page]);

  useEffect(() => {
    if (!page) {
      setIsPanelOpen(true);
      if (userid) {
        router.push(`/profile?page=mypdct`);
      } else {
        router.push(`/profile?page=pfle`);
      }
      setTimeout(() => {
        setIsPanelOpen(false);
        router.push(`/profile?page=mypdct`);
      }, 3000);
    } else {
      setIsPanelOpen(false);
    }
  }, []);

  if (loading) {
    return (
      <div className='flex items-center justify-center w-full mt-10'>
        <div className='relative flex flex-col items-center space-y-1'>
          <div className='w-36 h-36 border-4 border-t-4 border-r-4 border-b-4 border-l-4 border-t-red-500 border-r-blue-500 border-b-green-500 border-l-yellow-500 rounded-full animate-spin'></div>
          <div className='text-lg text-gray-500'>Loading...</div>
        </div>
      </div>
    );
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div className='w-full min-h-screen justify-center items-center flex'>
      <div className='w-9/12 flex'>
        {/* Sliding Panel */}
        <div
          className={`fixed top-0 left-0 transform ${
            isPanelOpen ? "translate-x-0" : "-translate-x-full"
          } transition-transform duration-500 ease-in-out w-4/12 min-h-screen bg-white shadow-lg border-r-2 border-dashed z-50`}
          onMouseEnter={() => handleHideShowButton(true)}
          onMouseLeave={() => handleHideShowButton(false)}>
          <div className='absolute top-4 right-4'>
            <button
              className='bg-pink-500 text-white p-2 rounded-full'
              onClick={() => handleClosePanel()}>
              <Image
                src='/icons/closepanel.svg'
                alt='Icon'
                width={20}
                height={20}
                priority
              />
            </button>
          </div>

          <div
            className='full'
            ref={panelContentRef}
            style={{ maxHeight: "100vh", overflow: "hidden" }}>
            <div className='w-full p-5'>
              <UploadUserPhoto
                userid={userid}
                photopath={photopath}
                name={name}
              />
              {userdata.user_id != null ? (
                <UsersProfileDetails
                  userFullName={name}
                  userEmail={email}
                  userLocation={location}
                  userHeadline={headline}
                  userSkills={skills}
                  userAbout={about}
                  userLink={userLink}
                />
              ) : (
                <div className='w-full max-w-md mx-auto h-auto p-6 mt-10 mb-2 bg-gradient-to-r from-blue-200 via-blue-300 to-blue-400 rounded-lg shadow-lg'>
                  <h1 className='text-3xl text-white font-bold mb-6 text-center'>
                    Let's Start with Your Personal Information
                  </h1>
                  <p className='text-white text-center'>
                    Please provide your personal information to get started.
                  </p>
                </div>
              )}
            </div>
          </div>
          {hideShowButton && (
            <>
              <div className='fixed z-10 bottom-16 right-4'>
                <button
                  className='bg-pink-500 text-white p-2 rounded-full'
                  onClick={scrollUp}
                  style={{ transform: "rotate(180deg)", top: "-50px" }}>
                  <Image
                    src={`/icons/scrollup.svg`}
                    alt={`Icon`}
                    width={20}
                    height={20}
                    priority
                  />
                </button>
              </div>
              <div className='fixed z-10 bottom-4 right-4'>
                <button
                  className='bg-pink-500 text-white p-2 rounded-full'
                  onClick={scrollDown}>
                  <Image
                    src={`/icons/scrolldown.svg`}
                    alt={`Icon`}
                    width={20}
                    height={20}
                    priority
                  />
                </button>
              </div>
            </>
          )}
        </div>

        {/* End Sliding Panel */}

        {/* Toggle Button */}
        <div className='w-3/12 min-h-screen '>
          <div className='w-full p-10 items-center justify-end flex flex-wrap'>
            <button
              className={`${
                page === "pfle" || page === ""
                  ? "bg-pink-500 text-white"
                  : "border border-pink-500"
              } py-2 px-4 w-full rounded rounded-full flex ${
                isPanelOpen ? "hidden" : ""
              } hover:bg-pink-100 ease-in duration-300`}
              onClick={() => handleShowPanel()}>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                fill={`${
                  page === "pfle" ? "currentColor" : "rgb(236, 72, 153)"
                }`}
                viewBox='0 0 24 24'
                strokeWidth='1.5'
                className='w-6 h-6 mr-2'>
                <path d='M15.75 6a3.75 3.75 0 1 1-7.5 0 3.75 3.75 0 0 1 7.5 0ZM4.501 20.118a7.5 7.5 0 0 1 14.998 0A17.933 17.933 0 0 1 12 21.75c-2.676 0-5.216-.584-7.499-1.632Z' />
              </svg>

              <span className=''>
                {isPanelOpen ? "Close Profile" : "Profile"}
              </span>
            </button>

            <button
              className={`${
                page === "mypdct"
                  ? "bg-pink-500 text-white"
                  : "border border-pink-500"
              }  py-2 px-4 w-full rounded rounded-full flex mt-10 hover:bg-pink-100 ease-in duration-300`}
              onClick={() => getButtonValue("mypdct")}
              disabled={userdata.length === 0}>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 24 24'
                fill={`${
                  page === "mypdct" ? "currentColor" : "rgb(236, 72, 153)"
                }`}
                className='w-6 h-6 mr-2'>
                <path d='M7.502 6h7.128A3.375 3.375 0 0 1 18 9.375v9.375a3 3 0 0 0 3-3V6.108c0-1.505-1.125-2.811-2.664-2.94a48.972 48.972 0 0 0-.673-.05A3 3 0 0 0 15 1.5h-1.5a3 3 0 0 0-2.663 1.618c-.225.015-.45.032-.673.05C8.662 3.295 7.554 4.542 7.502 6ZM13.5 3A1.5 1.5 0 0 0 12 4.5h4.5A1.5 1.5 0 0 0 15 3h-1.5Z' />
                <path d='M3 9.375C3 8.339 3.84 7.5 4.875 7.5h9.75c1.036 0 1.875.84 1.875 1.875v11.25c0 1.035-.84 1.875-1.875 1.875h-9.75A1.875 1.875 0 0 1 3 20.625V9.375Zm9.586 4.594a.75.75 0 0 0-1.172-.938l-2.476 3.096-.908-.907a.75.75 0 0 0-1.06 1.06l1.5 1.5a.75.75 0 0 0 1.116-.062l3-3.75Z' />
              </svg>
              <span className=''>Products</span>
            </button>

            <button
              className={`${
                page === "jobs"
                  ? "bg-pink-500 text-white"
                  : "border border-pink-500"
              }  py-2 px-4 w-full rounded rounded-full flex mt-10 hover:bg-pink-100 ease-in duration-300`}
              onClick={() => getButtonValue("jobs")}
              disabled={userdata.length === 0}>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 24 24'
                fill={`${
                  page === "jobs" ? "currentColor" : "rgb(236, 72, 153)"
                }`}
                className='w-6 h-6 mr-2'>
                <path d='M2.25 5.25a3 3 0 0 1 3-3h13.5a3 3 0 0 1 3 3V15a3 3 0 0 1-3 3h-3v.257c0 .597.237 1.17.659 1.591l.621.622a.75.75 0 0 1-.53 1.28h-9a.75.75 0 0 1-.53-1.28l.621-.622a2.25 2.25 0 0 0 .659-1.59V18h-3a3 3 0 0 1-3-3V5.25Zm1.5 0v7.5a1.5 1.5 0 0 0 1.5 1.5h13.5a1.5 1.5 0 0 0 1.5-1.5v-7.5a1.5 1.5 0 0 0-1.5-1.5H5.25a1.5 1.5 0 0 0-1.5 1.5Z' />
              </svg>
              <span className=''>Jobs</span>
            </button>

            <button
              className={`${
                page === "srvs"
                  ? "bg-pink-500 text-white"
                  : "border border-pink-500"
              }  py-2 px-4 w-full rounded rounded-full flex mt-10 hover:bg-pink-100 ease-in duration-300`}
              onClick={() => getButtonValue("srvs")}
              disabled={userdata.length === 0}>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 24 24'
                fill={`${
                  page === "srvs" ? "currentColor" : "rgb(236, 72, 153)"
                }`}
                className='w-6 h-6 mr-2'>
                <path d='M19.902 4.098a3.75 3.75 0 0 0-5.304 0l-4.5 4.5a3.75 3.75 0 0 0 1.035 6.037.75.75 0 0 1-.646 1.353 5.25 5.25 0 0 1-1.449-8.45l4.5-4.5a5.25 5.25 0 1 1 7.424 7.424l-1.757 1.757a.75.75 0 1 1-1.06-1.06l1.757-1.757a3.75 3.75 0 0 0 0-5.304Zm-7.389 4.267a.75.75 0 0 1 1-.353 5.25 5.25 0 0 1 1.449 8.45l-4.5 4.5a5.25 5.25 0 1 1-7.424-7.424l1.757-1.757a.75.75 0 1 1 1.06 1.06l-1.757 1.757a3.75 3.75 0 1 0 5.304 5.304l4.5-4.5a3.75 3.75 0 0 0-1.035-6.037.75.75 0 0 1-.354-1Z' />
              </svg>
              <span className=''>Services</span>
            </button>

            <button
              className={`${
                page === "msg"
                  ? "bg-pink-500 text-white"
                  : "border border-pink-500"
              }  py-2 px-4 w-full rounded rounded-full flex mt-10 hover:bg-pink-100 ease-in duration-300`}
              onClick={() => getButtonValue("msg")}
              disabled={userdata.length === 0}>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                viewBox='0 0 24 24'
                fill={`${
                  page === "msg" ? "currentColor" : "rgb(236, 72, 153)"
                }`}
                className='w-6 h-6 mr-2'>
                <path d='M5.337 21.718a6.707 6.707 0 0 1-.533-.074.75.75 0 0 1-.44-1.223 3.73 3.73 0 0 0 .814-1.686c.023-.115-.022-.317-.254-.543C3.274 16.587 2.25 14.41 2.25 12c0-5.03 4.428-9 9.75-9s9.75 3.97 9.75 9c0 5.03-4.428 9-9.75 9-.833 0-1.643-.097-2.417-.279a6.721 6.721 0 0 1-4.246.997Z' />
              </svg>
              <span className=''>Messages</span>
            </button>
          </div>
        </div>

        {/* Users Profile Field */}
        <div className={`${isPanelOpen ? "w-8/12" : "w-full"}  min-h-screen`}>
          {page === "pfle" || page === false ? (
            <UsersProfileFields
              prop={prop}
              fetchUsersProfile={fetchUsersProfile}
              userdata={userdata}
            />
          ) : page === "mypdct" && userdata ? (
            <UsersProducts userdata={userdata} />
          ) : page === "jobs" && userdata ? (
            <UsersJobs userdata={userdata} />
          ) : page === "srvs" && userdata ? (
            <UsersServices userdata={userdata} />
          ) : page === "msg" && userdata ? (
            <UsersMessages userdata={userdata} />
          ) : (
            <UsersProfileFields
              prop={prop}
              fetchUsersProfile={fetchUsersProfile}
              userdata={userdata}
            />
          )}
        </div>
        {/* End Users Profile Field */}
      </div>
    </div>
  );
}
