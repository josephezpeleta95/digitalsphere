"use client";
import Header from "@header/header";
import Footer from "@footer/footer";
import Body from "./body/body";
import { SessionProvider } from "next-auth/react";
import Protected from "../../controller/protected";
import Error from "./error";
export default function LoginPage() {
  return (
    <main className='flex flex-col items-center justify-between bg-white'>
      <SessionProvider>
        <Protected>
          <Header />
          <Body fallback={<Error />} />
          {/* <TopFooter /> */}
          <Footer />
        </Protected>
      </SessionProvider>
    </main>
  );
}
