import React, { useEffect, useState, useRef } from "react";
import { useSearchParams } from "next/navigation";
import {
  c_submitmessage,
  c_getmessages,
  c_getmessagethread,
} from "@/app/controller/(profile)/c_messages";
import { decrypt } from "../../../lib/crypt";
import { useRouter } from "next/navigation";

export default function Messages({ userdata }) {
  const router = useRouter();
  const scrollableDivRef = useRef(null);
  const searchParams = useSearchParams();
  const sub = searchParams.get("subject") || "";
  const xndTo = searchParams.get("send_to") || "";
  const rxId = searchParams.get("xdto") || "";
  const crxCode = searchParams.get("crx") || "";
  const [subject, setSubject] = useState("");
  const [sendTo, setSendto] = useState("");
  const [recipientId, setRecipientId] = useState("");
  const [message, setMessage] = useState("");
  const [threadId, setThreadId] = useState("");
  const [messageThread, setMessageThread] = useState([]);
  const [viewMessageThread, setViewMessageThread] = useState([]);
  const [cryxcode, setCryxCode] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  function formatTimestamp(timestamp) {
    const date = new Date(timestamp);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0");
    const day = String(date.getDate()).padStart(2, "0");
    const hours = String(date.getHours()).padStart(2, "0");
    const minutes = String(date.getMinutes()).padStart(2, "0");
    const seconds = String(date.getSeconds()).padStart(2, "0");
    const formattedDate = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
    return formattedDate;
  }

  const handleViewMessage = (messageThreadId) => {
    c_getmessagethread(messageThreadId)
      .then((value) => {
        setViewMessageThread(value.rows);
        if (value.rows[0].sender_id === userdata.user_id) {
          setRecipientId(value.rows[0].recipient_id);
          setSendto(value.rows[0].recipient_name);
        } else {
          setRecipientId(value.rows[0].sender_id);
          setSendto(value.rows[0].sender_name);
        }
        setThreadId(messageThreadId);
        setSubject(value.rows[0].subject);
        setMessage("");
        fetchMessages();
        // Scroll to bottom
        setTimeout(() => {
          if (scrollableDivRef.current) {
            scrollableDivRef.current.scrollTop =
              scrollableDivRef.current.scrollHeight;
          }
        }, 100);

        router.push(`/profile?page=msg`);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const submitMessages = () => {
    c_submitmessage(recipientId, threadId, subject, message)
      .then((response) => {
        if (response.code === "1111") {
          fetchMessages();
          handleViewMessage(threadId);
          router.push(`/profile?page=msg`);
          alert("Message Sent..");
          setTimeout(() => {
            if (scrollableDivRef.current) {
              scrollableDivRef.current.scrollTop =
                scrollableDivRef.current.scrollHeight;
            }
          }, 100);
        } else {
          alert("Error on Sending");
        }
      })
      .then((error) => {
        console.log(error);
      });
  };

  const handleSubmitMessage = () => {
    if (crxCode) {
      if (subject === cryxcode[0] && recipientId === cryxcode[1]) {
        submitMessages();
      } else {
        alert("Opps, There is changes on the data...");
      }
    } else {
      submitMessages();
    }
  };

  const fetchMessages = () => {
    c_getmessages()
      .then((value) => {
        setMessageThread(value.rows);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const handleCloseMessageThread = () => {
    setViewMessageThread([]);
    setSubject("");
    setSendto("");
  };

  useEffect(() => {
    if (crxCode) {
      const crxcode = decrypt(crxCode);
      const itemsCrxcode = crxcode.split(":");
      setCryxCode(itemsCrxcode);
    }
    setSubject(sub);
    setSendto(xndTo);
    setRecipientId(rxId);
    fetchMessages();
  }, [sub, xndTo, rxId]);

  if (loading) {
    return (
      <div className='flex items-center justify-center w-full mt-10'>
        <div className='relative flex flex-col items-center space-y-1'>
          <div className='w-36 h-36 border-4 border-t-4 border-r-4 border-b-4 border-l-4 border-t-red-500 border-r-blue-500 border-b-green-500 border-l-yellow-500 rounded-full animate-spin'></div>
          <div className='text-lg text-gray-500'>Loading...</div>
        </div>
      </div>
    );
  }

  if (error) {
    return <div>Error: {error}</div>;
  }
  return (
    <>
      <div className='w-6/12 flex flex-wrap items-center px-4 mt-10'>
        <div className='w-full'>
          <input
            type='text'
            className='w-full border rounded-full border-slate-300 py-1 px-3 mb-3 outline-none'
            placeholder='Search'
          />
        </div>
      </div>
      <div className='w-full ml-2 justify-center flex'>
        <div className='w-6/12 p-1'>
          {messageThread.length > 0 ? (
            messageThread.map((messages, index) => (
              <div
                className='w-full min-h-20 border border-slate-300 border-dashed p-1 mt-2'
                key={index}>
                <div className='w-full flex text-sm text-sky-700 p-1'>
                  <h5 className='mr-4 text-xs'>From:</h5>
                  <p className='font-normal text-xs'>{messages.sender_name}</p>
                </div>
                <div className='w-full flex text-sm text-sky-700 p-1'>
                  <h5 className='mr-4 text-xs'>Subject:</h5>
                  <p
                    className={`${
                      messages.is_read === false ? "font-black " : "font-normal"
                    } text-xs`}>
                    {messages.subject}
                  </p>
                </div>
                <div className='w-full flex text-sm text-slate-500 p-1'>
                  <p className='text-xs pb-4'>{messages.message_text}</p>
                </div>
                <div className='w-full flex text-sm text-slate-500 p-1'>
                  <div className='w-8/12 justify-start items-center flex'>
                    <p className='text-xs pb-4'>
                      {formatTimestamp(messages.sent_at)}
                    </p>
                  </div>
                  <div className='w-2/12 justify-end items-center flex'>
                    <img
                      className='w-5 h-5 cursor-pointer'
                      src='/icons/eye.svg'
                      alt='Profile'
                      onClick={() => handleViewMessage(messages.thread_id)}
                    />
                  </div>
                  <div className='w-2/12 justify-end items-center flex pr-4'>
                    <img
                      className='w-5 h-5'
                      src='/icons/delete.svg'
                      alt='Profile'
                    />
                  </div>
                </div>
              </div>
            ))
          ) : (
            <div className='flex flex-col items-center justify-center h-full p-6 bg-gray-50 border border-gray-200 rounded-lg shadow-md'>
              <div className='flex flex-col items-center'>
                <svg
                  className='w-12 h-12 mb-4 text-gray-400'
                  fill='none'
                  stroke='currentColor'
                  viewBox='0 0 24 24'
                  xmlns='http://www.w3.org/2000/svg'>
                  <path
                    strokeLinecap='round'
                    strokeLinejoin='round'
                    strokeWidth='2'
                    d='M7 8h10M7 12h4m-4 4h10M5 6h14a2 2 0 012 2v12a2 2 0 01-2 2H5a2 2 0 01-2-2V8a2 2 0 012-2z'></path>
                </svg>
                <h2 className='mb-2 text-lg font-semibold text-gray-600'>
                  No Messages
                </h2>
                <p className='text-sm text-gray-500'>
                  You have no messages at the moment.
                </p>
              </div>
            </div>
          )}
        </div>
        <div className='relative w-6/12 p-1 min-h-96 border border-slate-300 mt-3'>
          {viewMessageThread.length > 0 && (
            <>
              <div className='absolute fixed top-0 left-0 right-0 bg-slate-800 h-8 items-center opacity-70 text-white flex justify-end text-sm px-2'>
                <button
                  className='bg-blue-800 px-2 rounded-full'
                  onClick={() => handleCloseMessageThread()}>
                  Close
                </button>
              </div>
              <div
                className='w-full overflow-y-auto h-64 '
                ref={scrollableDivRef}>
                {viewMessageThread.map((message, index) => (
                  <div key={index} className='w-full text-sm p-1'>
                    <div className='w-full flex'>
                      <p className='font-semibold'>{message.sender_name}</p>
                    </div>
                    <div
                      className={`w-8/12 p-1 items-center flex flex-wrap ${
                        message.message_text.length > 0
                          ? message.sender_id === userdata.user_id
                            ? "bg-sky-600 text-white rounded-lg"
                            : "bg-slate-500 text-white rounded-lg border"
                          : ""
                      }`}>
                      <p className={`text-xs`}>{message.message_text}</p>
                    </div>
                    <div className='w-full flex p-1 rounded-full'>
                      <p className='text-xs pb-4 font-light'>
                        {formatTimestamp(message.sent_at)}
                      </p>
                    </div>
                  </div>
                ))}
              </div>
            </>
          )}

          {sub && xndTo && rxId && (
            <div className='w-full mb-4 p-2 bg-blue-100 border border-blue-300 rounded text-blue-700'>
              <p>
                You are sending a message to <strong>{sendTo}</strong> regarding
                the subject: <strong>{subject}</strong>.
              </p>
            </div>
          )}
          {userdata.user_id === rxId && (
            <div className='w-full mb-4 p-2 bg-red-100 border border-red-300 rounded text-red-700'>
              <p>You cannot send a message to yourself.</p>
            </div>
          )}
          <div className='w-full min-h-28'>
            <div className='w-full mb-2'>
              <label className='text-xs text-slate-500'>Sent To:</label>
              <input
                type='text'
                value={sendTo}
                className='w-full rounded border p-1 outline-none text-slate-500 text-xs'
                onChange={(e) => setSendto(e.target.value)}
                readOnly={sendTo !== ""}
              />
            </div>

            <div className='w-full mb-2'>
              <label className='text-xs text-slate-500'>Subject:</label>
              <input
                type='text'
                value={subject}
                className='w-full rounded border p-1 outline-none text-slate-500 text-xs'
                onChange={(e) => setSubject(e.target.value)}
                readOnly={subject !== ""}
              />
            </div>

            <textarea
              className='w-full h-40 rounded border p-1 outline-none text-slate-500 text-xs'
              value={message}
              onChange={(e) => setMessage(e.target.value)}></textarea>

            <img
              className='w-7 h-7 ml-4 mt-2 mb-2 cursor-pointer'
              src='/icons/send.svg'
              alt='Send'
              onClick={() => handleSubmitMessage()}
            />
          </div>
        </div>
      </div>
    </>
  );
}
