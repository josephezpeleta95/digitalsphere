import React, { useState, useRef, useEffect } from "react";
import { c_uploaduserphoto } from "../../../controller/(profile)/c_userprofile";
import Image from "next/image";

export default function UploadUserPhoto({ userid, photopath, name }) {
  // Upload Photo
  const fileInputRef = useRef(null);
  const [image, setImage] = useState(null);
  const [preview, setPreview] = useState(null);
  const userPhoto = `/photos/${userid}/avatar.jpg`;

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        setImage(file);
        setPreview(reader.result);
      };
      reader.readAsDataURL(file);
    }
  };
  const handleSave = () => {
    c_uploaduserphoto(image, userid)
      .then((response) => {
        if (response.code === "1111") {
          window.location.reload();
          alert(response.message);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleUploadClick = () => {
    fileInputRef.current.click();
  };
  const handleClearPhoto = () => {
    setPreview(null);
    setImage(null);
  };

  useEffect(() => {}, [photopath]);
  // End Upload Photo;
  return (
    <>
      <div className='flex flex-col items-center'>
        <div className='w-full items-center justify-center flex'>
          <div className='w-60 bg-slate-100 h-60 rounded rounded-full relative border border-slate-200'>
            {preview ? (
              <>
                <Image
                  src={preview}
                  alt='Photo'
                  className='w-full h-full object-cover rounded-full'
                  width={500}
                  height={500}
                  priority
                />
                <button
                  className='absolute top-0 right-0 m-2 p-2 bg-red-500 text-white rounded-full z-10'
                  onClick={() => handleClearPhoto()}>
                  <p className='text-xs'>Remove</p>
                </button>
              </>
            ) : (
              <>
                {photopath ? (
                  <Image
                    src={userPhoto}
                    alt='Users Photo'
                    className='w-full h-full object-cover rounded-full absolute'
                    width={500}
                    height={500}
                    priority
                  />
                ) : (
                  <Image
                    src={"/images/others/avatar.png"}
                    alt='Avatar'
                    className='w-full h-full object-cover rounded-full'
                    width={500}
                    height={500}
                    priority
                  />
                )}
              </>
            )}
          </div>
        </div>
      </div>

      {name ? (
        <div className='w-full flex justify-center item-center mt-4'>
          <div>
            {image ? (
              <button
                className='py-2 px-4 border border-pink-500 rounded-full bg-pink-500 text-white'
                onClick={handleSave}>
                Save Photo
              </button>
            ) : (
              <>
                <button
                  className='py-2 px-4 border border-pink-500 rounded-full text-pink-500'
                  onClick={handleUploadClick}>
                  Choose Photo
                </button>
                <input
                  type='file'
                  accept='image/*'
                  onChange={handleFileChange}
                  className='hidden'
                  ref={fileInputRef}
                />
              </>
            )}
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
}
