import React, { useEffect, useRef } from "react";

export default function Wysiwyg({ content, label, returnData }) {
  const contentEditableRef = useRef(null);

  const handleInput = (e) => {
    // Check if the Enter key is pressed
    if (e.key === "Enter") {
      // Prevent default behavior of the Enter key
      e.preventDefault();

      // Insert a <p> tag with a line break
      document.execCommand("insertHTML", false, "<p><br></p>");
    } else if (contentEditableRef.current) {
      // Get the innerHTML content from the ref
      const newContent = contentEditableRef.current.innerText;

      // Get the type of data
      const type = typeof newContent;
      // Update the state with the type info
    }
    content(e.target.innerHTML);
  };

  const applyFormat = (format) => {
    document.execCommand(format, false, null);
  };

  const insertLink = () => {
    const url = prompt("Enter the URL:");
    if (url) {
      document.execCommand("createLink", false, url);
    }
  };

  const insertImage = () => {
    const url = prompt("Enter the image URL:");
    if (url) {
      document.execCommand("insertImage", false, url);
    }
  };

  useEffect(() => {
    // Check if the contentEditableRef is available
    if (contentEditableRef.current) {
      // Insert HTML content after component mount
      contentEditableRef.current.innerHTML = returnData;
      content(contentEditableRef.current.innerHTML);
    }
  }, [returnData]);
  return (
    <>
      <div className='w-full mr-2'>
        <label className='text-sm font-sans text-slate-500'>{label}</label>
        {/*  */}
        <div className='p-4 max-w-full  bg-white shadow-md rounded-md'>
          <div className='flex justify-end mb-2'>
            <button
              className='mr-2 bg-gray-200 hover:bg-gray-300 px-1 py-1 rounded-md'
              onClick={() => applyFormat("bold")}>
              <svg
                className='h-5 w-5 text-pink-600'
                width='24'
                height='24'
                viewBox='0 0 24 24'
                strokeWidth='2'
                stroke='currentColor'
                fill='none'
                strokeLinecap='round'
                strokeLinejoin='round'>
                {" "}
                <path stroke='none' d='M0 0h24v24H0z' />{" "}
                <path d='M7 5h6a3.5 3.5 0 0 1 0 7h-6z' />{" "}
                <path d='M13 12h1a3.5 3.5 0 0 1 0 7h-7v-7' />
              </svg>
            </button>
            <button
              className='mr-2 bg-gray-200 hover:bg-gray-300 px-2 py-1 rounded-md'
              onClick={() => applyFormat("italic")}>
              <svg
                className='h-5 w-5 text-pink-600'
                width='24'
                height='24'
                viewBox='0 0 24 24'
                strokeWidth='2'
                stroke='currentColor'
                fill='none'
                strokeLinecap='round'
                strokeLinejoin='round'>
                {" "}
                <path stroke='none' d='M0 0h24v24H0z' />{" "}
                <line x1='11' y1='5' x2='17' y2='5' />{" "}
                <line x1='7' y1='19' x2='13' y2='19' />{" "}
                <line x1='14' y1='5' x2='10' y2='19' />
              </svg>
            </button>
            <button
              className='mr-2 bg-gray-200 hover:bg-gray-300 px-2 py-1 rounded-md'
              onClick={() => applyFormat("underline")}>
              <svg
                className='h-5 w-5 text-pink-600'
                viewBox='0 0 24 24'
                fill='none'
                stroke='currentColor'
                strokeWidth='2'
                strokeLinecap='round'
                strokeLinejoin='round'>
                {" "}
                <path d='M6 3v7a6 6 0 0 0 6 6 6 6 0 0 0 6-6V3' />{" "}
                <line x1='4' y1='21' x2='20' y2='21' />
              </svg>
            </button>
            <button
              className='mr-2 bg-gray-200 hover:bg-gray-300 px-2 py-1 rounded-md'
              onClick={() => applyFormat("justifyRight")}>
              <svg
                className='h-5 w-5 text-pink-600'
                viewBox='0 0 24 24'
                fill='none'
                stroke='currentColor'
                strokeWidth='2'
                strokeLinecap='round'
                strokeLinejoin='round'>
                {" "}
                <line x1='21' y1='10' x2='7' y2='10' />{" "}
                <line x1='21' y1='6' x2='3' y2='6' />{" "}
                <line x1='21' y1='14' x2='3' y2='14' />{" "}
                <line x1='21' y1='18' x2='7' y2='18' />
              </svg>
            </button>
            <button
              className='mr-2 bg-gray-200 hover:bg-gray-300 px-2 py-1 rounded-md'
              onClick={() => applyFormat("justifyLeft")}>
              <svg
                className='h-5 w-5 text-pink-600'
                viewBox='0 0 24 24'
                fill='none'
                stroke='currentColor'
                strokeWidth='2'
                strokeLinecap='round'
                strokeLinejoin='round'>
                {" "}
                <line x1='17' y1='10' x2='3' y2='10' />{" "}
                <line x1='21' y1='6' x2='3' y2='6' />{" "}
                <line x1='21' y1='14' x2='3' y2='14' />{" "}
                <line x1='17' y1='18' x2='3' y2='18' />
              </svg>
            </button>
            <button
              className='mr-2 bg-gray-200 hover:bg-gray-300 px-2 py-1 rounded-md'
              onClick={() => applyFormat("justifyCenter")}>
              <svg
                className='h-5 w-5 text-pink-600'
                viewBox='0 0 24 24'
                fill='none'
                stroke='currentColor'
                strokeWidth='2'
                strokeLinecap='round'
                strokeLinejoin='round'>
                {" "}
                <line x1='18' y1='10' x2='6' y2='10' />{" "}
                <line x1='21' y1='6' x2='3' y2='6' />{" "}
                <line x1='21' y1='14' x2='3' y2='14' />{" "}
                <line x1='18' y1='18' x2='6' y2='18' />
              </svg>
            </button>
            <button
              className='mr-2 bg-gray-200 hover:bg-gray-300 px-2 py-1 rounded-md'
              onClick={() => applyFormat("justifyFull")}>
              <svg
                className='h-5 w-5 text-pink-600'
                viewBox='0 0 24 24'
                fill='none'
                stroke='currentColor'
                strokeWidth='2'
                strokeLinecap='round'
                strokeLinejoin='round'>
                {" "}
                <line x1='21' y1='10' x2='3' y2='10' />{" "}
                <line x1='21' y1='6' x2='3' y2='6' />{" "}
                <line x1='21' y1='14' x2='3' y2='14' />{" "}
                <line x1='21' y1='18' x2='3' y2='18' />
              </svg>
            </button>

            <button
              className='mr-2 bg-gray-200 hover:bg-gray-300 px-2 py-1 rounded-md'
              onClick={insertLink}>
              <svg
                className='h-5 w-5 text-pink-600'
                width='24'
                height='24'
                viewBox='0 0 24 24'
                strokeWidth='2'
                stroke='currentColor'
                fill='none'
                strokeLinecap='round'
                strokeLinejoin='round'>
                {" "}
                <path stroke='none' d='M0 0h24v24H0z' />{" "}
                <path d='M10 14a3.5 3.5 0 0 0 5 0l4 -4a3.5 3.5 0 0 0 -5 -5l-.5 .5' />{" "}
                <path d='M14 10a3.5 3.5 0 0 0 -5 0l-4 4a3.5 3.5 0 0 0 5 5l.5 -.5' />
              </svg>
            </button>
            <button
              className='mr-2 bg-gray-200 hover:bg-gray-300 px-2 py-1 rounded-md'
              onClick={insertImage}>
              <svg
                className='h-5 w-5 text-pink-600'
                viewBox='0 0 24 24'
                fill='none'
                stroke='currentColor'
                strokeWidth='2'
                strokeLinecap='round'
                strokeLinejoin='round'>
                {" "}
                <rect x='3' y='3' width='18' height='18' rx='2' ry='2' />{" "}
                <circle cx='8.5' cy='8.5' r='1.5' />{" "}
                <polyline points='21 15 16 10 5 21' />
              </svg>
            </button>
          </div>
          <div
            className='border border-gray-300 p-2 rounded-md min-h-40'
            contentEditable
            ref={contentEditableRef}
            onInput={handleInput}></div>
        </div>
        {/*  */}
      </div>
    </>
  );
}
