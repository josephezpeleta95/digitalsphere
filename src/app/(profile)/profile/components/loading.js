import React from "react";

const Loading = () => {
  return (
    <div className='flex items-center justify-center w-full mt-10'>
      <div className='relative flex flex-col items-center space-y-1'>
        <div className='w-36 h-36 border-4 border-t-4 border-r-4 border-b-4 border-l-4 border-t-red-500 border-r-blue-500 border-b-green-500 border-l-yellow-500 rounded-full animate-spin'></div>
        <div className='text-lg text-gray-500'>Loading...</div>
      </div>
    </div>
  );
};

export default Loading;
