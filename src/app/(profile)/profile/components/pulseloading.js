import React from "react";

const PulseLoading = () => {
  return (
    <div className='p-4 max-w-xs mx-auto bg-white shadow-md rounded-md'>
      {/* Circle */}

      <div className='animate-pulse w-full h-full object-cover rounded-full bg-gray-300 h-12 w-12 mb-4 '></div>
      {/* One Line */}
      <div className='animate-pulse bg-gray-300 h-4 rounded w-3/4 mb-4'></div>
      {/* Two Lines */}
      <div className='animate-pulse bg-gray-300 h-4 rounded mb-4'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-5/6 mb-4'></div>
      {/* Four Lines */}
      <div className='animate-pulse bg-gray-300 h-4 rounded w-full mb-2'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-5/6 mb-2'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-3/4 mb-2'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-2/3 mb-2'></div>
    </div>
  );
};

export const CirclePulseLoading = () => {
  return (
    <div className='animate-pulse rounded-full bg-gray-300 h-10 w-10 mb-2'></div>
  );
};

export const OneLinePulseLoading = () => {
  return (
    <div className='animate-pulse bg-gray-300 h-4 rounded w-3/4 mb-4'></div>
  );
};

export const TwoLinesPulseLoading = () => {
  return (
    <>
      <div className='animate-pulse bg-gray-300 h-4 rounded mb-4'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-5/6 mb-4'></div>
    </>
  );
};

export const FourLinesPulseLoading = () => {
  return (
    <>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-full mb-2'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-5/6 mb-2'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-3/4 mb-2'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-2/3 mb-2'></div>
    </>
  );
};

export default PulseLoading;
