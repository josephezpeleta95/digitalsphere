import React, { useEffect, useState, useCallback } from "react";
import Image from "next/image";
export default function Select({
  data,
  type,
  searchresult,
  selectIndex,
  textfieldlabel,
  returnData,
}) {
  const [isFocused, setIsFocused] = useState(false);
  const [filteredItems, setFilteredItems] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [items, setItems] = useState([]);
  const [newreturnData, setreturnData] = useState([]);

  const handleFocus = () => {
    setIsFocused(true);
  };

  const handleSearch = (e) => {
    const term = e.target.value.toLowerCase();
    setSearchTerm(term);
    const filtered = data.filter((datum) =>
      datum.label.toLowerCase().includes(term)
    );
    setFilteredItems(filtered);
  };

  const handlegetSearchValue = useCallback(
    (data) => {
      if (newreturnData.length > 0 && data) {
        if (type === "multiple") {
          const labelExistsInItems = items.some(
            (item) => item.label === data.label
          );

          if (!labelExistsInItems) {
            newreturnData.push(data);
            setItems([...newreturnData]);
          }
        } else {
          if (data) {
            setItems([data]);
          } else {
            setItems([newreturnData]);
          }
        }
      } else {
        if (type === "multiple") {
          if (!items.some((item) => item.label === data.label)) {
            setItems((prevItems) => [...prevItems, data]);
          }
          return;
        } else {
          setItems([data]);
        }
      }

      setIsFocused(false);
    },
    [type, items, newreturnData]
  );

  const handleRemoveItem = (index) => {
    if (items.length > 0) {
      setItems((prevItems) => prevItems.filter((_, i) => i !== index));
    } else {
      setItems(returnData.filter((_, i) => i !== index));
    }
  };

  const handleMouseLeave = () => {
    setSearchTerm("");
    setFilteredItems(data);
    setIsFocused(false);
  };

  useEffect(() => {
    setFilteredItems(data);
  }, [data]);

  useEffect(() => {
    if (items.length > 0) {
      searchresult(items, selectIndex);
    } else {
      searchresult(returnData, selectIndex);
    }
  }, [items, selectIndex]);
  useEffect(() => {
    setItems(returnData);
  }, []);
  return (
    <>
      <div className='w-full relative mr-2'>
        <label className='text-sm font-sans text-slate-500'>
          {textfieldlabel}
        </label>
        <div>
          <div
            className={`w-full rounded border min-h-9 border-slate-200 flex flex-wrap pb-1`}
            onClick={handleFocus}>
            {items.length > 0
              ? // Render items
                items.map((item, index) => (
                  <div
                    key={index}
                    className='border border-pink-500 rounded-full items-center flex  h-6 inline-block ml-1 mt-1 px-2'>
                    <div className='items-center flex'>
                      <p className='mr-1 text-sm'>{item.label}</p>
                      <span
                        onClick={() => handleRemoveItem(index)}
                        className='cursor-pointer text-sm'>
                        <Image
                          src={`./icons/remove.svg`}
                          alt={`Icons`}
                          className='w-5 h-5 object-cover rounded-full'
                          width={15}
                          height={15}
                          priority
                        />
                      </span>
                    </div>
                  </div>
                ))
              : // Render returnData
                returnData.map((item, index) => (
                  <div
                    key={index}
                    className='border border-pink-500 rounded-full items-center h-6 inline-block ml-1 mt-1 px-2'>
                    <div className='flex'>
                      <p className='mr-1 text-sm'>{item.label}</p>
                      <span
                        onClick={() => handleRemoveItem(index)}
                        className='cursor-pointer text-sm'>
                        <Image
                          src={`./icons/remove.svg`}
                          alt={`Icons`}
                          className='w-5 h-5 object-cover rounded-full'
                          width={15}
                          height={15}
                          priority
                        />
                      </span>
                    </div>
                  </div>
                ))}
          </div>

          <div
            className={`w-full max-h-50 min-h-10  absolute z-10 bg-white border-slate-200 border p-2 ${
              isFocused ? "" : "hidden"
            }`}
            onMouseLeave={handleMouseLeave}>
            <div className='w-full flex items-center mb-4'>
              <input
                type='text'
                className={`w-full rounded border z-10 p-1 mr-1 outline-none text-sm text-slate-500`}
                placeholder='Search...'
                value={searchTerm}
                onChange={handleSearch}
              />
              <p
                className={`text-slate-400 text-xs cursor-pointer ${
                  isFocused ? "" : "hidden"
                }`}
                onClick={() => handleMouseLeave()}>
                Close
              </p>
            </div>
            <div className='w-full overflow-y-auto max-h-28 min-h-10 '>
              <ul className='text-sm'>
                {filteredItems.map((item, index) => (
                  <li
                    key={index}
                    className='text-sky-500 mb-1 cursor-pointer hover:text-orange-500'
                    onClick={() =>
                      handlegetSearchValue({
                        id: item.id,
                        label: item.label,
                        value: item.value,
                      })
                    }>
                    {item.label}
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
