import React from "react";

export default function ResumePreview({ fileUrl }) {
  return (
    <>
      <div className='w-full justify-center flex'>
        <div className='w-8/12 px-8 border border-1'>
          <iframe
            src={fileUrl}
            width='100%'
            height='600px'
            title='File Preview'
            style={{
              border: "none",
              borderRadius: "10px",
              boxShadow: "0 4px 6px rgba(0, 0, 0, 0.1)",
              overflow: "hidden",
              transition: "box-shadow 0.3s ease",
            }}
            onMouseEnter={(e) => {
              e.target.style.boxShadow = "0 4px 10px rgba(0, 0, 0, 0.2)";
            }}
            onMouseLeave={(e) => {
              e.target.style.boxShadow = "0 4px 6px rgba(0, 0, 0, 0.1)";
            }}
            seamless
          />
        </div>
      </div>
    </>
  );
}
