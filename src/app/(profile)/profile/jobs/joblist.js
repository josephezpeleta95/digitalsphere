import React, { useState, useEffect, useRef } from "react";
import {
  c_getJobListings,
  c_getBuildResume,
} from "@/app/controller/(profile)/c_jobs";
import Loading from "./../components/loading";
import ApplicationModal from "./jobapplicationmodal";
import { useRouter } from "next/navigation";
import Link from "next/link";

const JobListings = ({ userData }) => {
  const router = useRouter();
  const [loading, setLoading] = useState(true);
  const [jobListings, setJoblistings] = useState([]);
  const [showModal, setShowModal] = useState(false); // State for modal visibility
  const [resumeSubmitted, setResumeSubmitted] = useState(false);
  const [jobId, setJobId] = useState("");
  const [companyId, setCompanyId] = useState(false);
  const contentRef = useRef(null);
  const [currentPage, setCurrentPage] = useState(1);
  const listingsPerPage = 5;

  // Logic for displaying job listings on current page
  const indexOfLastListing = currentPage * listingsPerPage;
  const indexOfFirstListing = indexOfLastListing - listingsPerPage;
  const currentListings = jobListings.slice(
    indexOfFirstListing,
    indexOfLastListing
  );

  // Logic for page navigation
  const paginate = (pageNumber) => setCurrentPage(pageNumber);
  // Function to toggle scroll class
  const toggleScroll = () => {
    const content = contentRef.current;
    if (content && content.scrollHeight > content.clientHeight) {
      // Check if content exists before accessing scrollHeight
      content.classList.add("scrollable");
      content.classList.remove("no-scroll");
    } else if (content) {
      content.classList.add("no-scroll");
      content.classList.remove("scrollable");
    }
  };

  const handleFetchJoblistings = () => {
    c_getJobListings()
      .then((value) => {
        setJoblistings(value.rows);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const handleQuickApply = (job_id, company_id) => {
    setJobId(job_id);
    setCompanyId(company_id);
    setShowModal(true); // Open the modal
  };

  const handleCheckResume = () => {
    c_getBuildResume(userData.user_id)
      .then((value) => {
        if (
          value.rows[0].user_data.certifications == null &&
          value.rows[0].user_data.education == null &&
          value.rows[0].user_data.experience == null &&
          value.rows[0].user_data.projects == null
        ) {
          setResumeSubmitted(false);
        } else {
          setResumeSubmitted(true);
        }
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const handleOptionResume = (option) => {
    router.push(`/profile?page=jobs&&xts=resume&&option=${option}`);
  };

  useEffect(() => {
    handleCheckResume();
    handleFetchJoblistings();
  }, []);

  useEffect(() => {
    toggleScroll();
  }, [jobListings]);

  if (loading) {
    return <Loading />;
  }

  return (
    <div className='container p-6 mx-auto bg-white shadow-lg rounded-lg'>
      <h2 className='text-2xl font-semibold text-slate-700 mb-6'>
        Job Listings
      </h2>
      {/* Job Listings */}
      <div className='grid gap-4'>
        {currentListings.map((listing) => (
          <div key={listing.job_id} className='border p-4 rounded-md shadow-md'>
            <h2 className='text-xl font-semibold mb-2 text-slate-600'>
              {listing.title}
            </h2>
            <p className='text-orange-600 mb-2 text-sm'>
              <span> {listing.company_name}</span>{" "}
              <span>({listing.industry})</span>
            </p>
            <div
              ref={contentRef}
              className='text-sm font-sans text-slate-700 h-52 overflow-auto'
              dangerouslySetInnerHTML={{ __html: listing.description }}
            />
            <div className='sm:flex justify-between items-center mt-10'>
              <div className='sm:w-6/12 w-full'>
                <p className='text-gray-700 mb-1'>
                  <span className='font-semibold'>Location: </span>
                  <span className='font-light'> {listing.job_location}</span>
                </p>
                <p className='text-gray-700 mb-1'>
                  <span className='font-semibold'>Posted: </span>
                  <span className='font-light'>
                    {new Date(listing.date_posted).toLocaleDateString()}
                  </span>
                </p>
              </div>

              <div className='sm:w-6/12 w-full'>
                <p className='text-gray-700 mb-1'>
                  <span className='font-semibold'> Salary: </span>
                  <span className='font-light'> {listing.salary_range}</span>
                </p>
                <p className='text-gray-700 mb-1'>
                  <span className='font-semibold'>Closed Date: </span>

                  <span className='font-light'>
                    {new Date(listing.closed_date).toLocaleDateString()}
                  </span>
                </p>
              </div>
            </div>
            <div>
              <h3 className='font-semibold mt-4 text-slate-600'>
                Skills Required:
              </h3>
              <ul className='flex flex-wrap gap-2 mt-2'>
                {JSON.parse(listing.skills).map((skill, index) => (
                  <li
                    key={index}
                    className='text-gray-700 border border-pink-500 px-2 py-1 rounded-full'>
                    {skill}
                  </li>
                ))}
              </ul>
            </div>
            <div>
              <p className='mb-1 mt-5 text-lg'>
                <span className='font-semibold text-sky-600'>
                  {listing.setup}
                </span>
                {" - "}
                <span className='font-light'>{listing.work_arrangement}</span>
              </p>
            </div>
            <div className='mt-4 flex justify-end text-sky-600'>
              <Link
                href={`/jobs/${listing.job_id}`}
                target='_blank'
                className='sm:w-3/12 text-white  flex justify-end rounded text-xs'>
                {" "}
                <button className=' bg-pink-500 text-white px-4 py-2 rounded text-xs mr-5'>
                  View
                </button>
              </Link>
              {resumeSubmitted ? (
                <button
                  className='sm:w-3/12 w-full bg-blue-500 text-white px-4 py-2 rounded text-xs'
                  onClick={() =>
                    handleQuickApply(listing.job_id, listing.company_id)
                  }>
                  Quick Apply
                </button>
              ) : (
                <button
                  className='sm:w-3/12 w-full bg-blue-500 text-white px-4 py-2 rounded text-xs'
                  onClick={() => handleOptionResume("build")}>
                  Build your resume
                </button>
              )}
            </div>
          </div>
        ))}
      </div>
      {/* Pagination */}
      <div className='mt-8 flex justify-center'>
        <nav className='flex'>
          {[
            ...Array(Math.ceil(jobListings.length / listingsPerPage)).keys(),
          ].map((pageNumber) => (
            <button
              key={pageNumber}
              onClick={() => paginate(pageNumber + 1)}
              className={`mx-1 px-3 py-1 rounded-md ${
                pageNumber + 1 === currentPage
                  ? "bg-blue-500 text-white"
                  : "bg-gray-200 text-gray-700 hover:bg-gray-300"
              }`}>
              {pageNumber + 1}
            </button>
          ))}
        </nav>
      </div>
      {showModal && (
        <ApplicationModal
          setShowModal={setShowModal}
          user_Id={userData.user_id}
          job_Id={jobId}
          company_Id={companyId}
        />
      )}
    </div>
  );
};

export default JobListings;
