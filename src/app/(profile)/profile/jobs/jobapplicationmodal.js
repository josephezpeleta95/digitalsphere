import React, { useState } from "react";
import { c_submitJobApplication } from "@/app/controller/(profile)/c_jobs";

const JobListings = ({ setShowModal, user_Id, job_Id, company_Id }) => {
  const [agreed, setAgreed] = useState(false);

  const handleSubmit = () => {
    // Logic to handle form submission
    if (agreed) {
      c_submitJobApplication(user_Id, job_Id, company_Id)
        .then((response) => {
          if (response.code === "1111") {
            alert(response.message);
            setShowModal(false);
          } else {
            response.message;
          }
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      // Display error message or prevent form submission
      console.log("Please agree to the terms before submitting.");
    }
  };

  return (
    <div className='fixed inset-0 flex items-center justify-center z-50'>
      <div className='absolute inset-0 bg-black opacity-50'></div>
      <div className='w-6/12 bg-white p-8 rounded-lg shadow-lg  z-50'>
        <div className='mt-4'>
          <label className='flex items-center'>
            <input
              type='checkbox'
              checked={agreed}
              onChange={(e) => setAgreed(e.target.checked)}
              className='form-checkbox h-4 w-4 text-indigo-600 transition duration-150 ease-in-out'
            />
            <span className='ml-2 text-sm text-gray-600'>
              By pressing "Agree and Submit," you are allowing Digital Sphere
              platform to share your personal information with the company you
              are applying to.
            </span>
          </label>
        </div>
        <div className='mt-4 flex justify-between'>
          <button
            onClick={() => handleSubmit()}
            className={`${
              agreed
                ? "bg-blue-500 hover:bg-blue-700"
                : "bg-gray-300 cursor-not-allowed"
            } text-white font-bold py-2 px-4 rounded`}
            disabled={!agreed}>
            Agree and Submit
          </button>
          <button
            className='bg-red-500 text-white font-bold py-2 px-4 rounded rounded'
            onClick={() => setShowModal(false)} // Close the modal when clicked
          >
            Cancel
          </button>
        </div>
      </div>
    </div>
  );
};

export default JobListings;
