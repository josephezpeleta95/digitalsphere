import React, { useEffect, useState } from "react";
import { c_getJobApplication } from "@/app/controller/(profile)/c_jobs";
import Loading from "./../components/loading";

const JobApplicationList = ({ userData }) => {
  const [loading, setLoading] = useState(true);
  const [applicationList, setApplicationList] = useState([]);

  const fetchJobApplication = () => {
    c_getJobApplication(userData.user_id)
      .then((value) => {
        setApplicationList(value.rows);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    fetchJobApplication();
  }, []);

  if (loading) {
    return <Loading />;
  }

  return (
    <div className='container p-6 mx-auto bg-white shadow-lg rounded-lg'>
      <h2 className='text-2xl font-semibold text-slate-700 mb-6'>
        Job Applications
      </h2>
      <div className='grid grid-cols-1 gap-4'>
        {applicationList.length > 0 ? (
          applicationList.map((application) => (
            <div
              key={application.application_id}
              className='bg-white shadow-md rounded-md p-4'>
              <div className='flex justify-between mb-2'>
                <h3 className='text-2xl font-bold text-slate-600'>
                  {application.title}
                </h3>
                <span
                  className={`text-sm font-semibold border py-1 px-2 rounded-full ${
                    application.status === "Accepted"
                      ? "text-green-500 border-green-500"
                      : ""
                  } ${
                    application.status === "Rejected"
                      ? "text-red-500 border-red-500"
                      : ""
                  } ${
                    application.status === "Submitted"
                      ? "text-blue-500 border-blue-500"
                      : ""
                  }`}>
                  {application.status}
                </span>
              </div>
              <p className='text-sky-600 mb-2 text-lg'>
                {application.company_name}
              </p>
              <p className='text-gray-600 mb-2'>
                <span className='font-semibold text-sm'>
                  Date Posted and Closed:{" "}
                </span>
                <span className='font-light text-sm'>
                  {new Date(application.date_posted).toLocaleDateString()}{" "}
                  {" - "}
                  {new Date(application.closed_date).toLocaleDateString()}
                </span>
              </p>
              <p className='text-gray-600 mb-2'>
                <span className='font-semibold text-sm'>Date Applied: </span>
                <span className='font-light text-sm'>
                  {new Date(application.application_date).toLocaleDateString()}
                </span>
              </p>
              <p className='text-gray-600 mb-2'>
                <span className='font-semibold text-sm'>Job Location: </span>
                <span className='font-light text-sm'>
                  {application.job_location}
                </span>
              </p>
              <p className='text-gray-600 mb-2'>
                <span className='font-semibold text-sm'>Salary: </span>
                <span className='font-light text-sm'>
                  {application.salary_range}
                </span>
              </p>
            </div>
          ))
        ) : (
          <div className='flex flex-col items-center justify-center h-full p-6 bg-gray-50 border border-gray-200 rounded-lg shadow-md'>
            <div className='flex flex-col items-center'>
              <svg
                className='w-12 h-12 mb-4 text-gray-400'
                fill='none'
                stroke='currentColor'
                viewBox='0 0 24 24'
                xmlns='http://www.w3.org/2000/svg'>
                <path
                  strokeLinecap='round'
                  strokeLinejoin='round'
                  strokeWidth='2'
                  d='M7 8h10M7 12h4m-4 4h10M5 6h14a2 2 0 012 2v12a2 2 0 01-2 2H5a2 2 0 01-2-2V8a2 2 0 012-2z'></path>
              </svg>
              <h2 className='mb-2 text-lg font-semibold text-gray-600'>
                No Job Applications
              </h2>
              <p className='text-sm text-gray-500'>
                You have no job applications at the moment.
              </p>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default JobApplicationList;
