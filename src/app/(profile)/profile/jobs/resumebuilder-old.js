import React, { useEffect, useState } from "react";
import { c_skills } from "@/app/controller/(main)/c_main";
import {
  c_buildResume,
  c_getBuildResume,
} from "@/app/controller/(profile)/c_jobs";
import { jsPDF } from "jspdf";
import Select from "../components/tailwind-select";
import Image from "next/image";

const ResumeBuilder = ({ userData }) => {
  const [skillsSet, setSkillsSet] = useState([]);
  const [skills, setSkills] = useState([]);
  const [skillsUpdate, setSkillsUpdate] = useState([]);
  const [generatedResume, setGeneratedResume] = useState([]);
  const initialState = {
    personalInfo: {
      firstName: userData?.username || "",
      lastName: userData?.userlname || "",
      email: userData?.useremail || "",
      phone: "",
      address: "",
      city: JSON.parse(userData?.usermuni).label || "",
      state: JSON.parse(userData?.userprov).label || "",
      zip: "",
    },
    education: [
      {
        school: "",
        degree: "",
        fieldOfStudy: "",
        startDate: "",
        endDate: "",
      },
    ],
    experience: [
      {
        company: "",
        position: "",
        startDate: "",
        endDate: "",
        responsibilities: "",
      },
    ],
    projects: [
      {
        title: "",
        description: "",
        link: "",
      },
    ],
  };
  const [formData, setFormData] = useState(initialState);
  const labels = {
    personalInfo: {
      firstName: "First Name",
      lastName: "Last Name",
      email: "Email Address",
      phone: "Phone Number",
      address: "Street Address",
      city: "City/Municipality",
      state: "State/Province",
      zip: "ZIP/Postal Code",
    },
    education: {
      school: "School Name",
      degree: "Degree",
      fieldOfStudy: "Field of Study",
      startDate: "Start Date",
      endDate: "End Date",
    },
    experience: {
      company: "Company Name",
      position: "Job Title",
      startDate: "Start Date",
      endDate: "End Date",
      responsibilities: "Key Responsibilities",
    },
    projects: {
      title: "Project Title",
      description: "Project Description",
      link: "Project Link",
    },
  };
  const handleChange = (section, index, field, value) => {
    const updatedFormData = { ...formData };
    if (section === "skills") {
      updatedFormData[section][index] = value;
    } else if (index !== undefined) {
      updatedFormData[section][index][field] = value;
    } else {
      updatedFormData[section][field] = value;
    }
    setFormData(updatedFormData);
  };
  const addSectionItem = (section) => {
    const updatedFormData = { ...formData };
    const newItem = {};
    if (section === "skills") {
      updatedFormData[section].push("");
    } else {
      Object.keys(formData[section][0]).forEach((key) => (newItem[key] = ""));
      updatedFormData[section].push(newItem);
    }
    setFormData(updatedFormData);
  };
  const removeSectionItem = (section, index) => {
    const updatedFormData = { ...formData };
    updatedFormData[section].splice(index, 1);
    setFormData(updatedFormData);
  };

  const generatePDF = () => {
    const doc = new jsPDF();
    // Set up styles
    const titleFontSize = 24;
    const sectionTitleFontSize = 18;
    const contentFontSize = 12;
    const lineSpacing = 8;
    const sectionSpacing = 10;
    const margin = 20;
    const lineWidth = 0.5;
    const pageHeight = 297; // A4 height in mm
    let yOffset = margin;
    let currentPage = 1;

    const addText = (text, size, yOffset, xOffset = margin) => {
      doc.setFontSize(size);
      doc.text(text, xOffset, yOffset);
      return yOffset + size / 2 + lineSpacing;
    };

    const addLine = (yOffset, xOffset = margin, endX = 210 - margin) => {
      doc.setLineWidth(lineWidth);
      doc.line(xOffset, yOffset, endX, yOffset);
      return yOffset + lineSpacing / 2;
    };

    const checkRemainingSpace = (height) => {
      const remainingSpace = pageHeight - yOffset;
      if (remainingSpace < height) {
        doc.addPage();
        yOffset = margin;
        currentPage++;
      }
    };

    // Title
    yOffset = addText("Resume", titleFontSize, yOffset);
    yOffset += sectionSpacing;

    // Personal Information
    yOffset = addText("Personal Information", sectionTitleFontSize, yOffset);
    yOffset = addLine(yOffset);
    Object.keys(generatedResume.personalInfo).forEach((field) => {
      let labelText = labels.personalInfo[field];
      let value = "";
      if (labelText === "City/Municipality" || labelText === "State/Province") {
        value = JSON.parse(generatedResume.personalInfo[field]).label;
      } else {
        value = generatedResume.personalInfo[field];
      }
      yOffset = addText(`${labelText}: ${value}`, contentFontSize, yOffset);
    });

    yOffset += sectionSpacing;
    checkRemainingSpace(sectionSpacing);

    // Add a section to PDF function
    const addSectionToPDF = (sectionLabel, sectionData, sectionLabels) => {
      yOffset = addText(sectionLabel, sectionTitleFontSize, yOffset);
      yOffset = addLine(yOffset);
      sectionData.forEach((item) => {
        const itemHeight =
          Object.keys(item).length * (contentFontSize + lineSpacing);
        checkRemainingSpace(itemHeight + sectionSpacing);
        Object.keys(item).forEach((field) => {
          yOffset = addText(
            `${sectionLabels[field]}: ${item[field]}`,
            contentFontSize,
            yOffset
          );
        });
        yOffset += lineSpacing; // Add space between items
      });
      yOffset += sectionSpacing; // Add space between sections
    };

    // Education Section
    addSectionToPDF("Education", resumeData.education, labels.education);

    // Work Experience Section
    addSectionToPDF(
      "Work Experience",
      resumeData.experience,
      labels.experience
    );

    // Skills Section
    yOffset = addText("Skills", sectionTitleFontSize, yOffset);
    yOffset = addLine(yOffset);
    resumeData.skills.forEach((skill, index) => {
      yOffset = addText(`${index + 1}. ${skill}`, contentFontSize, yOffset);
    });
    yOffset += sectionSpacing;
    checkRemainingSpace(sectionSpacing);

    // Projects Section
    addSectionToPDF("Projects", resumeData.projects, labels.projects);

    // Save the PDF
    doc.save(`resume_page_${currentPage}.pdf`);
  };

  const degreeOptions = [
    "",
    "Elementary School",
    "High School",
    "Vocational",
    "College",
    "Post-Graduate",
    "Professional Certification",
  ];
  const fetchSkills = () => {
    c_skills()
      .then((value) => {
        setSkillsSet(value.rows);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const initialData = skillsSet.map(({ id, skill }) => ({
    id: id,
    value: skill,
    label: skill,
  }));
  const dataSearchSkills = (data) => {
    setSkills(data);
  };
  const handleSubmit = (e) => {
    e.preventDefault();

    // Add selected skills to formData before submitting
    const updatedFormData = {
      ...formData,
      skills: skills.map((skill) => skill.value),
      user_id: userData.user_id, // Adding user_id to the form data
    };

    c_buildResume(updatedFormData)
      .then((respone) => {
        if (respone.code === "1111") {
          alert("Resume Saved");
        } else {
          alert("Error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const fetchResumeBuild = () => {
    c_getBuildResume(userData.user_id)
      .then((value) => {
        console.log(value.rows[0].user_data);
        // generatePDF(value.rows[0].user_data);
        setGeneratedResume(value.rows[0].user_data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchSkills();
    fetchResumeBuild();
    const formattedSkills = userData.userskills.map((skill, index) => ({
      id: index + 1,
      value: skill,
      label: skill,
    }));

    setSkillsUpdate(formattedSkills);
  }, []);
  return (
    <form
      onSubmit={handleSubmit}
      className='max-w-4xl mx-auto p-6 space-y-6 bg-white shadow-lg rounded-lg'>
      <h2 className='text-3xl font-semibold text-gray-700 mb-6'>
        Resume Builder
      </h2>
      {/*  Personal Information */}
      <div className='space-y-4'>
        <h3 className='text-lg font-semibold text-gray-700'>
          Personal Information
        </h3>
        <div className='grid grid-cols-1 sm:grid-cols-2 gap-4'>
          {Object.keys(formData.personalInfo).map((field) => (
            <div key={field} className='flex flex-col'>
              <label
                htmlFor={field}
                className='text-xs font-medium text-gray-600'>
                {labels.personalInfo[field]}
              </label>
              <input
                id={field}
                type='text'
                value={formData.personalInfo[field]}
                readOnly={
                  field === "firstName" ||
                  field === "lastName" ||
                  field === "email" ||
                  field === "city" ||
                  field === "state"
                }
                onChange={(e) =>
                  handleChange("personalInfo", undefined, field, e.target.value)
                }
                className='mt-1 p-2 text-xs border border-gray-300 rounded-sm outline-none'
              />
            </div>
          ))}
        </div>
      </div>
      {/* Education */}
      <div className='space-y-4 mt-10'>
        <h3 className='text-lg font-semibold text-gray-700'>Education</h3>
        {formData.education.map((edu, index) => (
          <div
            key={index}
            className='space-y-4 border-b border-gray-200 pb-4 mb-4'>
            <div className='grid grid-cols-1 sm:grid-cols-2 gap-4'>
              {Object.keys(edu).map((field) => (
                <div key={field} className='flex flex-col'>
                  <label
                    htmlFor={`edu-${field}-${index}`}
                    className='text-xs font-medium text-gray-600'>
                    {labels.education[field]}
                  </label>
                  {field === "degree" ? (
                    <select
                      id={`edu-${field}-${index}`}
                      value={edu[field]}
                      onChange={(e) =>
                        handleChange("education", index, field, e.target.value)
                      }
                      className='mt-1 text-xs p-2 border border-gray-300 rounded-sm outline-none'>
                      {degreeOptions.map((option) => (
                        <option key={option} value={option}>
                          {option}
                        </option>
                      ))}
                    </select>
                  ) : (
                    <input
                      id={`edu-${field}-${index}`}
                      type={
                        field === "startDate" || field === "endDate"
                          ? "date"
                          : "text"
                      }
                      value={edu[field]}
                      onChange={(e) =>
                        handleChange("education", index, field, e.target.value)
                      }
                      className='mt-1 text-xs p-2 border border-gray-300 rounded-sm  outline-none'
                    />
                  )}
                </div>
              ))}
            </div>
            {formData.education.length > 1 && (
              <button
                type='button'
                onClick={() => removeSectionItem("education", index)}
                className='flex items-center text-red-500 hover:underline'>
                <Image
                  src={`./icons/remove.svg`}
                  alt={`Icons`}
                  className='w-8 h-8 object-cover rounded-full'
                  width={40}
                  height={40}
                  priority
                />
                <span className='ml-2'>Remove</span>
              </button>
            )}
          </div>
        ))}
        <button
          type='button'
          onClick={() => addSectionItem("education")}
          className='flex items-center text-blue-500 hover:underline'>
          <Image
            src={`./icons/add.svg`}
            alt={`Icons`}
            className='w-8 h-8 object-cover rounded-full'
            width={40}
            height={40}
            priority
          />
          <span className='ml-2'>Add</span>
        </button>
      </div>
      {/* Work Experience */}
      <div className='space-y-4 mt-48'>
        <h3 className='text-lg font-semibold text-gray-700'>Work Experience</h3>
        {formData.experience.map((exp, index) => (
          <div
            key={index}
            className='space-y-4 border-b border-gray-200 pb-4 mb-4'>
            <div className='grid grid-cols-1 sm:grid-cols-2 gap-4'>
              {Object.keys(exp).map((field) => (
                <div key={field} className='flex flex-col'>
                  <label
                    htmlFor={`exp-${field}-${index}`}
                    className='text-xs font-medium text-gray-600'>
                    {labels.experience[field]}
                  </label>
                  <input
                    id={`exp-${field}-${index}`}
                    type={
                      field === "startDate" || field === "endDate"
                        ? "date"
                        : "text"
                    }
                    value={exp[field]}
                    onChange={(e) =>
                      handleChange("experience", index, field, e.target.value)
                    }
                    className='mt-1 p-2 text-xs border border-gray-300 rounded-sm outline-none'
                  />
                </div>
              ))}
            </div>
            {formData.experience.length > 1 && (
              <button
                type='button'
                onClick={() => removeSectionItem("experience", index)}
                className='flex items-center text-red-500 hover:underline'>
                <Image
                  src={`./icons/remove.svg`}
                  alt={`Icons`}
                  className='w-8 h-8 object-cover rounded-full'
                  width={40}
                  height={40}
                  priority
                />
                <span className='ml-2'>Remove</span>
              </button>
            )}
          </div>
        ))}
        <button
          type='button'
          onClick={() => addSectionItem("experience")}
          className='flex items-center text-blue-500 hover:underline'>
          <Image
            src={`./icons/add.svg`}
            alt={`Icons`}
            className='w-8 h-8 object-cover rounded-full'
            width={40}
            height={40}
            priority
          />
          <span className='ml-2'>Add</span>
        </button>
      </div>
      {/* Skills */}
      <div className='space-y-4 mt-10'>
        <h3 className='text-lg font-semibold text-gray-700'>Skills</h3>
        <Select
          data={initialData}
          type={"multiple"}
          searchresult={dataSearchSkills}
          textfieldlabel={""}
          returnData={skillsUpdate}
        />
      </div>
      {/* Projects */}
      <div className='space-y-4 mt-10'>
        <h3 className='text-lg font-semibold text-gray-700'>Projects</h3>
        {formData.projects.map((project, index) => (
          <div
            key={index}
            className='space-y-4 border-b border-gray-200 pb-4 mb-4'>
            <div className='grid grid-cols-1 sm:grid-cols-2 gap-4'>
              {Object.keys(project).map((field) => (
                <div key={field} className='flex flex-col'>
                  <label
                    htmlFor={`project-${field}-${index}`}
                    className='text-xs font-medium text-gray-600'>
                    {labels.projects[field]}
                  </label>
                  <input
                    id={`project-${field}-${index}`}
                    type='text'
                    value={project[field]}
                    onChange={(e) =>
                      handleChange("projects", index, field, e.target.value)
                    }
                    className='mt-1 text-xs p-2 border border-gray-300 rounded-sm outline-none'
                  />
                </div>
              ))}
            </div>
            {formData.projects.length > 1 && (
              <button
                type='button'
                onClick={() => removeSectionItem("projects", index)}
                className='flex items-center text-red-500 hover:underline'>
                <Image
                  src={`./icons/remove.svg`}
                  alt={`Icons`}
                  className='w-8 h-8 object-cover rounded-full'
                  width={40}
                  height={40}
                  priority
                />
                <span className='ml-2'>Remove</span>
              </button>
            )}
          </div>
        ))}
        <button
          type='button'
          onClick={() => addSectionItem("projects")}
          className='flex items-center text-blue-500 hover:underline'>
          <Image
            src={`./icons/add.svg`}
            alt={`Icons`}
            className='w-8 h-8 object-cover rounded-full'
            width={40}
            height={40}
            priority
          />
          <span className='ml-2'>Add</span>
        </button>
      </div>

      <div className='flex justify-end space-x-4'>
        <button
          type='button'
          onClick={generatePDF}
          className='bg-green-500 text-white py-2 px-4 rounded-lg shadow hover:bg-green-600 transition duration-200'>
          Download PDF
        </button>
        <button
          type='submit'
          className='bg-blue-500 text-white py-2 px-4 rounded-lg shadow hover:bg-blue-600 transition duration-200'>
          Save Resume
        </button>
      </div>
    </form>
  );
};

export default ResumeBuilder;
