import React, { useEffect, useState } from "react";
import ResumeBuilderForm from "./resumebuilderform";
import ResumeBuilderEditForm from "./resumebuildereditform";
import Loading from "./../components/loading";
import { c_getBuildResume } from "@/app/controller/(profile)/c_jobs";

const ResumeBuilder = ({ userData }) => {
  const [resumeSubmitted, setResumeSubmitted] = useState(false);
  const [loading, setLoading] = useState(true);

  const handleCheckResume = () => {
    c_getBuildResume(userData.user_id)
      .then((value) => {
        if (
          value.rows[0].user_data.certifications == null &&
          value.rows[0].user_data.education == null &&
          value.rows[0].user_data.experience == null &&
          value.rows[0].user_data.projects == null
        ) {
          setResumeSubmitted(false);
        } else {
          setResumeSubmitted(true);
        }
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    handleCheckResume();
  }, []);
  if (loading) {
    return <Loading />;
  }

  return (
    <div className='container p-6 mx-auto bg-white shadow-lg rounded-lg'>
      {resumeSubmitted ? (
        <ResumeBuilderEditForm userData={userData} />
      ) : (
        <ResumeBuilderForm userData={userData} />
      )}
    </div>
  );
};

export default ResumeBuilder;
