import React, { useEffect, useState } from "react";
import { c_skills } from "@/app/controller/(main)/c_main";
import Select from "../components/tailwind-select-resume";
import { c_buildResume } from "@/app/controller/(profile)/c_jobs";

const ResumeBuilder = ({ userData }) => {
  const [skillsSet, setSkillsSet] = useState([]);
  const [firstName, setFirstName] = useState(userData?.username);
  const [lastName, setLastName] = useState(userData?.userlname);
  const [emailAddress, setEmailAddress] = useState(userData?.useremail);
  const [phoneNumber, setPhoneNumber] = useState("");
  const [completeAddress, setCompleteAddress] = useState("");
  const [cityMunicipality, setCityMunicipality] = useState(
    JSON.parse(userData?.usermuni).label
  );
  const [stateProvince, setStateProvince] = useState(
    JSON.parse(userData?.userprov).label
  );
  const [postalCode, setPostalCode] = useState("");
  const [expectedSalary, setExpectedSalary] = useState("");
  const [workSetUp, setWorkSetUp] = useState("");
  const [languages, setLanguages] = useState("");
  const initialData = skillsSet.map(({ id, skill }) => ({
    id: id,
    value: skill,
    label: skill,
  }));

  const fetchSkills = () => {
    c_skills()
      .then((value) => {
        setSkillsSet(value.rows);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const [workExperiences, setWorkExperiences] = useState([
    {
      companyName: "",
      jobTitle: "",
      startDate: "",
      endDate: "",
      responsibilities: "",
      skills: [],
    },
  ]);

  const [educations, setEducations] = useState([
    {
      schoolName: "",
      degree: "",
      startDate: "",
      endDate: "",
      fieldOfStudy: "",
    },
  ]);

  const [projects, setProjects] = useState([
    {
      projectTitle: "",
      projectLink: "",
      projectDescription: "",
    },
  ]);

  const [certifications, setCertifications] = useState([
    {
      certificationName: "",
      issuingOrganization: "",
      issueDate: "",
      expirationDate: "",
      credentialID: "",
      credentialURL: "",
    },
  ]);

  const addWorkExperience = () => {
    setWorkExperiences([
      ...workExperiences,
      {
        companyName: "",
        jobTitle: "",
        startDate: "",
        endDate: "",
        responsibilities: "",
        skills: [],
      },
    ]);
  };

  const removeWorkExperience = (index) => {
    setWorkExperiences(workExperiences.filter((_, i) => i !== index));
  };

  const handleInputChange = (index, field, value, type = "work") => {
    if (type === "work") {
      const newWorkExperiences = [...workExperiences];
      newWorkExperiences[index][field] = value;
      setWorkExperiences(newWorkExperiences);
    } else if (type === "education") {
      const newEducations = [...educations];
      newEducations[index][field] = value;
      setEducations(newEducations);
    } else if (type === "project") {
      const newProjects = [...projects];
      newProjects[index][field] = value;
      setProjects(newProjects);
    } else if (type === "certification") {
      const newCertification = [...certifications];
      newCertification[index][field] = value;
      setCertifications(newCertification);
    }
  };

  const dataValue = (data, index) => {
    let userSkills = "skills";
    if (data.length > 0) {
      const valuesArray = data.map((skill) => skill.value);
      handleInputChange(index, userSkills, valuesArray);
    }
  };

  const addEducation = () => {
    setEducations([
      ...educations,
      {
        schoolName: "",
        degree: "",
        startDate: "",
        endDate: "",
        fieldOfStudy: "",
      },
    ]);
  };

  const removeEducation = (index) => {
    setEducations(educations.filter((_, i) => i !== index));
  };

  const addProject = () => {
    setProjects([
      ...projects,
      {
        projectTitle: "",
        projectLink: "",
        projectDescription: "",
      },
    ]);
  };

  const removeProject = (index) => {
    setProjects(projects.filter((_, i) => i !== index));
  };

  const addCertification = () => {
    setCertifications([
      ...certifications,
      {
        certificationName: "",
        issuingOrganization: "",
        issueDate: "",
        expirationDate: "",
        credentialID: "",
        credentialURL: "",
      },
    ]);
  };

  const removeCertification = (index) => {
    setCertifications(certifications.filter((_, i) => i !== index));
  };
  const dataForm = {
    personalInfo: {
      userId: userData.user_id,
      firstName: firstName,
      lastName: lastName,
      email: emailAddress,
      phone: phoneNumber,
      address: completeAddress,
      city: cityMunicipality,
      state: stateProvince,
      zip: postalCode,
      expectedSalary: expectedSalary,
      workSetUp: workSetUp,
      languages: languages,
    },
    educations,
    workExperiences,
    projects,
    certifications,
  };

  const handleSaveInfo = () => {
    c_buildResume(dataForm)
      .then((response) => {
        if (response.code === "1111") {
          alert("Saved");
        } else {
          console.log("Error on saving");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const mapSkills = (skills) =>
    skills.map((skill, index) => ({
      id: index + 1,
      value: skill,
      label: skill,
    }));

  useEffect(() => {
    fetchSkills();
  }, []);

  return (
    <div className='w-full'>
      <h2 className='text-2xl font-semibold text-slate-700 mb-6'>
        Resume Builder
      </h2>

      {/* Personal Information */}
      <div className='w-full'>
        <div className='w-full'>
          <h4 className='text-slate-700 ml-2 font-semibold'>
            Personal Information
          </h4>
        </div>
        <div className='w-full flex flex-wrap'>
          <div className='sm:w-6/12 w-full px-2'>
            <label className='text-xs text-slate-700' htmlFor='firstName'>
              First Name
            </label>
            <input
              id='firstName'
              type='text'
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              readOnly
              className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
          </div>
          <div className='sm:w-6/12 w-full px-2'>
            <label className='text-xs text-slate-700' htmlFor='lastName'>
              Last Name
            </label>
            <input
              id='lastName'
              value={lastName}
              readOnly
              onChange={(e) => setLastName(e.target.value)}
              type='text'
              className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
          </div>
        </div>
        <div className='w-full flex flex-wrap mt-2'>
          <div className='sm:w-6/12 w-full px-2'>
            <label className='text-xs text-slate-700' htmlFor='emailAddress'>
              Email Address
            </label>
            <input
              id='emailAddress'
              value={emailAddress}
              onChange={(e) => setEmailAddress(e.target.value)}
              readOnly
              type='text'
              className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
          </div>
          <div className='sm:w-6/12 w-full px-2'>
            <label className='text-xs text-slate-700' htmlFor='phoneNumber'>
              Phone Number
            </label>
            <input
              id='phoneNumber'
              value={phoneNumber}
              onChange={(e) => setPhoneNumber(e.target.value)}
              type='text'
              className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
          </div>
        </div>
        <div className='w-full flex flex-wrap mt-2'>
          <div className='sm:w-6/12 w-full px-2'>
            <label className='text-xs text-slate-700' htmlFor='completeAddress'>
              Complete Address
            </label>
            <input
              id='completeAddress'
              value={completeAddress}
              onChange={(e) => setCompleteAddress(e.target.value)}
              type='text'
              className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
          </div>
          <div className='sm:w-6/12 w-full px-2'>
            <label
              className='text-xs text-slate-700'
              htmlFor='cityMunicipality'>
              City/Municipality
            </label>
            <input
              id='cityMunicipality'
              value={cityMunicipality}
              readOnly
              onChange={(e) => setCityMunicipality(e.target.value)}
              type='text'
              className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
          </div>
        </div>
        <div className='w-full flex flex-wrap mt-2'>
          <div className='sm:w-6/12 w-full px-2'>
            <label className='text-xs text-slate-700' htmlFor='stateProvince'>
              State/Province
            </label>
            <input
              id='stateProvince'
              value={stateProvince}
              readOnly
              onChange={(e) => setStateProvince(e.target.value)}
              type='text'
              className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
          </div>
          <div className='sm:w-6/12 w-full px-2'>
            <label className='text-xs text-slate-700' htmlFor='postalCode'>
              ZIP/Postal Code
            </label>
            <input
              id='postalCode'
              value={postalCode}
              onChange={(e) => setPostalCode(e.target.value)}
              type='text'
              className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
          </div>
        </div>
      </div>

      {/* Education */}
      {educations.map((education, index) => (
        <div key={index} className='w-full mt-5'>
          <div className='w-full'>
            <h4 className='text-slate-700 ml-2 font-semibold'>Education</h4>
          </div>
          <div className='w-full flex flex-wrap'>
            <div className='sm:w-6/12 w-full px-2'>
              <label
                className='text-xs text-slate-700'
                htmlFor={`schoolName-${index}`}>
                School Name
              </label>
              <input
                id={`schoolName-${index}`}
                type='text'
                value={education.schoolName}
                onChange={(e) =>
                  handleInputChange(
                    index,
                    "schoolName",
                    e.target.value,
                    "education"
                  )
                }
                className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
            </div>
            <div className='sm:w-6/12 w-full px-2'>
              <label
                className='text-xs text-slate-700'
                htmlFor={`degree-${index}`}>
                Degree
              </label>
              <select
                id={`degree-${index}`}
                value={education.degree}
                onChange={(e) =>
                  handleInputChange(
                    index,
                    "degree",
                    e.target.value,
                    "education"
                  )
                }
                className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'>
                <option> -</option>
                <option>Elementary School</option>
                <option>High School</option>
                <option>Vocational</option>
                <option>College</option>
                <option>Post-Graduate</option>
                <option>Professional Certification</option>
              </select>
            </div>
          </div>
          <div className='w-full flex flex-wrap mt-2'>
            <div className='sm:w-6/12 w-full px-2'>
              <label
                className='text-xs text-slate-700'
                htmlFor={`startDateEdu-${index}`}>
                Start Date
              </label>
              <input
                id={`startDateEdu-${index}`}
                type='date'
                value={education.startDate}
                onChange={(e) =>
                  handleInputChange(
                    index,
                    "startDate",
                    e.target.value,
                    "education"
                  )
                }
                className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
            </div>
            <div className='sm:w-6/12 w-full px-2'>
              <label
                className='text-xs text-slate-700'
                htmlFor={`endDateEdu-${index}`}>
                End Date
              </label>
              <input
                id={`endDateEdu-${index}`}
                type='date'
                value={education.endDate}
                onChange={(e) =>
                  handleInputChange(
                    index,
                    "endDate",
                    e.target.value,
                    "education"
                  )
                }
                className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
            </div>
          </div>
          <div className='w-full flex flex-wrap mt-2'>
            <div className='sm:w-6/12 w-full px-2'>
              <label
                className='text-xs text-slate-700'
                htmlFor={`fieldOfStudy-${index}`}>
                Field of Study
              </label>
              <input
                id={`fieldOfStudy-${index}`}
                type='text'
                value={education.fieldOfStudy}
                onChange={(e) =>
                  handleInputChange(
                    index,
                    "fieldOfStudy",
                    e.target.value,
                    "education"
                  )
                }
                className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
            </div>
          </div>
          {index > 0 && (
            <div className='w-full flex justify-end mt-2 px-2'>
              <button
                className='bg-red-500 text-white px-4 py-2 rounded'
                onClick={() => removeEducation(index)}>
                Remove
              </button>
            </div>
          )}
        </div>
      ))}
      <div className='w-full flex justify-end mt-2 px-2'>
        <button
          className='bg-blue-500 text-white px-4 py-2 rounded'
          onClick={addEducation}>
          Add Education
        </button>
      </div>

      {/* Work Experience */}
      {workExperiences.map((workExperience, index) => (
        <div key={index} className='w-full mt-5'>
          <div className='w-full'>
            <h4 className='text-slate-700 ml-2 font-semibold'>
              Work Experience
            </h4>
          </div>
          <div className='w-full flex flex-wrap'>
            <div className='sm:w-6/12 w-full px-2'>
              <label
                className='text-xs text-slate-700'
                htmlFor={`companyName-${index}`}>
                Company Name
              </label>
              <input
                id={`companyName-${index}`}
                type='text'
                value={workExperience.companyName}
                onChange={(e) =>
                  handleInputChange(index, "companyName", e.target.value)
                }
                className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
            </div>
            <div className='sm:w-6/12 w-full px-2'>
              <label
                className='text-xs text-slate-700'
                htmlFor={`jobTitle-${index}`}>
                Job Title
              </label>
              <input
                id={`jobTitle-${index}`}
                type='text'
                value={workExperience.jobTitle}
                onChange={(e) =>
                  handleInputChange(index, "jobTitle", e.target.value)
                }
                className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
            </div>
          </div>
          <div className='w-full flex flex-wrap mt-2'>
            <div className='sm:w-6/12 w-full px-2'>
              <label
                className='text-xs text-slate-700'
                htmlFor={`startDate-${index}`}>
                Start Date
              </label>
              <input
                id={`startDate-${index}`}
                type='date'
                value={workExperience.startDate}
                onChange={(e) =>
                  handleInputChange(index, "startDate", e.target.value)
                }
                className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
            </div>
            <div className='sm:w-6/12 w-full px-2'>
              <label
                className='text-xs text-slate-700'
                htmlFor={`endDate-${index}`}>
                End Date
              </label>
              <input
                id={`endDate-${index}`}
                type='date'
                value={workExperience.endDate}
                onChange={(e) =>
                  handleInputChange(index, "endDate", e.target.value)
                }
                className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
            </div>
          </div>
          <div className='w-full px-2 mt-2'>
            <label
              className='text-xs text-slate-700'
              htmlFor={`responsibilities-${index}`}>
              Responsibilities
            </label>
            <textarea
              id={`responsibilities-${index}`}
              value={workExperience.responsibilities}
              onChange={(e) =>
                handleInputChange(index, "responsibilities", e.target.value)
              }
              className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></textarea>
          </div>
          <div className='w-full px-2 mt-2'>
            <label
              className='text-xs text-slate-700'
              htmlFor={`skills-${index}`}>
              Skills
            </label>
            <Select
              data={initialData}
              type={"multiple"}
              searchresult={dataValue}
              selectIndex={index}
              textfieldlabel={""}
              returnData={mapSkills(workExperience.skills)}
              // returnData={skillsUpdate}
            />
            {/* {workExperience.skills} */}
          </div>
          {index > 0 && (
            <div className='w-full flex justify-end mt-2 px-2'>
              <button
                className='bg-red-500 text-white px-4 py-2 rounded'
                onClick={() => removeWorkExperience(index)}>
                Remove
              </button>
            </div>
          )}
        </div>
      ))}
      <div className='w-full flex justify-end mt-2 px-2'>
        <button
          className='bg-blue-500 text-white px-4 py-2 rounded text-xs'
          onClick={addWorkExperience}>
          Add Work Experience
        </button>
      </div>

      {/* Projects */}
      {projects.map((project, index) => (
        <div key={index} className='w-full mt-5'>
          <div className='w-full'>
            <h4 className='text-slate-700 ml-2 font-semibold'>Projects</h4>
          </div>
          <div className='w-full flex flex-wrap'>
            <div className='sm:w-6/12 w-full px-2'>
              <label
                className='text-xs text-slate-700'
                htmlFor={`projectTitle-${index}`}>
                Project Title
              </label>
              <input
                id={`projectTitle-${index}`}
                type='text'
                value={project.projectTitle}
                onChange={(e) =>
                  handleInputChange(
                    index,
                    "projectTitle",
                    e.target.value,
                    "project"
                  )
                }
                className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
            </div>
            <div className='sm:w-6/12 w-full px-2'>
              <label
                className='text-xs text-slate-700'
                htmlFor={`projectLink-${index}`}>
                Project Link
              </label>
              <input
                id={`projectLink-${index}`}
                type='text'
                value={project.projectLink}
                onChange={(e) =>
                  handleInputChange(
                    index,
                    "projectLink",
                    e.target.value,
                    "project"
                  )
                }
                className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
            </div>
          </div>
          <div className='w-full px-2 mt-2'>
            <label
              className='text-xs text-slate-700'
              htmlFor={`projectDescription-${index}`}>
              Project Description
            </label>
            <textarea
              id={`projectDescription-${index}`}
              value={project.projectDescription}
              onChange={(e) =>
                handleInputChange(
                  index,
                  "projectDescription",
                  e.target.value,
                  "project"
                )
              }
              className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></textarea>
          </div>
          {index > 0 && (
            <div className='w-full flex justify-end mt-2 px-2'>
              <button
                className='bg-red-500 text-white px-4 py-2 rounded'
                onClick={() => removeProject(index)}>
                Remove
              </button>
            </div>
          )}
        </div>
      ))}
      <div className='w-full flex justify-end mt-2 px-2'>
        <button
          className='bg-blue-500 text-white px-4 py-2 rounded text-xs'
          onClick={addProject}>
          Add Project
        </button>
      </div>

      {/* Certifications */}
      {certifications.map((certification, index) => (
        <div key={index} className='w-full mt-5'>
          <div className='w-full'>
            <h4 className='text-slate-700 ml-2 font-semibold'>
              Certifications
            </h4>
          </div>
          <div className='w-full flex flex-wrap'>
            <div className='sm:w-6/12 w-full px-2'>
              <label
                className='text-xs text-slate-700'
                htmlFor={`certificationName-${index}`}>
                Certification Name
              </label>
              <input
                id={`certificationName-${index}`}
                type='text'
                value={certification.certificationName}
                onChange={(e) =>
                  handleInputChange(
                    index,
                    "certificationName",
                    e.target.value,
                    "certification"
                  )
                }
                className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
            </div>
            <div className='sm:w-6/12 w-full px-2'>
              <label
                className='text-xs text-slate-700'
                htmlFor={`issuingOrganization-${index}`}>
                Issuing Organization
              </label>
              <input
                id={`issuingOrganization-${index}`}
                type='text'
                value={certification.issuingOrganization}
                onChange={(e) =>
                  handleInputChange(
                    index,
                    "issuingOrganization",
                    e.target.value,
                    "certification"
                  )
                }
                className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
            </div>
          </div>
          <div className='w-full flex flex-wrap mt-2'>
            <div className='sm:w-6/12 w-full px-2'>
              <label
                className='text-xs text-slate-700'
                htmlFor={`issueDate-${index}`}>
                Issue Date
              </label>
              <input
                id={`issueDate-${index}`}
                type='date'
                value={certification.issueDate}
                onChange={(e) =>
                  handleInputChange(
                    index,
                    "issueDate",
                    e.target.value,
                    "certification"
                  )
                }
                className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
            </div>
            <div className='sm:w-6/12 w-full px-2'>
              <label
                className='text-xs text-slate-700'
                htmlFor={`expirationDate-${index}`}>
                Expiration Date
              </label>
              <input
                id={`expirationDate-${index}`}
                type='date'
                value={certification.expirationDate}
                onChange={(e) =>
                  handleInputChange(
                    index,
                    "expirationDate",
                    e.target.value,
                    "certification"
                  )
                }
                className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
            </div>
          </div>
          <div className='w-full px-2 mt-2'>
            <label
              className='text-xs text-slate-700'
              htmlFor={`credentialID-${index}`}>
              Credential ID
            </label>
            <input
              id={`credentialID-${index}`}
              type='text'
              value={certification.credentialID}
              onChange={(e) =>
                handleInputChange(
                  index,
                  "credentialID",
                  e.target.value,
                  "certification"
                )
              }
              className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
          </div>
          <div className='w-full px-2 mt-2'>
            <label
              className='text-xs text-slate-700'
              htmlFor={`credentialURL-${index}`}>
              Credential URL
            </label>
            <input
              id={`credentialURL-${index}`}
              type='text'
              value={certification.credentialURL}
              onChange={(e) =>
                handleInputChange(
                  index,
                  "credentialURL",
                  e.target.value,
                  "certification"
                )
              }
              className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
          </div>
          {index > 0 && (
            <div className='w-full flex justify-end mt-2 px-2'>
              <button
                className='bg-red-500 text-white px-4 py-2 rounded'
                onClick={() => removeCertification(index)}>
                Remove
              </button>
            </div>
          )}
        </div>
      ))}
      <div className='w-full flex justify-end mt-2 px-2'>
        <button
          className='bg-blue-500 text-white px-4 py-2 rounded text-xs'
          onClick={addCertification}>
          Add Certification
        </button>
      </div>

      {/* Other Information */}
      <div className='w-full mt-4'>
        <div className='w-full'>
          <h4 className='text-slate-700 ml-2 font-semibold'>
            Other Information
          </h4>
        </div>
        <div className='w-full flex flex-wrap'>
          <div className='sm:w-6/12 w-full px-2'>
            <label className='text-xs text-slate-700' htmlFor='expectedSalary'>
              Expected Salary
            </label>
            <input
              id='expectedSalary'
              value={expectedSalary}
              onChange={(e) => setExpectedSalary(e.target.value)}
              type='number'
              className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
          </div>
          <div className='sm:w-6/12 w-full px-2'>
            <label className='text-xs text-slate-700' htmlFor='workSetUp'>
              Work Set-up
            </label>
            <select
              className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'
              id='workSetUp'
              value={workSetUp}
              onChange={(e) => setWorkSetUp(e.target.value)}>
              <option>Full-time</option>
              <option>Part-time</option>
              <option>Project Based</option>
            </select>
          </div>
        </div>
        <div className='w-full flex flex-wrap mt-2'>
          <div className='sm:w-6/12 w-full px-2'>
            <label className='text-xs text-slate-700' htmlFor='languages'>
              Languages
            </label>
            <input
              id='languages'
              value={languages}
              onChange={(e) => setLanguages(e.target.value)}
              type='text'
              className='w-full border rounded-sm outline-none p-1 text-xs text-slate-500'></input>
          </div>

          <div className='w-full flex flex-wrap mt-2'></div>
        </div>
      </div>

      {/* Button to save */}
      <div className='w-full flex justify-end mt-5 px-2 gap-4'>
        <button
          className='bg-green-500 text-white px-4 py-2 rounded text-xs'
          onClick={() => handleSaveInfo()}>
          Save
        </button>
      </div>
    </div>
  );
};

export default ResumeBuilder;
