import React, { useEffect, useState } from "react";
import { c_uploadResume } from "@/app/controller/(profile)/c_jobs";

export default function UploadResume({ userData, fetchUserResume, fileurl }) {
  const [selectedFile, setSelectedFile] = useState(null);
  const [uploadMessage, setUploadMessage] = useState("");
  const [uploadProgress, setUploadProgress] = useState(0);

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    if (file && file.type === "application/pdf") {
      const reader = new FileReader();
      reader.onload = () => {
        setSelectedFile(file);
      };
      reader.readAsDataURL(file);
    } else {
      setUploadMessage("Please upload a PDF file.");
    }
  };

  const handleUpload = () => {
    if (selectedFile) {
      c_uploadResume(
        selectedFile,
        userData.user_id,
        userData.username,
        userData.userlname
      )
        .then((response) => {
          if (response.code === "1111") {
            // Simulate upload progress
            const interval = setInterval(() => {
              setUploadProgress((prevProgress) => {
                if (prevProgress >= 100) {
                  clearInterval(interval);
                  setUploadMessage("Resume uploaded successfully!");
                  fetchUserResume();
                  return 100;
                }
                return prevProgress + 10;
              });
            }, 200);
          } else {
            // Error during uploading
            console.log("Error during uploading");
          }
        })
        .catch((error) => {
          // Error handling
          console.log(error);
        });
    } else {
      // No file selected
      setUploadMessage("Please select a file to upload.");
    }
  };

  return (
    <>
      <div className='w-full h-1/2 bg-slate-100 items-center justify-center flex mt-10'>
        <div className='flex items-center justify-center bg-gray-100 p-6'>
          <div
            className='bg-white rounded-lg shadow-lg'
            style={{ width: "600px" }}>
            <div className='m-8' style={{ width: "90%" }}>
              <div className='w-full flex items-center justify-between'>
                <h2 className='text-2xl font-semibold text-gray-700 mb-4'>
                  Upload Resume
                </h2>
                {fileurl ? (
                  <button
                    className='text-lg font-semibold text-slate-400 mb-4'
                    onClick={() => fetchUserResume()}>
                    Cancel
                  </button>
                ) : (
                  ""
                )}
              </div>

              <p className='text-gray-600 mb-4'>
                Please upload your resume in PDF format.
              </p>
              <div
                className='border-4 border-dashed border-gray-300 rounded-lg p-6 mb-4 flex items-center justify-center relative'
                style={{ width: "100%", boxSizing: "border-box" }}>
                <input
                  type='file'
                  accept='pdf/*'
                  onChange={handleFileChange}
                  className='w-full h-full opacity-0 cursor-pointer absolute'
                  style={{ top: 0, left: 0, bottom: 0, right: 0 }}
                />
                {!selectedFile ? (
                  <p className='text-gray-400 text-center'>
                    Drag & drop your file here or click to select a file
                  </p>
                ) : (
                  <p className='text-gray-600 text-center'>
                    {selectedFile.name}
                  </p>
                )}
              </div>
              <button
                onClick={() => handleUpload()}
                className='bg-blue-500 text-white py-2 rounded-lg shadow hover:bg-blue-600 transition duration-200'
                style={{ width: "100%" }}>
                Upload
              </button>
              {uploadMessage && (
                <p className='text-center text-sm text-green-500 mt-4'>
                  {uploadMessage}
                </p>
              )}
              {uploadProgress > 0 && uploadProgress < 100 && (
                <div className='relative pt-1' style={{ width: "100%" }}>
                  <div className='flex mb-2 items-center justify-between'>
                    <div>
                      <span className='text-xs font-semibold inline-block py-1 px-2 uppercase rounded-full text-blue-600 bg-blue-200'>
                        Uploading
                      </span>
                    </div>
                    <div className='text-right'>
                      <span className='text-xs font-semibold inline-block text-blue-600'>
                        {uploadProgress}%
                      </span>
                    </div>
                  </div>
                  <div className='overflow-hidden h-2 mb-4 text-xs flex rounded bg-blue-200'>
                    <div
                      style={{ width: `${uploadProgress}%` }}
                      className='shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-blue-500'></div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
