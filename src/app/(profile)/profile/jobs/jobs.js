import React, { useEffect, useState, useRef } from "react";
import UploadResume from "./uploadresume";
import ResumePreview from "./resumepreview";
import ResumeBuilder from "./resumebuilder";
import JobListings from "./joblist";
import JobApplication from "./jobapplication";
import { c_getResume } from "@/app/controller/(profile)/c_jobs";
import { useRouter } from "next/navigation";
import { useSearchParams } from "next/navigation";
// import jsPDF from "jspdf";

export default function Jobs({ userdata }) {
  const router = useRouter();
  const searchParams = useSearchParams();
  const option = searchParams.get("option");
  const xtsoption = searchParams.get("xts");
  const [showSubMenu, setShowSubMenu] = useState(false);
  const [loading, setLoading] = useState(true);
  const [fileUrl, setFileUrl] = useState("");
  const [fileUrlCheck, setFileUrlCheck] = useState("");

  const toggleSubMenu = () => {
    setShowSubMenu(!showSubMenu);
  };

  const handleOptionResume = (option) => {
    router.push(`/profile?page=jobs&&xts=resume&&option=${option}`);
    setShowSubMenu(false);
  };

  const handleXts = (option) => {
    router.push(`/profile?page=jobs&&xts=${option}`);
    setShowSubMenu(false);
  };

  const fetchResume = () => {
    fetch(`http://localhost:3000/api/profile/jobs/resume/${userdata.user_id}`)
      .then((response) => {
        if (!response.ok) {
          if (response.status === 404) {
            console.log("No file yet.");
          } else {
            console.log(`Error: ${response.status} - ${response.statusText}`);
          }
          throw new Error("Failed to fetch file");
        }
        return response.blob();
      })
      .then((blob) => {
        // Convert blob to object URL
        const url = URL.createObjectURL(blob);
        setFileUrl(url);
      })
      .catch((error) => {
        console.log("Error fetching file:", error.message);
      })
      .finally(() => {
        setLoading(false);
      });

    // Cleanup object URL when component unmounts
    return () => {
      if (fileUrl) {
        URL.revokeObjectURL(fileUrl);
      }
    };
  };
  useEffect(() => {
    fetchResume();
  }, [userdata.user_id]);

  if (loading) {
    return (
      <div className='flex items-center justify-center w-full mt-10'>
        <div className='relative flex flex-col items-center space-y-1'>
          <div className='w-36 h-36 border-4 border-t-4 border-r-4 border-b-4 border-l-4 border-t-red-500 border-r-blue-500 border-b-green-500 border-l-yellow-500 rounded-full animate-spin'></div>
          <div className='text-lg text-gray-500'>Loading...</div>
        </div>
      </div>
    );
  }

  return (
    <>
      <div className='w-full bg-white mt-10'>
        <ul className='justify-center items-center flex gap-2'>
          <li
            className='w-2/12 cursor-pointer border-b-4 border-white hover:border-b-4 hover:border-pink-500 text-center ease-in duration-300'
            onClick={() => handleXts("joblist")}>
            Jobs
          </li>
          <li
            className='w-2/12 cursor-pointer border-b-4 border-white hover:border-b-4 hover:border-pink-500 text-center ease-in duration-300'
            onClick={toggleSubMenu}>
            Resume
            {showSubMenu && (
              <div>
                {/* // <div onMouseOut={() => setShowSubMenu(false)}> */}
                <ul className='absolute bg-white text-black mt-2 rounded shadow-lg'>
                  <li
                    className='px-4 py-2 hover:bg-gray-200 cursor-pointer'
                    onClick={() => handleOptionResume("upload")}>
                    Upload Resume
                  </li>
                  <li
                    className='px-4 py-2 hover:bg-gray-200 cursor-pointer'
                    onClick={() => handleOptionResume("build")}>
                    Resume Builder
                  </li>
                </ul>
              </div>
            )}
          </li>

          <li
            className='w-2/12 cursor-pointer border-b-4 border-white hover:border-b-4  hover:border-pink-500 text-center ease-in duration-300'
            onClick={() => handleXts("application")}>
            Application
          </li>
        </ul>
      </div>
      {xtsoption === "resume" ? (
        option === "upload" ? (
          fileUrl ? (
            <div className='w-full pt-10'>
              <div className='w-full h-10 items-center justify-center flex'>
                <button
                  className='text-slate-400 text-2xl'
                  onClick={() => setFileUrl(null)}>
                  Change
                </button>
              </div>
              <ResumePreview fileUrl={fileUrl} />
            </div>
          ) : (
            <UploadResume
              userData={userdata}
              fetchUserResume={fetchResume}
              fileurl={fileUrlCheck}
            />
          )
        ) : option === "build" ? (
          <ResumeBuilder userData={userdata} />
        ) : (
          <div className='text-gray-600'>Please select an option.</div>
        )
      ) : xtsoption === "joblist" ? (
        <JobListings userData={userdata} />
      ) : xtsoption === "application" ? (
        <JobApplication userData={userdata} />
      ) : (
        <JobListings userData={userdata} />
      )}
    </>
  );
}
