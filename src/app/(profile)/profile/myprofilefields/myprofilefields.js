import React, { useEffect, useState, useRef } from "react";
import Select from "../components/tailwind-select";
import {
  c_saveuserprofile,
  c_updateuserprofile,
} from "@/app/controller/(profile)/c_userprofile";
import Wysiwyg from "../components/wysiwyg";
import { c_getprovinces } from "@/app/controller/(profile)/c_provinces";
import { c_getmunicipality } from "@/app/controller/(profile)/c_municipality";

export default function UsersProfileField({
  prop,
  fetchUsersProfile,
  userdata,
}) {
  const [username, setName] = useState("");
  const [userlname, setLname] = useState("");
  const [userprov, setProvince] = useState("");
  const [usermuni, setMuni] = useState("");
  const [userskills, setSkills] = useState([]);
  const [userheadline, setHeadline] = useState("");
  const [userabout, setAbout] = useState("");
  const [userprofilepicpath, setProfilepicpath] = useState("/");
  const [filteredMuni, setFilteredMun] = useState([]);
  const [provinces, setProvinces] = useState([]);
  const [municipality, setMunicipality] = useState([]);
  const [userskillsUpdate, setSkillsUpdate] = useState([]);
  const [usermuniUpdate, setMunipUpdate] = useState([]);
  const [userprovUpdate, setProvinceUpdate] = useState([]);
  const [useraboutUpdate, setAboutUpdate] = useState("");
  const [buttonUpdate, setButtonUpdate] = useState(false);
  const [userLink, setUserLink] = useState("");

  const submitInfo = () => {
    c_saveuserprofile(
      username,
      userlname,
      userprov,
      usermuni,
      prop.user.email,
      userskills,
      userheadline,
      userabout,
      userprofilepicpath
    )
      .then((response) => {
        if (response.code == "0000") {
          window.location.reload();
          alert(response.message);
          fetchUsersProfile();
        } else {
          alert(response.message);
        }
      })
      .catch((error) => {
        console.log("Error", error);
      });
  };

  const skills = [
    {
      id: 1,
      skills: "Programmer",
    },
    {
      id: 2,
      skills: "Data Entry",
    },
    {
      id: 3,
      skills: "Graphic Artist",
    },
    {
      id: 4,
      skills: "Web Developer",
    },
    {
      id: 5,
      skills: "UI/UX Designer",
    },

    // Add the rest of your data here
  ];

  const filterMunicipalityByRegionCode = (mun, provcode) => {
    return mun.filter((item) => item.provcode === provcode);
  };

  const initialData = provinces.map(({ id, provcode, provdesc, regcode }) => ({
    id: id,
    value: provcode,
    label: provdesc,
    otherCode: regcode,
  }));

  const skillsData = skills.map(({ id, skills }) => ({
    id: id,
    value: skills,
    label: skills,
  }));

  const municipalityData = filteredMuni.map(({ provcode, citymundesc }) => ({
    id: provcode,
    value: citymundesc,
    label: citymundesc,
  }));

  const dataSearchProv = (data) => {
    if (data && data.length > 0) {
      let filteredMunicipality = filterMunicipalityByRegionCode(
        municipality,
        data[0].value.toString()
      );
      setProvince(data[0]);
      setFilteredMun(filteredMunicipality);
      return;
    }
  };

  const dataSearchMun = (data) => {
    if (data && data.length > 0) {
      setMuni(data[0]);
      return;
    }
  };

  const dataSearchSkill = (data) => {
    if (data && data.length > 0) {
      const skillsArray = data.map((item) => item.label);
      setSkills(skillsArray);
    } else {
      setSkills([]); // Reset skills if data is empty
    }
  };

  const handleContent = (value) => {
    setAbout(value);
  };

  const fetchProvMun = () => {
    c_getprovinces()
      .then((value) => {
        setProvinces(value.rows);
      })
      .catch((error) => {
        console.log(error);
      });

    c_getmunicipality()
      .then((value) => {
        setMunicipality(value.rows);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const editProfile = () => {
    setName(userdata.username);
    setLname(userdata.userlname);
    setHeadline(userdata.userheadline);

    const formattedSkills = userdata.userskills.map((skill, index) => ({
      id: index + 1,
      value: skill,
      label: skill,
    }));

    setSkillsUpdate(formattedSkills);
    setMunipUpdate([JSON.parse(userdata.usermuni)]);
    setProvinceUpdate([JSON.parse(userdata.userprov)]);
    setAboutUpdate(userdata.userabout);
    setUserLink(userdata.user_link);
    setButtonUpdate(true);
  };

  const updateInfo = () => {
    c_updateuserprofile(
      username,
      userlname,
      userprov,
      usermuni,
      prop.user.email,
      userskills,
      userheadline,
      userabout,
      userLink
    )
      .then((response) => {
        if (response.code == "1111") {
          setSkillsUpdate([]);
          setMunipUpdate([]);
          setProvinceUpdate([]);
          setName("");
          setLname("");
          setAboutUpdate("");
          setHeadline("");
          setUserLink("");
          fetchUsersProfile();
          setButtonUpdate(false);
          // window.location.reload();
          alert(response.message);
        } else {
          alert(response.message);
        }
      })
      .catch((error) => {
        console.log("Error", error);
      });
  };

  useEffect(() => {
    fetchProvMun();
  }, [userdata]);

  return (
    <>
      <div className='w-full items-center justify-start flex pt-12 ml-14'>
        {userdata.user_id != null ? (
          buttonUpdate ? (
            <button
              className='text-sm bg-red-500 text-white px-2 py-1 rounded-full'
              onClick={() => setButtonUpdate(false)}>
              Cancel
            </button>
          ) : (
            <button
              className='text-sm bg-orange-500 text-white px-2 py-1 rounded-full'
              onClick={() => editProfile()}>
              Edit Information
            </button>
          )
        ) : (
          <>
            <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
              <div className='w-6/12 mr-2'>
                <label className='text-sm font-sans text-slate-500'>
                  http://localhost:3000/profile/
                </label>
                <input
                  type='text'
                  className='w-full rounded border p-1 outline-none text-slate-500'
                  value={userLink}
                  onChange={(e) => setUserLink(e.target.value)}></input>
              </div>
            </div>{" "}
            <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
              <div className='w-6/12 mr-2'>
                <label className='text-sm font-sans text-slate-500'>
                  First Name
                </label>
                <input
                  type='text'
                  className='w-full rounded border p-1 outline-none text-slate-500'
                  value={username}
                  onChange={(e) => setName(e.target.value)}></input>
              </div>
              <div className='w-6/12 '>
                <label className='text-sm font-sans text-slate-500'>
                  Last Name
                </label>
                <input
                  type='text'
                  className='w-full rounded border p-1 outline-none text-slate-500'
                  value={userlname}
                  onChange={(e) => setLname(e.target.value)}></input>
              </div>
            </div>
            <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
              {/* Customize Searchble Select */}
              <Select
                data={initialData}
                type={"single"}
                searchresult={dataSearchProv}
                textfieldlabel={"Provinces"}
                returnData={userprovUpdate}
              />
            </div>
            <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
              <div className='w-full mr-2'>
                <Select
                  data={municipalityData}
                  type={"single"}
                  searchresult={dataSearchMun}
                  textfieldlabel={"Municipality"}
                  returnData={usermuniUpdate}
                />
              </div>
            </div>
            <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
              <div className='w-full mr-2'>
                <Select
                  data={skillsData}
                  type={"multiple"}
                  searchresult={dataSearchSkill}
                  textfieldlabel={"Skills"}
                  returnData={userskillsUpdate}
                />
              </div>
            </div>
            <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
              <div className='w-full mr-2'>
                <label className='text-sm font-sans text-slate-500'>
                  Headline
                </label>
                <input
                  type='text'
                  className='w-full rounded border p-1 outline-none text-slate-500'
                  value={userheadline}
                  onChange={(e) => setHeadline(e.target.value)}></input>
              </div>
            </div>
            <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
              <Wysiwyg
                content={handleContent}
                label={"About"}
                returnData={useraboutUpdate}
              />
            </div>
            <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
              <div className='w-full mr-2'>
                {buttonUpdate ? (
                  <button
                    className='border w-1/5 bg-pink-500 rounded-full px-3 py-2 text-white  shadow-lg shadow-pink-500/30'
                    onClick={() => updateInfo()}>
                    Update
                  </button>
                ) : (
                  <button
                    className='border w-1/5 bg-pink-500 rounded-full px-3 py-2 text-white  shadow-lg shadow-pink-500/30'
                    onClick={() => submitInfo()}>
                    Submit
                  </button>
                )}
              </div>
            </div>
          </>
        )}
      </div>
      {buttonUpdate ? (
        <>
          <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
            <div className='w-6/12 mr-2'>
              <label className='text-sm font-sans text-slate-500'>
                http://localhost:3000/profile/
              </label>
              <input
                type='text'
                className='w-full rounded border p-1 outline-none text-slate-500'
                value={userLink}
                onChange={(e) => setUserLink(e.target.value)}></input>
            </div>
          </div>{" "}
          <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
            <div className='w-6/12 mr-2'>
              <label className='text-sm font-sans text-slate-500'>
                First Name
              </label>
              <input
                type='text'
                className='w-full rounded border p-1 outline-none text-slate-500'
                value={username}
                onChange={(e) => setName(e.target.value)}></input>
            </div>
            <div className='w-6/12 '>
              <label className='text-sm font-sans text-slate-500'>
                Last Name
              </label>
              <input
                type='text'
                className='w-full rounded border p-1 outline-none text-slate-500'
                value={userlname}
                onChange={(e) => setLname(e.target.value)}></input>
            </div>
          </div>
          <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
            {/* Customize Searchble Select */}
            <Select
              data={initialData}
              type={"single"}
              searchresult={dataSearchProv}
              textfieldlabel={"Provinces"}
              returnData={userprovUpdate}
            />
          </div>
          <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
            <div className='w-full mr-2'>
              <Select
                data={municipalityData}
                type={"single"}
                searchresult={dataSearchMun}
                textfieldlabel={"Municipality"}
                returnData={usermuniUpdate}
              />
            </div>
          </div>
          <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
            <div className='w-full mr-2'>
              <Select
                data={skillsData}
                type={"multiple"}
                searchresult={dataSearchSkill}
                textfieldlabel={"Skills"}
                returnData={userskillsUpdate}
              />
            </div>
          </div>
          <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
            <div className='w-full mr-2'>
              <label className='text-sm font-sans text-slate-500'>
                Headline
              </label>
              <input
                type='text'
                className='w-full rounded border p-1 outline-none text-slate-500'
                value={userheadline}
                onChange={(e) => setHeadline(e.target.value)}></input>
            </div>
          </div>
          <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
            <Wysiwyg
              content={handleContent}
              label={"About"}
              returnData={useraboutUpdate}
            />
          </div>
          <div className='w-full items-center justify-center flex pt-4 justify-between flex ml-14'>
            <div className='w-full mr-2'>
              {buttonUpdate ? (
                <button
                  className='border w-1/5 bg-pink-500 rounded-full px-3 py-2 text-white  shadow-lg shadow-pink-500/30'
                  onClick={() => updateInfo()}>
                  Update
                </button>
              ) : (
                <button
                  className='border w-1/5 bg-pink-500 rounded-full px-3 py-2 text-white  shadow-lg shadow-pink-500/30'
                  onClick={() => submitInfo()}>
                  Submit
                </button>
              )}
            </div>
          </div>
        </>
      ) : (
        ""
      )}
    </>
  );
}
