// Import the required library
const { Pool } = require("pg");

// Create a pool
const pool = new Pool({
  user: "postgres",
  host: "localhost",
  database: "dbx_cayap",
  password: "admin",
  port: 5432,
});

// Export the pool for use in other modules
module.exports = { pool };
