"use client";
import Header from "@header/header";
import Footer from "@footer/footer";
import { SessionProvider } from "next-auth/react";
import Content from "./contents";

export default function ProductProfile({ params }) {
  const product_id = params.product_id;

  return (
    <main className='flex flex-col items-center bg-white'>
      <SessionProvider>
        <Header />
        <Content productId={product_id} />
        <Footer />
      </SessionProvider>
    </main>
  );
}
