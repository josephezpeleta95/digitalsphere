import React, { useState, useEffect } from "react";
import { c_getJobListings } from "@/app/controller/(profile)/c_jobs";
import Link from "next/link";
import { JobsPulseLoading } from "../components/pulseloading";

export default function OnlineJobs() {
  const [jobListings, setJoblistings] = useState([]);
  const [loadingPulse, setLoadingPulse] = useState(true);

  const handleFetchJoblistings = () => {
    c_getJobListings()
      .then((value) => {
        setJoblistings(value.rows);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoadingPulse(false);
      });
  };

  useEffect(() => {
    handleFetchJoblistings();
  }, []);

  return (
    <div className='bg-slate-50 w-full items-center flex justify-center relative'>
      <div className='w-9/12 mt-4'>
        <div className='w-full p-5 text-2xl'>
          <h1 className='font-roboto leading-normal text-sky-700 font-semibold'>
            Online Jobs
          </h1>
        </div>
        <div className='lg:w-full flex bg-slate-50 '>
          <div className='z-40 w-full flex flex-wrap items-center p-4 mb-10'>
            {loadingPulse ? (
              <JobsPulseLoading />
            ) : (
              jobListings.slice(0, 9).map((job, index) => (
                <Link
                  href={`/jobs/${job.job_id}`}
                  target='_blank'
                  className='w-full md:w-6/12 lg:w-4/12 px-5 py-3'
                  key={index}>
                  <div className='w-full bg-white h-auto border border-slate-200 rounded-lg shadow-md p-5 transform transition-transform hover:scale-105 cursor-pointer'>
                    <div className='mb-2'>
                      <h4 className='text-lg font-semibold text-slate-700 truncate'>
                        {job.title}
                      </h4>
                      <p className='text-sm text-slate-500'>
                        {job.company_name}
                      </p>
                    </div>
                    <div className='text-slate-600'>
                      <p className='text-sm mb-1'>
                        <strong>Location:</strong> {job.job_location}
                      </p>
                      <p className='text-sm mb-1'>
                        <strong>Salary:</strong> {job.salary_range}
                      </p>
                      <p className='text-sm mb-1'>
                        <strong>Posted:</strong>{" "}
                        {new Date(job.date_posted).toLocaleDateString()}
                      </p>
                      <p className='text-sm'>
                        <strong>Setup:</strong> {job.setup}
                      </p>
                      <p className='text-sm'>
                        <strong>Work Arrangement:</strong>{" "}
                        {job.work_arrangement}
                      </p>
                    </div>
                  </div>
                </Link>
              ))
            )}
          </div>
        </div>
        <div className='justify-center items-center flex mb-10 w-full'>
          <Link href='../all-online-jobs'>
            <button className='z-50 relative border border-slate-500 rounded-full py-2 px-4 text-sky-700 hover:bg-orange-500 hover:text-white hover:border-orange-500'>
              View more
            </button>
          </Link>
        </div>

        <svg
          width='100%'
          height='100px'
          viewBox='0 0 1280 140'
          preserveAspectRatio='none'
          xmlns='http://www.w3.org/2000/svg'
          className='absolute bottom-0 -z-05 left-0 w-full'
          style={{ transform: "scaleY(-1)" }}>
          <g fill='#e0f2fe'>
            <path
              d='M1280 0L640 70 0 0v140l640-70 640 70V0z'
              fillOpacity='.5'
            />
            <path d='M1280 0H0l640 70 640-70z' />
          </g>
        </svg>
      </div>
    </div>
  );
}
