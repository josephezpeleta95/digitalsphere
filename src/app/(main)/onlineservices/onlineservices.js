import React, { useEffect, useState } from "react";
import { c_services } from "@/app/controller/(main)/c_main";
import Link from "next/link";
import { ServicesPulseLoading } from "../components/pulseloading";

export default function DigitalProducts() {
  const [services, setServices] = useState([]);
  const [loadingPulse, setLoadingPulse] = useState(true);

  function addCommas(numString) {
    return Number(numString).toLocaleString();
  }

  const ServicesCard = (
    {
      username,
      userlname,
      covers,
      userprofilepic,
      service_rate,
      service_name,
      service_work_setup,
      service_per,
      service_id,
    },
    index
  ) => (
    <>
      {service_name ? (
        <>
          {" "}
          <Link
            href={`/service/${service_id}`}
            key={index}
            target='_blank'
            className='w-full sm:w-1/2 md:w-1/3 lg:w-1/4 px-4 py-2'>
            <div className='bg-white rounded-lg shadow-lg overflow-hidden'>
              <div
                className='w-full bg-white h-48 bg-cover bg-center items-end justify-center flex transform transition-transform hover:scale-105 cursor-pointer'
                style={{
                  backgroundImage:
                    covers && covers.length > 0 && covers[0].service_cover
                      ? `url(/${covers[0].service_cover.replace(
                          /^\.\/public\//,
                          ""
                        )})`
                      : "none",
                }}>
                <div className='bg-slate-600 bg-opacity-90 w-11/12 p-1 border rounded-full flex mb-1'>
                  <div
                    className='w-8 h-8 rounded-full bg-sky-300 bg-cover bg-center border'
                    style={{ backgroundImage: `url(${userprofilepic})` }}></div>
                  <div className='items-center flex'>
                    <h5 className='text-xs ml-1 text-white truncate overflow-hidden'>
                      {username} {userlname}
                    </h5>
                  </div>
                </div>
              </div>
              <div className='h-20 p-2'>
                <div className='bg-white h-10 mt-1 justify-center flex py-1 text-slate-600 font-semibold'>
                  <div className='w-full flex-wrap text-xs'>
                    <h5 className='line-clamp-2'>{service_name}</h5>
                  </div>
                </div>
                <div className='flex justify-between text-sky-600'>
                  <div className='w-8/12 flex-wrap text-xs font-semibold'>
                    <h5>
                      ₱ {addCommas(service_rate)}
                      {"/"}
                      {service_per}
                    </h5>
                  </div>
                  <div className='w-4/12 text-xs items-start justify-end flex'>
                    <h5> ({service_work_setup})</h5>
                  </div>
                </div>
              </div>
            </div>
          </Link>
        </>
      ) : (
        ""
      )}
    </>
  );

  const fetchServices = () => {
    c_services()
      .then((value) => {
        setServices(value.rows);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoadingPulse(false);
      });
  };

  useEffect(() => {
    fetchServices();
  }, []);

  return (
    <div className='bg-white w-9/12 mt-4'>
      <div className='w-full p-5 text-2xl'>
        <h1 className='font-roboto leading-normal text-sky-700 font-semibold'>
          Online Services
        </h1>
      </div>
      <div className='flex justify-center'>
        <div className='w-full flex flex-wrap items-center p-4'>
          {loadingPulse ? (
            <ServicesPulseLoading />
          ) : (
            services.map((serviceprofile, index) => (
              <ServicesCard {...serviceprofile} key={index} />
            ))
          )}
        </div>
      </div>
      <div className='flex justify-center items-center mb-10 w-full'>
        <Link href={`/all-services`} target='_blank'>
          <button className='border border-slate-500 rounded-full py-2 px-4 text-sky-700 hover:bg-orange-500 hover:text-white hover:border-orange-500'>
            Show more
          </button>
        </Link>
      </div>
    </div>
  );
}
