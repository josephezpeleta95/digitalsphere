import React, { useEffect, useState } from "react";
import Link from "next/link";
import { c_talents } from "@/app/controller/(main)/c_main";
import { TalentsPulseLoading } from "../components/pulseloading";

export default function Talents() {
  const [talents, setTalents] = useState([]);
  const [loadingPulse, setLoadingPulse] = useState(true);

  const fetchTalents = () => {
    c_talents()
      .then((value) => {
        setTalents(value.rows);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoadingPulse(false);
      });
  };

  useEffect(() => {
    fetchTalents();
  }, []);

  const ProfileCard = ({
    username,
    userlname,
    userskills,
    userprofilepic,
    user_link,
  }) => (
    <Link
      href={`/talent/${user_link}`}
      target='_blank'
      className='cursor-pointer w-full sm:w-1/2 md:w-1/3 lg:w-1/4 p-2'>
      <div className='bg-white rounded-lg shadow-lg overflow-hidden'>
        <div
          className='bg-slate-200 h-48 bg-cover bg-center'
          style={{ backgroundImage: `url(${userprofilepic})` }}></div>
        <div className='p-4'>
          <div className='flex justify-between items-center mb-2'>
            <h3 className='text-lg font-bold'>
              {username} {userlname}
            </h3>
            <img src='/icons/check.svg' alt='check' className='w-6 h-6' />
          </div>
          <div className='flex flex-wrap'>
            {userskills.slice(0, 1).map((role, index) => (
              <span
                key={index}
                className='border p-1 rounded-full border-pink-500 text-xs mr-1 mb-1'>
                {role}
              </span>
            ))}
            {userskills.length > 1 && (
              <span className='border p-1 rounded-full border-pink-500 text-xs mr-1 mb-1'>
                {userskills.length - 1}+
              </span>
            )}
          </div>
        </div>
      </div>
    </Link>
  );

  return (
    <>
      <div className='bg-slate-50 w-full justify-center flex relative'>
        <div className='w-9/12 z-40'>
          <div className='w-full mb-5 mt-10 text-center text-6xl'>
            <span className='font-roboto leading-normal leading-snug lg:leading-normal font-semibold bg-clip-text text-transparent bg-gradient-to-r from-pink-500 to-violet-500'>
              Our Talents
            </span>
          </div>
          <div className='flex justify-center items-center'>
            <div className='w-11/12 flex flex-wrap justify-center items-center'>
              {loadingPulse ? (
                <TalentsPulseLoading />
              ) : (
                talents.map((talents_profile, index) => (
                  <ProfileCard {...talents_profile} key={index} />
                ))
              )}
            </div>
          </div>
          <div className='flex justify-center items-center p-4 w-full mb-5'>
            <Link href={`/all-talents`} target='_blank'>
              <button className='border border-slate-500 rounded-full py-2 px-4 text-sky-700 hover:bg-orange-500 hover:text-white hover:border-orange-500'>
                Show more
              </button>
            </Link>
          </div>
        </div>
        <svg
          xmlns='http://www.w3.org/2000/svg'
          viewBox='0 0 1440 320'
          className='absolute bottom-0 -z-05 left-0 w-full'>
          <path
            fill='#ffffff'
            fillOpacity='1'
            d='M0,224L48,213.3C96,203,192,181,288,186.7C384,192,480,224,576,218.7C672,213,768,171,864,144C960,117,1056,107,1152,112C1248,117,1344,139,1392,149.3L1440,160L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z'></path>
        </svg>
      </div>
    </>
  );
}
