"use client";

import Header from "@header/header";
import Footer from "@footer/footer";
import { SessionProvider } from "next-auth/react";
import React, { useEffect, useState } from "react";
import {
  c_pftalent,
  c_pfservices,
  c_pfproduct,
} from "@/app/controller/(main)/c_main";
import Products from "./products";
import Services from "./services";
import Image from "next/image";
import Protected from "../../../controller/protected";
import {
  UserProductsPulseLoading,
  UserProfilePulseLoading,
} from "../../components/pulseloading";

export default function ProductProfile({ params }) {
  const { user_link } = params;
  const [userInfo, setUserInfo] = useState([]);
  const [userProduct, setUserProduct] = useState([]);
  const [userService, setUserService] = useState([]);
  const [loadingPulse, setLoadingPulse] = useState(true);

  const fetchProfile = () => {
    c_pftalent(user_link)
      .then((value) => {
        setUserInfo(value.rows[0]);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoadingPulse(false);
      });
  };

  const fetchProducts = () => {
    c_pfproduct(user_link)
      .then((value) => {
        setUserProduct(value.rows);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoadingPulse(false);
      });
  };

  const fetchServices = () => {
    c_pfservices(user_link)
      .then((value) => {
        setUserService(value.rows);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoadingPulse(false);
      });
  };

  useEffect(() => {
    if (user_link) {
      try {
        // const decryptedEmail = decrypt(user_email);
        fetchServices(user_link);
        fetchProducts(user_link);
        fetchProfile(user_link);
      } catch (error) {
        console.error(error);
      }
    } else {
      console.warn("user_link is undefined");
    }
  }, [user_link]);

  const SkillsCard = ({ skill }, index) => (
    <>
      <button
        key={index}
        className='p-2 border border-pink-500 rounded rounded-full text-sm m-1'>
        {skill}
      </button>
    </>
  );
  return (
    <main className='flex flex-col items-center bg-white'>
      <SessionProvider>
        <Protected>
          <Header />
        </Protected>
        <div className='w-full mt-5'>
          {/* User's Profile */}
          {loadingPulse ? (
            <UserProfilePulseLoading />
          ) : (
            <div className='w-full justify-center flex'>
              <div className='w-7/12 bg-slate-50  pt-10 pl-2'>
                <div className='w-full flex min-h-96'>
                  <div className='w-1/6 h-48 relative rounded-lg'>
                    {userInfo.userprofilepic ? (
                      <Image
                        src={userInfo.userprofilepic}
                        alt='User Photo'
                        className='w-full h-full object-cover rounded-lg'
                        layout='fill'
                        priority
                      />
                    ) : (
                      <Image
                        src={"/images/others/avatar.png"}
                        alt='User Photo'
                        className='w-full h-full object-cover'
                        layout='fill'
                        priority
                      />
                    )}
                  </div>
                  <div className='w-5/6 pl-5 pt-5 pr-10 mb-5'>
                    <h1 className='text-4xl text-neutral-700 text-2xl font-bold'>
                      {userInfo.username} {userInfo.userlname}
                    </h1>
                    {userInfo.userprov ? (
                      <h1 className='text-xs text-sky-600 mt-2'>
                        {userInfo.userprov && userInfo.userprov.length > 0
                          ? JSON.parse(userInfo.userprov).label
                          : ""}
                        {", "}
                        {userInfo.usermuni && userInfo.usermuni.length > 0
                          ? JSON.parse(userInfo.usermuni).label
                          : ""}
                        , PHILIPPINES
                      </h1>
                    ) : (
                      ""
                    )}

                    <p className='mt-2  text-neutral-500 font-bold'>
                      {userInfo.userheadline}
                    </p>
                    <div
                      className='w-full mt-5 text-xs font-light'
                      dangerouslySetInnerHTML={{
                        __html: userInfo.userabout,
                      }}></div>
                    <div className='w-full items-center justify-center text-wrap pt-5'>
                      {userInfo.userskills
                        ? userInfo.userskills.map((skill, index) => (
                            <SkillsCard skill={skill} key={index} />
                          ))
                        : ""}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}

          {/* End User's Profile */}
          <div className='w-full flex justify-center'>
            <div className='w-7/12'>
              {/* Products */}
              {loadingPulse ? (
                <UserProductsPulseLoading />
              ) : (
                <Products userProduct={userProduct} />
              )}

              {/* End Products */}
              {/* Services */}
              {loadingPulse ? (
                <UserProductsPulseLoading />
              ) : (
                <Services userService={userService} />
              )}
            </div>
          </div>
        </div>
        <Footer />
      </SessionProvider>
    </main>
  );
}
