"use client";

import React, { useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";

export default function Products({ userProduct }) {
  function addCommas(numString) {
    return Number(numString).toLocaleString();
  }
  return (
    <>
      {/* Products */}
      <div className='pt-10'>
        <h1 className='text-2xl mb-10 text-slate-500 font-bold'>
          Digital Products
        </h1>
        <div className='w-full flex flex-wrap items-center justify-start'>
          {userProduct.map((products, index) => (
            <Link
              href={`/product/${products.product_id}`}
              key={index}
              className='w-4/12 h-60 flex  flex justify-center items-center'>
              <div className='w-full h-full p-1 flex justify-center relative transform transition-transform hover:scale-105'>
                {products.covers != null ? (
                  <>
                    <div className='absolute top-2 justify-center flex h-18 rounded-full w-5/12 bg-slate-800 hover:bg-orange-800 bg-opacity-90 hover:bg-opacity-90 p-1 cursor-pointer'>
                      <p className='text-white font-bold'>
                        Price {addCommas(products.price)}
                      </p>
                    </div>

                    <Image
                      src={`/${products.covers[0].product_cover.replace(
                        /^\.\/public\//,
                        ""
                      )}`}
                      alt='User Photo'
                      className='object-cover rounded-lg w-full h-full'
                      width={500}
                      height={500}
                      priority
                    />

                    <div className='absolute bottom-1 h-18 rounded-t-lg w-11/12 bg-slate-800 hover:bg-orange-800 bg-opacity-90 hover:bg-opacity-90 p-1 cursor-pointer transition-all duration-300'>
                      <div className='items-center h-10 flex w-full z-10'>
                        <p className='text-white text-xs'>
                          {products.product_name}
                        </p>
                      </div>
                      <div className='items-center mt-1 flex w-full z-10 opacity-90 hover:opacity-100 transition-opacity duration-300 mb-1'>
                        <div className='w-6/12 items-center justify-start flex'>
                          <p className='text-xs text-white'>Sold</p>
                          <p className='text-xs text-white font-semibold ml-4'>
                            {addCommas(products.total_sold)}
                          </p>
                        </div>
                        <div className='w-6/12 items-center justify-end flex'>
                          <p className='text-xs text-white'>Likes</p>
                          <p className='text-xs text-white font-semibold ml-4'>
                            {products.total_likes}
                          </p>
                        </div>
                      </div>
                    </div>
                  </>
                ) : (
                  <div className='w-full flex items-center justify-center'>
                    <div className='text-center p-2 bg-white rounded-lg shadow-lg'>
                      <h2 className='text-lg font-bold text-slate-500 mb-2'>
                        No Products Available
                      </h2>
                      <p className='text-gray-500 mb-2 text-xs'>
                        We're sorry, but there are no products available at the
                        moment. Please check back later.
                      </p>
                    </div>
                  </div>
                )}
              </div>
            </Link>
          ))}
        </div>
      </div>
      {/* End Products */}
    </>
  );
}
