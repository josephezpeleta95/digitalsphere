import Image from "next/image";

export default function TopFooter() {
  return (
    <div className='bg-white w-full flex justify-center'>
      <div className='w-full lg:w-9/12 mt-4 flex flex-col lg:flex-row justify-center items-center lg:space-x-10 p-4'>
        <div className='w-full lg:w-8/12 p-4 lg:p-10'>
          <p className='text-slate-500 text-base sm:text-sm md:text-md lg:text-lg leading-6 sm:leading-8 md:leading-9 lg:leading-10 text-center lg:text-left'>
            Our commitment is to provide a platform where Filipinos, both
            experienced professionals and newcomers, including those in search
            of job opportunities, can freely showcase their talents and digital
            creations to consumers worldwide, without constraints of location or
            expenses. We aim to create an equal opportunity environment,
            ensuring that every Filipino, regardless of background, can thrive
            in the digital marketplace. Join us in fostering a community where
            Filipino creativity flourishes, and talent knows no bounds.
          </p>
        </div>
        <div className='w-full lg:w-4/12 flex justify-center lg:justify-end'>
          <div className='w-full h-full sm:w-80 sm:h-80 md:w-96 md:h-96 lg:w-full lg:h-full'>
            <Image
              src={`/images/others/working-img.png`}
              alt={`Working People`}
              className='object-cover rounded-lg border-2 border-white'
              width={500}
              height={500}
              priority
            />
          </div>
        </div>
      </div>
    </div>
  );
}
