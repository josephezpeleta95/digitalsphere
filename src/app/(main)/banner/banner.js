export default function Banner() {
  return (
    //   Header Navbar
    <div className='p-4 lg:p-8  flex items-center justify-center h-60 lg:h-72'>
      <div className='max-w-4xl text-center'>
        <h1 className='text-2xl lg:text-4xl font-roboto leading-snug lg:leading-normal text-sky-800 font-semibold'>
          DigitalSphere{" "}
          <span className='font-normal'>
            is the dynamic platform connecting talent, services, and digital
            products in the Philippines.
          </span>
        </h1>
      </div>
    </div>
  );
}
