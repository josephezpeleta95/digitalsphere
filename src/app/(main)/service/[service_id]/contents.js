"use client";
import { useSession } from "next-auth/react";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import {
  c_servicereviews,
  c_submit_servicereview,
} from "@/app/controller/(main)/c_main";
import { c_getserviceprofile } from "@/app/controller/(main)/c_main";
import Loading from "@/app/(profile)/profile/components/loading";
import { encrypt, decrypt } from "../../../lib/crypt";
import { c_getprofile } from "@/app/controller/(profile)/c_userprofile";

export default function ProductProfile({ serviceId }) {
  const service_id = serviceId;
  const { data: session, status } = useSession();
  const [userId, setUserId] = useState("");
  const [serviceUserId, setServiceUserId] = useState("");
  const [serviceName, setServiceName] = useState("");
  const [serviceDescription, setServiceDescription] = useState("");
  const [serviceRate, setServiceRate] = useState("");
  const [serviceCover, setServiceCover] = useState([]);
  const [serviceWorkSetUp, setServiceWorkSetUp] = useState("");
  const [serviceWorkPer, setServiceWorkPer] = useState("");
  const [userName, setUserName] = useState("");
  const [userimage, setUserImage] = useState("");
  const [useremail, setUserEmail] = useState("");
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [reviews, setReviews] = useState([]);
  const [newReview, setNewReview] = useState("");
  const [newRating, setNewRating] = useState(0);
  const [encryptedParamter, setEncryptedParamter] = useState("");

  const fetchServiceProfile = async () => {
    try {
      const value = await c_getserviceprofile(service_id);
      if (value.rows.length > 0) {
        const services = value.rows[0];
        setServiceUserId(services.user_id);
        setUserEmail(services.useremail);
        setServiceName(services.service_name);
        setServiceDescription(services.service_description);
        setServiceCover(services.covers);
        setServiceRate(services.service_rate);
        setServiceWorkSetUp(services.service_work_setup);
        setServiceWorkPer(services.service_per);
        setUserName(`${services.username} ${services.userlname}`);
        setUserImage(services.userprofilepic);
        handleEncrypt(services.service_name, services.user_id);
      }
    } catch (error) {
      setError(error.message);
    } finally {
      setLoading(false);
    }
  };

  const fetchReviews = async () => {
    try {
      const value = await c_servicereviews(service_id);
      setReviews(value.rows);
    } catch (error) {
      setError(error.message);
    }
  };

  const handleReviewSubmit = async () => {
    try {
      await c_submit_servicereview(service_id, userId, newReview, newRating);
      setNewReview("");
      setNewRating(0);
      fetchReviews();
      alert("Submitted..");
    } catch (error) {
      console.log(error);
    }
  };
  const fetchUsersProfile = async () => {
    c_getprofile()
      .then((value) => {
        if (value.rows.length > 0) {
          const profile = value.rows[0];
          setUserId(profile.user_id);
        }
      })
      .catch((error) => {
        if (process.env.NODE_ENV === "development") {
          console.error(error);
        }
      });
  };

  function addCommas(numString) {
    return Number(numString).toLocaleString();
  }
  const handleEncrypt = (srvnme, usrid) => {
    const concatenatedText = `${srvnme}:${usrid}`;
    const encrypted = encrypt(concatenatedText);
    setEncryptedParamter(encrypted);
  };
  useEffect(() => {
    fetchServiceProfile();
    fetchReviews();
    fetchUsersProfile();
  }, []);

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }
  const isCurrentUser = session?.user?.email === useremail;

  return (
    <div className='w-8/12 min-h-screen mt-5'>
      <div className='w-full p-2 mt-5 flex flex-wrap rounded-lg relative'>
        {/* Background with opacity */}
        <div className='absolute inset-0 bg-gradient-to-r from-sky-500 via-orange-500 to-pink-500 rounded-lg opacity-50'></div>
        {/* Content */}
        {serviceCover.map((url, index) => (
          <div
            key={index}
            className='w-2/6 max-h-80 p-1 flex items-center transform transition-transform hover:scale-105 cursor-pointer'>
            <Image
              src={`/${url.service_cover.replace(/^\.\/public\//, "")}`}
              alt={`${url.service_cover_id} Image ${index}`}
              className='w-80 h-80 object-cover rounded-lg border-2 border-white'
              width={500}
              height={500}
              priority
            />
          </div>
        ))}
      </div>

      <div className='w-full p-2 mt-5 flex'>
        <div className='w-8/12 '>
          <div className='w-full flex '>
            <div>
              <Image
                src={`${userimage}`}
                alt={`Image`}
                className='w-10 h-10 object-cover rounded-full'
                width={500}
                height={500}
                priority
              />
            </div>
            <div className='items-center flex px-5'>
              <h1 className='text-slate-700 text-lg font-normal'>{userName}</h1>
            </div>
          </div>

          <h1 className='text-slate-700 text-2xl font-bold mt-5'>
            {serviceName}
          </h1>

          <div
            className='w-full p-2 mt-5'
            dangerouslySetInnerHTML={{
              __html: serviceDescription,
            }}></div>
        </div>
        <div className='w-4/12 min-h-96 max-h-96 rounded-lg border border-dashed border-orange-500 justify-center items-center flex flex-wrap'>
          <div className='w-10/12 pt-2 justify-start items-center flex'>
            <h1 className='text-slate-700 text-2xl font-normal'>Rate</h1>
          </div>
          <div className='w-10/12 pt-2 justify-center items-center flex border-b-2'>
            <h1 className='text-slate-700 text-5xl font-normal mb-5'>
              ₱{addCommas(serviceRate)}
            </h1>
          </div>
          <div className='w-10/12 pt-2 justify-center items-center flex'>
            <h1 className='text-slate-700 text-2xl font-normal mb-5'>
              {serviceWorkSetUp}/ {serviceWorkPer}
            </h1>
          </div>
          {session && !isCurrentUser && (
            <div className='w-full pt-2 justify-center items-end flex mb-5'>
              <Link
                href={`/profile?page=msg&subject=${encodeURIComponent(
                  serviceName
                )}&send_to=${encodeURIComponent(
                  userName
                )}&xdto=${encodeURIComponent(
                  serviceUserId
                )}&crx=${encodeURIComponent(encryptedParamter)}`}
                passHref>
                <button className='bg-sky-600 p-2 text-white rounded-full px-4'>
                  Message
                </button>
              </Link>
            </div>
          )}

          <div className='w-4/12 bg-orange-400 h-8 items-center justify-center flex px-2 font-semibold text-white mr-1 rounded'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              fill='none'
              viewBox='0 0 24 24'
              strokeWidth='1.5'
              stroke='currentColor'
              className='w-4 h-4 mr-2'>
              <path
                strokeLinecap='round'
                strokeLinejoin='round'
                d='M6.633 10.25c.806 0 1.533-.446 2.031-1.08a9.041 9.041 0 0 1 2.861-2.4c.723-.384 1.35-.956 1.653-1.715a4.498 4.498 0 0 0 .322-1.672V2.75a.75.75 0 0 1 .75-.75 2.25 2.25 0 0 1 2.25 2.25c0 1.152-.26 2.243-.723 3.218-.266.558.107 1.282.725 1.282m0 0h3.126c1.026 0 1.945.694 2.054 1.715.045.422.068.85.068 1.285a11.95 11.95 0 0 1-2.649 7.521c-.388.482-.987.729-1.605.729H13.48c-.483 0-.964-.078-1.423-.23l-3.114-1.04a4.501 4.501 0 0 0-1.423-.23H5.904m10.598-9.75H14.25M5.904 18.5c.083.205.173.405.27.602.197.4-.078.898-.523.898h-.908c-.889 0-1.713-.518-1.972-1.368a12 12 0 0 1-.521-3.507c0-1.553.295-3.036.831-4.398C3.387 9.953 4.167 9.5 5 9.5h1.053c.472 0 .745.556.5.96a8.958 8.958 0 0 0-1.302 4.665c0 1.194.232 2.333.654 3.375Z'
              />
            </svg>
          </div>
        </div>
      </div>

      <div className='w-full p-2 mt-5'>
        <h2 className='text-2xl font-semibold mb-4'>Reviews and Ratings</h2>

        {session
          ? !isCurrentUser && (
              <div className='mb-5'>
                <div className='mb-4'>
                  <label className='block text-sm font-medium text-gray-700 mb-2'>
                    Your Review
                  </label>
                  <textarea
                    value={newReview}
                    onChange={(e) => setNewReview(e.target.value)}
                    className='w-full px-3 py-2 border rounded-lg'
                    required></textarea>
                </div>
                <div className='mb-4'>
                  <label className='block text-sm font-medium text-gray-700 mb-2'>
                    Your Rating
                  </label>
                  <select
                    value={newRating}
                    onChange={(e) => setNewRating(Number(e.target.value))}
                    className='w-full px-3 py-2 border rounded-lg'
                    required>
                    <option value={0}>Select Rating</option>
                    {[1, 2, 3, 4, 5].map((rating) => (
                      <option key={rating} value={rating}>
                        {rating} Star{rating > 1 && "s"}
                      </option>
                    ))}
                  </select>
                </div>
                <button
                  type='submit'
                  className='bg-sky-600 text-white px-4 py-2 rounded-lg'
                  onClick={() => handleReviewSubmit()}>
                  Submit Review
                </button>
              </div>
            )
          : ""}
        <div>
          {" "}
          {reviews.length === 0 ? (
            <p>No reviews yet. Be the first to review!</p>
          ) : (
            reviews.map((review, index) => (
              <div key={index} className='mb-4 p-4 border rounded-lg'>
                <div className='flex items-center mb-2'>
                  {review.userprofilepic ? (
                    <Image
                      src={`${review.userprofilepic}`}
                      alt={`Image`}
                      className='w-10 h-10 object-cover rounded-full'
                      width={500}
                      height={500}
                      priority
                    />
                  ) : (
                    <Image
                      src={`/images/others/default.png`}
                      alt={`Image`}
                      className='w-10 h-10 object-cover rounded-full'
                      width={500}
                      height={500}
                      priority
                    />
                  )}
                  <h4 className='font-normal ml-2'>{review.username}</h4>
                </div>
                <div className='flex items-center mb-2'>
                  {Array(review.rating)
                    .fill()
                    .map((_, i) => (
                      <svg
                        key={i}
                        xmlns='http://www.w3.org/2000/svg'
                        fill='currentColor'
                        viewBox='0 0 24 24'
                        stroke='currentColor'
                        className='w-4 h-4 text-yellow-500'>
                        <path
                          strokeLinecap='round'
                          strokeLinejoin='round'
                          strokeWidth={2}
                          d='M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.286 3.974a1 1 0 00.95.69h4.174c.969 0 1.371 1.24.588 1.81l-3.385 2.457a1 1 0 00-.364 1.118l1.287 3.974c.3.921-.755 1.688-1.54 1.118l-3.385-2.457a1 1 0 00-1.176 0l-3.385 2.457c-.785.57-1.84-.197-1.54-1.118l1.287-3.974a1 1 0 00-.364-1.118L2.82 9.401c-.784-.57-.381-1.81.588-1.81h4.174a1 1 0 00.95-.69l1.286-3.974z'
                        />
                      </svg>
                    ))}
                </div>
                <p>{review.review_text}</p>
              </div>
            ))
          )}
        </div>
      </div>
    </div>
  );
}
