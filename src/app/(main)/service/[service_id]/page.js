"use client";
import Header from "@header/header";
import Footer from "@footer/footer";
import { SessionProvider } from "next-auth/react";
import React from "react";
import Content from "./contents";
import Protected from "../../../controller/protected";

export default function ProductProfile({ params }) {
  const service_id = params.service_id;
  return (
    <main className='flex flex-col items-center bg-white'>
      <SessionProvider>
        <Header />
        <Content serviceId={service_id} />
        <Footer />
      </SessionProvider>
    </main>
  );
}
