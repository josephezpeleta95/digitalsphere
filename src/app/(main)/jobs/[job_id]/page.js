"use client";
import Header from "@header/header";
import Footer from "@footer/footer";
import { SessionProvider } from "next-auth/react";
import Content from "./contents";

export default function JobsProfile({ params }) {
  const job_id = params.job_id;

  return (
    <main className='flex flex-col items-center bg-white'>
      <SessionProvider>
        <Header />
        <Content jobId={job_id} />
        <Footer />
      </SessionProvider>
    </main>
  );
}
