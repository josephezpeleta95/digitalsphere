"use client";

import { useSession } from "next-auth/react";
import React, { useEffect, useState } from "react";
import Loading from "@/app/(profile)/profile/components/loading";
import { c_getJobprofile } from "@/app/controller/(main)/c_main";
import ApplicationModal from "./../../../(profile)/profile/jobs/jobapplicationmodal";
import { c_getprofile } from "@/app/controller/(profile)/c_userprofile";

export default function JobProfile({ jobId }) {
  const job_id = jobId;
  const { data: session } = useSession();
  const [userId, setUserId] = useState("");
  const [jobTitle, setJobTitle] = useState("");
  const [jobDescription, setJobDescription] = useState("");
  const [jobLocation, setJobLocation] = useState("");
  const [datePosted, setDatePosted] = useState("");
  const [salaryRange, setSalaryRange] = useState("");
  const [closedDate, setClosedDate] = useState("");
  const [setup, setSetup] = useState("");
  const [skills, setSkills] = useState([]);
  const [workArrangement, setWorkArrangement] = useState("");
  const [companyId, setCompanyId] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [companyLocation, setCompanyLocation] = useState("");
  const [companyEmail, setCompanyEmail] = useState("");
  const [contactNumber, setContactNumber] = useState("");
  const [contactPerson, setContactPerson] = useState("");
  const [website, setWebsite] = useState("");
  const [industry, setIndustry] = useState("");
  const [companySize, setCompanySize] = useState("");
  const [showModal, setShowModal] = useState(false); // State for modal visibility
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const fetchJobProfile = async () => {
    c_getJobprofile(job_id)
      .then((value) => {
        if (value.rows.length > 0) {
          console.log(value.rows[0]);
          const job = value.rows[0];
          setJobTitle(job.title);
          setJobDescription(job.description);
          setJobLocation(job.job_location);
          setDatePosted(job.date_posted);
          setSalaryRange(job.salary_range);
          setClosedDate(job.closed_date);
          setSetup(job.setup);
          setSkills(JSON.parse(job.skills));
          setWorkArrangement(job.work_arrangement);
          setCompanyId(job.company_id);
          setCompanyName(job.company_name);
          setCompanyLocation(job.company_location);
          setCompanyEmail(job.company_email);
          setContactNumber(job.contact_number);
          setContactPerson(job.contact_person);
          setWebsite(job.website);
          setIndustry(job.industry);
          setCompanySize(job.size);
        }
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const handleQuickApply = () => {
    setShowModal(true);
  };

  const fetchUsersProfile = async () => {
    c_getprofile()
      .then((value) => {
        if (value.rows.length > 0) {
          const userProfile = value.rows[0];
          setUserId(userProfile.user_id);
        }
      })
      .catch((error) => {
        if (process.env.NODE_ENV === "development") {
          console.error(error);
        }
      });
  };

  useEffect(() => {
    fetchUsersProfile();
    fetchJobProfile();
  }, []);

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <>
      <div className='w-8/12 min-h-screen mt-5 mx-auto'>
        <div className='bg-white shadow-lg rounded-lg p-6'>
          <div className='border-b pb-4 mb-4'>
            <h1 className='text-3xl font-bold text-gray-800'>{jobTitle}</h1>
            <p className='text-gray-600'>
              {companyName} - {companyLocation}
            </p>
          </div>

          <div className='mb-6'>
            <h2 className='text-2xl font-semibold text-gray-800 mb-2'>
              Job Description
            </h2>
            <div className='prose'>
              <div
                className='text-sm font-sans text-slate-700'
                dangerouslySetInnerHTML={{
                  __html: jobDescription,
                }}
              />
            </div>
          </div>

          <div className='mb-6'>
            <h2 className='text-2xl font-semibold text-gray-800 mb-2'>
              Job Details
            </h2>
            <ul className='list-disc list-inside text-gray-700'>
              <li>
                <strong>Location:</strong> {jobLocation}
              </li>
              <li>
                <strong>Date Posted:</strong>
                {new Date(datePosted).toLocaleDateString()}
              </li>
              <li>
                <strong>Salary Range:</strong> {salaryRange}
              </li>
              <li>
                <strong>Closing Date:</strong>{" "}
                {new Date(closedDate).toLocaleDateString()}
              </li>
              <li>
                <strong>Setup:</strong> {setup}
              </li>
              <li>
                <strong>Work Arrangement:</strong> {workArrangement}
              </li>
            </ul>
          </div>

          <div className='mb-6'>
            <h2 className='text-2xl font-semibold text-gray-800 mb-2'>
              Skills Required
            </h2>
            <ul className='list-disc list-inside text-gray-700'>
              {skills.map((skill, index) => (
                <li key={index}>{skill}</li>
              ))}
            </ul>
          </div>

          <div className='border-t pt-4 mt-4'>
            <h2 className='text-2xl font-semibold text-gray-800 mb-2'>
              Company Information
            </h2>
            <ul className='list-disc list-inside text-gray-700'>
              <li>
                <strong>Company Name:</strong> {companyName}
              </li>
              <li>
                <strong>Location:</strong> {companyLocation}
              </li>
              <li>
                <strong>Email:</strong>{" "}
                <a
                  href='mailto:projects@projectmanagement.com'
                  className='text-blue-500'>
                  {companyEmail}
                </a>
              </li>
              <li>
                <strong>Contact Number:</strong> {contactNumber}
              </li>
              <li>
                <strong>Contact Person:</strong> {contactPerson}
              </li>
              <li>
                <strong>Website:</strong>{" "}
                <a href={website} target='_blank' className='text-blue-500'>
                  {website}
                </a>
              </li>
              <li>
                <strong>Industry:</strong> {industry}
              </li>
              <li>
                <strong>Company Size:</strong> {companySize}
              </li>
            </ul>
          </div>
          <div className='w-full flex justify-end'>
            {session ? (
              <button
                className='sm:w-3/12 w-full bg-blue-500 text-white px-4 py-2 rounded text-xs'
                onClick={() => handleQuickApply()}>
                Quick Apply
              </button>
            ) : (
              ""
              // <button
              //   className='sm:w-3/12 w-full bg-blue-500 text-white px-4 py-2 rounded text-xs'
              //   onClick={() => handleOptionResume("build")}>
              //   Build your resume
              // </button>
            )}
          </div>
        </div>
      </div>
      {showModal && (
        <ApplicationModal
          setShowModal={setShowModal}
          user_Id={userId}
          job_Id={job_id}
          company_Id={companyId}
        />
      )}
    </>
  );
}
