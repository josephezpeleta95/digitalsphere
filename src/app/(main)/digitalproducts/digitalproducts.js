import React, { useEffect, useState } from "react";
import { c_products } from "@/app/controller/(main)/c_main";
import Link from "next/link";
import { DigitalProductsPulseLoading } from "../components/pulseloading";

export default function DigitalProducts() {
  const [products, setProducts] = useState([]);
  const [loadingPulse, setLoadingPulse] = useState(true);

  function truncateString(str, maxLength) {
    if (str.length <= maxLength) {
      return str;
    } else {
      return str.slice(0, maxLength - 3) + "...";
    }
  }

  function addCommas(numString) {
    return Number(numString).toLocaleString();
  }

  const ProductCard = ({
    username,
    userlname,
    covers,
    userprofilepic,
    price,
    total_sold,
    product_name,
    total_likes,
    product_id,
  }) => (
    <Link
      href={`/product/${product_id}`}
      target='_blank'
      className='w-full sm:w-1/2 md:w-1/3 lg:w-1/4 px-4 py-2'>
      <div className='bg-white rounded-lg shadow-lg overflow-hidden'>
        <div
          className='h-48 bg-cover bg-center flex items-end justify-center transition-transform transform hover:scale-105'
          style={{
            backgroundImage: `url(/${covers[0].product_cover.replace(
              /^\.\/public\//,
              ""
            )})`,
          }}>
          <div className='bg-slate-600 bg-opacity-90 w-11/12 p-1 border rounded-full flex mb-1'>
            <div
              className='w-8 h-8 rounded-full bg-cover bg-center border'
              style={{ backgroundImage: `url(${userprofilepic})` }}></div>
            <div className='flex items-center'>
              <h5 className='text-xs ml-1 text-white truncate'>
                {username} {userlname}
              </h5>
            </div>
          </div>
        </div>
        <div className='p-4'>
          <div className='flex justify-between text-slate-600 h-8 font-semibold'>
            <div className='flex-wrap text-xs w-8/12'>
              <h5 className='line-clamp-2'>{product_name}</h5>
            </div>
            <div className='text-xs  w-4/12 justify-end flex'>
              <h5> Sold {total_sold ? total_sold : 0}</h5>
            </div>
          </div>
          <div className='flex justify-between text-sky-600'>
            <div className='text-xs font-semibold'>₱ {addCommas(price)}</div>
            <div className='text-xs'>Likes ({total_likes})</div>
          </div>
        </div>
      </div>
    </Link>
  );

  const fetchProducts = () => {
    c_products()
      .then((value) => {
        setProducts(value.rows);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoadingPulse(false);
      });
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <div className='bg-white w-9/12 mt-4'>
      <div className='w-full p-5 text-2xl'>
        <h1 className='font-roboto leading-normal text-sky-700 font-semibold'>
          Digital Products
        </h1>
      </div>
      <div className='flex justify-center'>
        <div className='w-full flex flex-wrap items-center p-4'>
          {loadingPulse ? (
            <DigitalProductsPulseLoading />
          ) : (
            products.map((productprofile, index) => (
              <ProductCard {...productprofile} key={index} />
            ))
          )}
        </div>
      </div>
      <div className='flex justify-center items-center mb-10 w-full'>
        <Link href={`/all-digital-products`} target='_blank'>
          <button className='border border-slate-500 rounded-full py-2 px-4 text-sky-700 hover:bg-orange-500 hover:text-white hover:border-orange-500'>
            Show more
          </button>
        </Link>
      </div>
    </div>
  );
}
