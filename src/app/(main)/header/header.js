"use client";
import Link from "next/link";
import { useSession, signOut } from "next-auth/react";
import React, { useEffect, useState, useRef } from "react";
import { c_getprofile } from "@/app/controller/(profile)/c_userprofile";
import { c_logout } from "@/app/controller/(main)/c_main";
import Image from "next/image";

export default function Header() {
  const { data: session } = useSession();
  const [usersProfilePic, setUsersProfilePic] = useState("");
  const [usersName, setUsersName] = useState("");
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const fetchUsersProfile = async () => {
    c_getprofile()
      .then((value) => {
        if (value.rows.length > 0) {
          const profile = value.rows[0];
          setUsersProfilePic(profile.userprofilepic);
          setUsersName(profile.username);
        }
      })
      .catch((error) => {
        if (process.env.NODE_ENV === "development") {
          console.error(error);
        }
      });
  };

  const handleSignOutWith = () => {
    c_logout()
      .then((response) => {
        if (response.code === "1111") {
          signOut({
            redirect: true,
            callbackUrl: "http://localhost:3000/login",
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchUsersProfile();
  }, []);
  return (
    //   Header Navbar
    <div className='z-10 max-w-6xl p-1 py-4 lg:w-full  items-center justify-between font-mono text-sm flex mt-2 border-b-2 border-slate-300 border-dashed'>
      <div className='w-3/12'>
        <Link href='/'>
          <img
            className='ml-2'
            src='/images/header/cayap.png'
            alt='Cayap Logo'
            width='100'
            height='100'
          />
        </Link>
      </div>
      <div className='hidden lg:w-3/5 lg:flex lg:justify-end pr-10'>
        <ul className='items-center flex text-sm w-7/12 mr-4'>
          <li className='mr-8 font-sans cursor-pointer hover:text-pink-500 border-b-4  border-white hover:border-b-4 hover:border-pink-500 text-center ease-in duration-300'>
            <Link href='../all-online-jobs'>Online Jobs</Link>
          </li>
          <li className='mr-8 font-sans cursor-pointer hover:text-pink-500 border-b-4  border-white hover:border-b-4 hover:border-pink-500 text-center ease-in duration-300'>
            <Link href='./all-digital-products'>Digital Products</Link>
          </li>
          <li className='font-sans mr-8 cursor-pointer hover:text-pink-500 border-b-4  border-white hover:border-b-4 hover:border-pink-500 text-center ease-in duration-300'>
            <Link href='./all-services'>Online Services</Link>
          </li>
        </ul>
        <ul className='items-center flex text-sm'>
          <li className='relative flex-1 font-sans cursor-pointer hover:text-orange-500'>
            {!session ? (
              <Link href='../login'>Login / Signup</Link>
            ) : (
              <div className='relative'>
                <div className='flex items-center'>
                  {usersProfilePic ? (
                    <>
                      <Image
                        src={`${usersProfilePic.replace(/^\.\/public\//, "")}`}
                        alt={`Profile Picture`}
                        className='w-10 h-10 object-cover rounded-full'
                        width={40}
                        height={40}
                        priority
                      />
                      <svg
                        xmlns='http://www.w3.org/2000/svg'
                        fill='none'
                        viewBox='0 0 24 24'
                        strokeWidth='1.5'
                        stroke='currentColor'
                        onClick={() => setDropdownOpen(!dropdownOpen)}
                        className='w-6 h-6'>
                        <path
                          strokeLinecap='round'
                          strokeLinejoin='round'
                          d='M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5'
                        />
                      </svg>
                    </>
                  ) : (
                    <>
                      <Image
                        src={`/images/others/default.png`}
                        alt={`Profile Picture`}
                        className='w-10 h-10 object-cover rounded-full'
                        width={500}
                        height={500}
                        priority
                      />
                      <svg
                        xmlns='http://www.w3.org/2000/svg'
                        fill='none'
                        viewBox='0 0 24 24'
                        strokeWidth='1.5'
                        stroke='currentColor'
                        onClick={() => setDropdownOpen(!dropdownOpen)}
                        className='w-6 h-6'>
                        <path
                          strokeLinecap='round'
                          strokeLinejoin='round'
                          d='M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5'
                        />
                      </svg>
                    </>
                  )}
                </div>

                {dropdownOpen && (
                  <div className='absolute right-0 mt-2 w-48 bg-white border border-gray-200 rounded-lg shadow-lg z-9999'>
                    <p className='block px-4 py-2 text-gray-800 hover:bg-gray-100 mt-2'>
                      Hello! {usersName}
                    </p>
                    <Link
                      href='../profile?page=mypdct'
                      className='block px-4 py-2 text-gray-800 hover:bg-gray-100'>
                      Profile
                    </Link>
                    <button
                      className='block w-full text-left px-4 py-2 text-gray-800 hover:bg-gray-100 mb-2'
                      onClick={() => handleSignOutWith()}>
                      Logout
                    </button>
                  </div>
                )}
              </div>
            )}
          </li>
        </ul>
      </div>
      <div className='lg:hidden w-64 w-3/5 flex justify-end pr-10'>
        <button className='lg:hidden ml-auto bg-skay-300'>Menu</button>
      </div>
    </div>
  );
}
