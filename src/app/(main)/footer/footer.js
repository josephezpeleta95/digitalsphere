import Image from "next/image";
import Link from "next/link";
export default function TopFooter() {
  return (
    <div className='bg-white w-full flex justify-center mt-20 mb-20'>
      <div className='w-full lg:w-9/12 mt-4 flex flex-col lg:flex-row justify-center items-center border-t-2 border-slate-300 border-dashed p-4 lg:p-10'>
        <div className='w-full lg:w-5/12 p-4 lg:p-10 flex justify-center lg:justify-start'>
          <Image
            src={`/images/header/cayap.png`}
            alt={`Logo`}
            className='object-cover rounded-lg border-2 border-white'
            width={130}
            height={100}
            priority
          />
        </div>
        <div className='w-full lg:w-7/12 flex flex-col lg:flex-row'>
          <div className='w-full lg:w-4/12 p-2'>
            <ul className='text-sm text-slate-500 font-medium text-center lg:text-left'>
              <li className='mb-2'>
                <Link href='../all-online-jobs'>Online Jobs</Link>
              </li>
              <li className='mb-2'>
                <Link href='./all-digital-products'>Digital Products</Link>
              </li>
              <li className='mb-2'>
                <Link href='./all-services'>Online Services</Link>
              </li>
            </ul>
          </div>
          <div className='w-full lg:w-4/12 p-2'>
            <ul className='text-sm text-slate-500 font-medium text-center lg:text-left'>
              <li className='mb-2'>Help center</li>
              <li className='mb-2'>Terms & conditions</li>
              <li className='mb-2'>Privacy policy</li>
              <li className='mb-2'>Contact us</li>
              <li className='mb-2'>About us</li>
            </ul>
          </div>
          <div className='w-full lg:w-4/12 p-2'>
            <p className='text-pink-500 text-sm leading-5 text-center lg:text-left'>
              Join our community to explore endless opportunities for growth,
              collaboration, and success.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
