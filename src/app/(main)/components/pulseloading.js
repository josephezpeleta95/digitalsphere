import React from "react";

const PulseLoading = () => {
  return (
    <div className='p-4 max-w-xs mx-auto bg-white shadow-md rounded-md'>
      {/* Circle */}

      <div className='animate-pulse w-full h-full object-cover rounded-full bg-gray-300 h-12 w-12 mb-4 '></div>
      {/* One Line */}
      <div className='animate-pulse bg-gray-300 h-4 rounded w-3/4 mb-4'></div>
      {/* Two Lines */}
      <div className='animate-pulse bg-gray-300 h-4 rounded mb-4'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-5/6 mb-4'></div>
      {/* Four Lines */}
      <div className='animate-pulse bg-gray-300 h-4 rounded w-full mb-2'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-5/6 mb-2'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-3/4 mb-2'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-2/3 mb-2'></div>
    </div>
  );
};

export const CirclePulseLoading = () => {
  return (
    <div className='animate-pulse rounded-full bg-gray-300 h-10 w-10 mb-2'></div>
  );
};
export const TalentsPulseLoading = () => {
  return (
    <div className='flex space-x-4 w-full'>
      <div className='animate-pulse bg-gray-300 h-96 w-3/12'></div>
      <div className='animate-pulse bg-gray-300 h-96 w-3/12'></div>
      <div className='animate-pulse bg-gray-300 h-96 w-3/12'></div>
      <div className='animate-pulse bg-gray-300 h-96 w-3/12'></div>
    </div>
  );
};

export const DigitalProductsPulseLoading = () => {
  return (
    <div className='flex flex-wrap w-full'>
      {[...Array(10)].map((_, index) => (
        <div className='p-1 h-48 w-1/5' key={index}>
          <div
            key={index}
            className='animate-pulse h-full w-full bg-gray-300 mb-4'></div>
        </div>
      ))}
    </div>
  );
};

export const ServicesPulseLoading = () => {
  return (
    <div className='flex flex-wrap w-full'>
      {[...Array(10)].map((_, index) => (
        <div className='p-1 h-48 w-1/5' key={index}>
          <div
            key={index}
            className='animate-pulse h-full w-full bg-gray-300 mb-4'></div>
        </div>
      ))}
    </div>
  );
};

export const JobsPulseLoading = () => {
  return (
    <div className='flex flex-wrap w-full'>
      {[...Array(9)].map((_, index) => (
        <div className='p-1 h-48 w-4/12' key={index}>
          <div
            key={index}
            className='animate-pulse h-full w-full bg-gray-300 mb-4'></div>
        </div>
      ))}
    </div>
  );
};

export const UserProductsPulseLoading = () => {
  return (
    <div className='flex flex-wrap w-full'>
      {[...Array(6)].map((_, index) => (
        <div className='p-1 h-48 w-4/12'>
          <div
            key={index}
            className='animate-pulse h-full w-full bg-gray-300 mb-4'></div>
        </div>
      ))}
    </div>
  );
};

export const OneLinePulseLoading = () => {
  return (
    <div className='animate-pulse bg-gray-300 h-4 rounded w-3/4 mb-4'></div>
  );
};

export const TwoLinesPulseLoading = () => {
  return (
    <>
      <div className='animate-pulse bg-gray-300 h-4 rounded mb-4'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-5/6 mb-4'></div>
    </>
  );
};

export const FourLinesPulseLoading = () => {
  return (
    <>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-full mb-2'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-5/6 mb-2'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-3/4 mb-2'></div>
      <div className='animate-pulse bg-gray-300 h-4 rounded w-2/3 mb-2'></div>
    </>
  );
};

export const UserProfilePulseLoading = () => {
  return (
    <>
      <div className='w-full min-h-96 justify-center flex'>
        <div className='w-7/12 bg-slate-50 pt-10 pl-2'>
          <div className='w-full flex'>
            <div className='w-1/6 h-48 relative overflow-hidden rounded-lg animate-pulse bg-gray-300'></div>
            <div className='w-5/6 h-48 pl-5 pt-5 pr-10'>
              <div className='h-8 w-3/4 bg-gray-300 animate-pulse mb-2'></div>
              <div className='h-4 w-1/2 bg-gray-300 animate-pulse mb-2'></div>
              <div className='h-6 w-full bg-gray-300 animate-pulse mb-2'></div>
              <div className='h-32 w-full bg-gray-300 animate-pulse mb-2'></div>
              <div className='flex flex-wrap mt-5'>
                {[...Array(5)].map((_, index) => (
                  <div
                    key={index}
                    className='h-6 w-1/4 bg-gray-300 animate-pulse mr-2 mb-2'></div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default PulseLoading;
