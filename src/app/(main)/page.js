"use client";
import Header from "./header/header";
import Banner from "./banner/banner";
import Talents from "./talent/talents";
import DigitalProducts from "./digitalproducts/digitalproducts";
import OnlineServices from "./onlineservices/onlineservices";
import OnlineJobs from "./onlinejobs/onlinejobs";
import TopFooter from "./topfooter/topfooter";
import Footer from "./footer/footer";
import { SessionProvider } from "next-auth/react";
import dynamic from "next/dynamic";

// const DigitalProducts = dynamic(
//   () => import("./digitalproducts/digitalproducts"),
//   {
//     ssr: false, // Disable server-side rendering if the component relies on browser-specific APIs
//     loading: () => <p>Loading...</p>, // Optionally provide a loading component
//   }
// );

export default function Home() {
  return (
    <>
      <main className='flex flex-col items-center justify-between bg-white'>
        <SessionProvider>
          <Header />
        </SessionProvider>
        <Banner />
        <Talents />
        <DigitalProducts />
        <OnlineServices />
        <OnlineJobs />
        <TopFooter />
        <Footer />
      </main>
    </>
  );
}
