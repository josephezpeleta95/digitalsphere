import axios from "axios";

const c_getproductprofile = async (product_id) => {
  let c_getproductprofile = axios
    .get(`http://localhost:3000/api/v_productprofile/${product_id}`, {
      headers: {
        "Content-Type": "application/json",
      },
      withCredentials: true, // Include credentials (cookies) with the request
    })
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getproductprofile;
};

export { c_getproductprofile };
