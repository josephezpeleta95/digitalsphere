import { useRouter } from "next/navigation";
import { useSession } from "next-auth/react";
import React, { useEffect, useState } from "react";

const Protected = ({ children }) => {
  const router = useRouter();
  const { data: session, status } = useSession();
  const [isSessionLoaded, setIsSessionLoaded] = useState(false);

  useEffect(() => {
    const checkAuthentication = async () => {
      try {
        if (status === "authenticated" || status === "loading") {
          setIsSessionLoaded(true);
        } else {
          router.push("/");
        }
      } catch (error) {
        console.error("Error while redirecting:", error);
      }
    };

    checkAuthentication();
  }, [status, router]);

  if (!isSessionLoaded) {
    return null; // or you can render a loading spinner or a message indicating that the session is being checked
  }

  if (status === "authenticated") {
    // Pass the value as a prop to the children component
    const recursiveCloneChildren = (children) => {
      return React.Children.map(children, (child) => {
        if (!React.isValidElement(child)) {
          return child;
        }

        // Clone child and pass additional props
        return React.cloneElement(
          child,
          { prop: session },
          recursiveCloneChildren(child.props.children)
        );
      });
    };

    // Render your component content, including children, only if session is loaded
    return isSessionLoaded ? recursiveCloneChildren(children) : children;
  }

  return null; // or you can render a message indicating that the user is being redirected
};

export default Protected;
