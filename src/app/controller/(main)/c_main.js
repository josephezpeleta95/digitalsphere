import axios from "axios";

const c_logout = async () => {
  try {
    const c_logout = await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/api/main/logout`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      }
    );
    const result = c_logout.data;
    if (result.success) {
      return { code: "1111", message: "Information Saved" };
    } else {
      console.error("Error:", result.message);
      return { code: "0000", message: result.message };
    }
  } catch (error) {
    console.error("Error:", error);
    return { code: "0000", message: "Catch an Error!" };
  }
};
const c_skills = async () => {
  try {
    let c_talents = await axios
      .get(`${process.env.NEXT_PUBLIC_API_URL}/api/main/v_skills`, {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      })
      .then((result) => result.data)
      .catch((error) => {
        console.error("Error occurred:", error);
      });
    return c_talents;
  } catch (error) {
    console.error("Error:", error.message);
  }
};
const c_talents = async () => {
  try {
    let c_talents = await axios
      .get(`${process.env.NEXT_PUBLIC_API_URL}/api/main/v_talents`, {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      })
      .then((result) => result.data)
      .catch((error) => {
        console.error("Error occurred:", error);
      });
    return c_talents;
  } catch (error) {
    console.error("Error:", error.message);
  }
};
const c_products = async () => {
  try {
    let c_products = await axios
      .get(`${process.env.NEXT_PUBLIC_API_URL}/api/main/v_products`, {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      })
      .then((result) => result.data)
      .catch((error) => {
        console.error("Error occurred:", error);
      });
    return c_products;
  } catch (error) {
    console.error("Error:", error.message);
  }
};
const c_pftalent = async (userLink) => {
  try {
    let c_talentprofile = await axios
      .get(`${process.env.NEXT_PUBLIC_API_URL}/api/main/v_talent/${userLink}`, {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      })
      .then((result) => result.data)
      .catch((error) => {
        console.error("Error occurred:", error);
      });
    return c_talentprofile;
  } catch (error) {
    console.error("Error:", error.message);
  }
};
const c_pfproduct = async (userLink) => {
  try {
    let c_pfproduct = await axios
      .get(
        `${process.env.NEXT_PUBLIC_API_URL}/api/main/v_products/${userLink}`,
        {
          headers: {
            "Content-Type": "application/json",
          },
          withCredentials: true,
        }
      )
      .then((result) => result.data)
      .catch((error) => {
        console.error("Error occurred:", error);
      });
    return c_pfproduct;
  } catch (error) {
    console.error("Error:", error.message);
  }
};
const c_pfservices = async (userLink) => {
  try {
    let c_pfservices = await axios
      .get(
        `${process.env.NEXT_PUBLIC_API_URL}/api/main/v_services/${userLink}`,
        {
          headers: {
            "Content-Type": "application/json",
          },
          withCredentials: true,
        }
      )
      .then((result) => result.data)
      .catch((error) => {
        console.error("Error occurred:", error);
      });
    return c_pfservices;
  } catch (error) {
    console.error("Error:", error.message);
  }
};
const c_services = async () => {
  try {
    let c_services = await axios
      .get(`${process.env.NEXT_PUBLIC_API_URL}/api/main/v_services`, {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      })
      .then((result) => result.data)
      .catch((error) => {
        console.error("Error occurred:", error);
      });
    return c_services;
  } catch (error) {
    console.error("Error:", error.message);
  }
};
const c_productreviews = async (product_id) => {
  try {
    let c_productreviews = await axios
      .get(
        `${process.env.NEXT_PUBLIC_API_URL}/api/main/v_productreview/${product_id}`,
        {
          headers: {
            "Content-Type": "application/json",
          },
          withCredentials: true,
        }
      )
      .then((result) => result.data)
      .catch((error) => {
        console.error("Error occurred:", error);
      });
    return c_productreviews;
  } catch (error) {
    console.error("Error:", error.message);
  }
};
const c_servicereviews = async (service_id) => {
  try {
    let c_servicereviews = await axios
      .get(
        `${process.env.NEXT_PUBLIC_API_URL}/api/main/v_servicereview/${service_id}`,
        {
          headers: {
            "Content-Type": "application/json",
          },
          withCredentials: true,
        }
      )
      .then((result) => result.data)
      .catch((error) => {
        console.error("Error occurred:", error);
      });
    return c_servicereviews;
  } catch (error) {
    console.error("Error:", error.message);
  }
};
const c_submit_servicereview = async (
  service_id,
  userId,
  newReview,
  newRating
) => {
  try {
    const c_submit_servicereview = await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/api/main/v_servicereview`,
      {
        serviceId: service_id,
        userId: userId,
        newReview: newReview,
        newRating: newRating,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      }
    );
    const result = c_submit_servicereview.data;
    if (result.success) {
      return { code: "1111", message: "Information Saved" };
    } else {
      console.error("Error:", result.message);
      return { code: "0000", message: result.message };
    }
  } catch (error) {
    console.error("Error:", error);
    return { code: "0000", message: "Catch an Error!" };
  }
};
const c_submit_productreview = async (
  product_id,
  userId,
  newReview,
  newRating
) => {
  try {
    const c_submit_productreview = await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/api/main/v_productreview`,
      {
        productId: product_id,
        userId: userId,
        newReview: newReview,
        newRating: newRating,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      }
    );
    const result = c_submit_productreview.data;
    if (result.success) {
      return { code: "1111", message: "Information Saved" };
    } else {
      console.error("Error:", result.message);
      return { code: "0000", message: result.message };
    }
  } catch (error) {
    console.error("Error:", error);
    return { code: "0000", message: "Catch an Error!" };
  }
};
const c_getJobListings = async () => {
  let c_getJobListings = axios
    .get(`${process.env.NEXT_PUBLIC_API_URL}/api/main/jobs/joblistings`, {
      headers: {
        "Content-Type": "application/json",
      },
      withCredentials: true, // Include credentials (cookies) with the request
    })
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getJobListings;
};
const c_getJobprofile = async (job_id) => {
  let c_getJobprofile = axios
    .get(`${process.env.NEXT_PUBLIC_API_URL}/api/main/jobprofile/${job_id}`, {
      headers: {
        "Content-Type": "application/json",
      },
      withCredentials: true, // Include credentials (cookies) with the request
    })
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getJobprofile;
};

const c_getproductprofile = async (product_id) => {
  let c_getproductprofile = axios
    .get(
      `${process.env.NEXT_PUBLIC_API_URL}/api/main/productprofile/${product_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true, // Include credentials (cookies) with the request
      }
    )
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getproductprofile;
};

const c_getproducts_cover = async (product_id) => {
  let c_getproducts_cover = axios
    .get(
      `${process.env.NEXT_PUBLIC_API_URL}/api/main/productcover/${product_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true, // Include credentials (cookies) with the request
      }
    )
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getproducts_cover;
};

const c_getproducts_file = async (product_id) => {
  let c_getproducs_file = axios
    .get(
      `${process.env.NEXT_PUBLIC_API_URL}/api/main/productfile/${product_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true, // Include credentials (cookies) with the request
      }
    )
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getproducs_file;
};

const c_getserviceprofile = async (service_id) => {
  let c_getserviceprofile = axios
    .get(
      `${process.env.NEXT_PUBLIC_API_URL}/api/main/serviceprofile/${service_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true, // Include credentials (cookies) with the request
      }
    )
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getserviceprofile;
};

export {
  c_logout,
  c_talents,
  c_products,
  c_pftalent,
  c_pfservices,
  c_pfproduct,
  c_services,
  c_productreviews,
  c_servicereviews,
  c_submit_servicereview,
  c_submit_productreview,
  c_skills,
  c_getJobListings,
  c_getJobprofile,
  c_getproductprofile,
  c_getproducts_cover,
  c_getproducts_file,
  c_getserviceprofile,
};
