import axios from "axios";

const c_getprovinces = async () => {
  try {
    let c_getprovinces = await axios
      .get(`${process.env.NEXT_PUBLIC_API_URL}/api/profile/provinces`, {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      })
      .then((result) => result.data)
      .catch((error) => {
        console.error("Error occurred:", error);
      });
    return c_getprovinces;
  } catch (error) {
    console.error("Error:", error.message);
  }
};

export { c_getprovinces };
