import axios from "axios";

const c_getmunicipality = async () => {
  try {
    let c_getmunicipality = await axios
      .get(`${process.env.NEXT_PUBLIC_API_URL}/api/profile/municipality`, {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      })
      .then((result) => result.data)
      .catch((error) => {
        console.error("Error occurred:", error);
      });
    return c_getmunicipality;
  } catch (error) {
    console.error("Error:", error.message);
  }
};

export { c_getmunicipality };
