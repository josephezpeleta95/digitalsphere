import axios from "axios";

const c_uploadResume = async (resumeFile, userId, userFname, userLname) => {
  try {
    const formData = new FormData();
    formData.append("userId", userId);
    formData.append("resumeFile", resumeFile);
    formData.append("userFname", userFname);
    formData.append("userLname", userLname);
    const c_uploadResume = await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/jobs/resume`,
      formData,
      {
        headers: {
          "Content-Type": "multipart/form-data", // Use multipart/form-data for file uploads
        },
        withCredentials: true,
      }
    );
    const result = c_uploadResume.data;
    if (result.success) {
      return { code: "1111", message: "Information Saved" };
    } else {
      console.error("Error:", result.message);
      return { code: "0000", message: result.message };
    }
  } catch (error) {
    console.error("Error:", error);
    return { code: "0000", message: "Catch an Error!" };
  }
};
const c_getBuildResume = async (userId) => {
  let c_getResume = axios
    .get(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/jobs/builder/${userId}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true, // Include credentials (cookies) with the request
      }
    )
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getResume;
};
const c_deleteproducts = async (product_id) => {
  let c_deletproducts = axios
    .delete(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/deleteproducts/${product_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true, // Include credentials (cookies) with the request
      }
    )
    .then((result) => {
      if (result.data.success) {
        return { code: "1111", message: "Information Saved" };
      } else {
        console.error("Error:", result.data.message);
        return { code: "0000", message: result.message };
      }
    })
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_deletproducts;
};
const c_updateproduct_file = async (
  dbproductId,
  dbproductName,
  dbproductDescription,
  dbprice,
  coverfile,
  productfile,
  dbuserId,
  dbproductFileId
) => {
  const formData = new FormData();
  formData.append("dbproductId", dbproductId);
  formData.append("dbuserId", dbuserId);
  formData.append("dbproductFileId", dbproductFileId);
  formData.append("dbproductName", dbproductName);
  formData.append("dbproductDescription", dbproductDescription);
  formData.append("dbprice", dbprice);

  for (let i = 0; i < coverfile.length; i++) {
    formData.append("coverfile[]", coverfile[i]);
  }

  formData.append("productfile", productfile);

  try {
    const c_updateproduct_file = await axios.put(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/updateproduct`,
      formData,
      {
        headers: {
          "Content-Type": "multipart/form-data", // Set the correct content type for form data
        },
        withCredentials: true,
      }
    );
    const result = c_updateproduct_file.data;
    if (result.success) {
      return { code: "1111", message: "Information Updated" };
    } else {
      return { code: "0000", message: result.message };
    }
  } catch (error) {
    return { code: "0000", message: "Catch an Error!" };
  }
};
const c_buildResume = async (dataForm) => {
  try {
    const c_buildResume = await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/jobs/builder`,
      dataForm,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      }
    );
    const result = c_buildResume.data;
    if (result.success) {
      return { code: "1111", message: "Information Saved" };
    } else {
      console.error("Error:", result.message);
      return { code: "0000", message: result.message };
    }
  } catch (error) {
    console.error("Error:", error);
    return { code: "0000", message: "Catch an Error!" };
  }
};
const c_getJobListings = async () => {
  let c_getJobListings = axios
    .get(`${process.env.NEXT_PUBLIC_API_URL}/api/profile/jobs/joblistings`, {
      headers: {
        "Content-Type": "application/json",
      },
      withCredentials: true, // Include credentials (cookies) with the request
    })
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getJobListings;
};

const c_submitJobApplication = async (userId, jobId, companyId) => {
  try {
    const c_submitJobApplication = await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/jobs/jobapplication`,
      {
        jobId: jobId,
        userId: userId,
        companyId: companyId,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      }
    );
    const result = c_submitJobApplication.data;
    if (result.success) {
      return { code: "1111", message: "Information Saved" };
    } else {
      console.error("Error:", result.message);
      return { code: "0000", message: "Information not saved" };
    }
  } catch (error) {
    console.error("Error:", error);
    return { code: "0000", message: "Catch an Error!" };
  }
};
const c_getJobApplication = async (userId) => {
  let c_getJobApplication = axios
    .get(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/jobs/jobapplication/${userId}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true, // Include credentials (cookies) with the request
      }
    )
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getJobApplication;
};
const c_getJobprofile = async (job_id) => {
  let c_getJobprofile = axios
    .get(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/jobprofile/${job_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true, // Include credentials (cookies) with the request
      }
    )
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getJobprofile;
};
export {
  c_uploadResume,
  c_deleteproducts,
  c_updateproduct_file,
  c_buildResume,
  c_getBuildResume,
  c_getJobListings,
  c_submitJobApplication,
  c_getJobApplication,
  c_getJobprofile,
};
