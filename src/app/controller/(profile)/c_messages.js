import axios from "axios";
const c_submitmessage = async (recipientId, threadId, subject, message) => {
  try {
    const c_submitmessage = await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/message`,
      {
        recipientId: recipientId,
        threadId: threadId,
        subject: subject,
        message: message,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      }
    );
    const result = c_submitmessage.data;
    if (result.success) {
      return { code: "1111", message: "Information Saved" };
    } else {
      console.error("Error:", result.message);
      return { code: "0000", message: result.message };
    }
  } catch (error) {
    console.error("Error:", error);
    return { code: "0000", message: "Catch an Error!" };
  }
};
const c_getmessages = async () => {
  try {
    let c_getmessages = await axios
      .get(`${process.env.NEXT_PUBLIC_API_URL}/api/profile/message`, {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      })
      .then((result) => result.data)
      .catch((error) => {
        console.error("Error occurred:", error);
      });
    return c_getmessages;
  } catch (error) {
    console.error("Error:", error.message);
  }
};
const c_getmessagethread = async (threadId) => {
  try {
    let c_getmessagethread = await axios
      .get(
        `${process.env.NEXT_PUBLIC_API_URL}/api/profile/message/${threadId}`,
        {
          headers: {
            "Content-Type": "application/json",
          },
          withCredentials: true,
        }
      )
      .then((result) => result.data)
      .catch((error) => {
        console.error("Error occurred:", error);
      });
    return c_getmessagethread;
  } catch (error) {
    console.error("Error:", error.message);
  }
};
export { c_submitmessage, c_getmessages, c_getmessagethread };
