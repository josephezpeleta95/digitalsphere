import axios from "axios";
const c_saveuserprofile = async (
  username,
  userlname,
  userprov,
  usermuni,
  useremail,
  userskills,
  userheadline,
  userabout,
  userprofilepicpath
) => {
  try {
    const c_saveuserprofile = await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/userprofile`,
      {
        username: username,
        userlname: userlname,
        userprov: userprov,
        usermuni: usermuni,
        useremail: useremail,
        userskills: userskills,
        userheadline: userheadline,
        userabout: userabout,
        userprofilepicpath: userprofilepicpath,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      }
    );
    const result = c_saveuserprofile.data;
    if (result.success) {
      return { code: "1111", message: "Information Saved" };
    } else {
      console.error("Error:", result.message);
      return { code: "0000", message: result.message };
    }
  } catch (error) {
    return { code: "0000", message: "Catch an Error!" };
  }
};

const c_getprofile = async () => {
  try {
    let c_getprofile = await axios
      .get(`${process.env.NEXT_PUBLIC_API_URL}/api/profile/userprofile`, {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      })
      .then((result) => result.data)
      .catch((error) => {
        console.error("Error:", error);
      });
    return c_getprofile;
  } catch (error) {
    console.error("Error:", error);
  }
};

const c_uploaduserphoto = async (file, userid) => {
  try {
    const formData = new FormData();
    formData.append("photo", file);
    formData.append("userid", userid);
    const usersphoto = await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/userphoto`,
      formData, // Pass the formData directly
      {
        headers: {
          "Content-Type": "multipart/form-data", // Set the correct content type for form data
        },
        withCredentials: true, // Assuming you need to send cookies with the request
      }
    );
    const result = usersphoto.data;
    if (result.success) {
      return { code: "1111", message: "Information Saved" };
    } else {
      console.error("Error:", result.message);
      return { code: "0000", message: result.message };
    }
  } catch (error) {
    console.error("Error:", error.message);
  }
};

const c_updateuserprofile = async (
  username,
  userlname,
  userprov,
  usermuni,
  useremail,
  userskills,
  userheadline,
  userabout,
  userLink
) => {
  try {
    const c_updateuserprofile = await axios.put(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/userprofile`,
      {
        username: username,
        userlname: userlname,
        userprov: userprov,
        usermuni: usermuni,
        useremail: useremail,
        userskills: userskills,
        userheadline: userheadline,
        userabout: userabout,
        userLink: userLink,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      }
    );
    const result = c_updateuserprofile.data;
    if (result.success) {
      return { code: "1111", message: "Information Saved" };
    } else {
      console.error("Error:", result.message);
      return { code: "0000", message: result.message };
    }
  } catch (error) {
    return { code: "0000", message: "Catch an Error!" };
  }
};

export {
  c_saveuserprofile,
  c_getprofile,
  c_uploaduserphoto,
  c_updateuserprofile,
};
