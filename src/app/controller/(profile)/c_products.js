import axios from "axios";

const c_saveproducts = async (
  productName,
  productDescription,
  price,
  coverfile,
  productfile,
  user_id
) => {
  try {
    const formData = new FormData();
    formData.append("productName", productName);
    formData.append("productDescription", productDescription);
    formData.append("price", price);

    // Append cover files
    for (let i = 0; i < coverfile.length; i++) {
      formData.append("coverfile[]", coverfile[i]);
    }
    // Append product file
    formData.append("productfile", productfile);
    formData.append("user_id", user_id);

    const c_saveproducts = await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/products`,
      formData,
      {
        headers: {
          "Content-Type": "multipart/form-data",
        },
        withCredentials: true,
      }
    );
    const result = c_saveproducts.data;
    if (result.success) {
      return { code: "1111", message: "Information Saved" };
    } else {
      console.error("Error:", result.message);
      return { code: "0000", message: result.message };
    }
  } catch (error) {
    console.error("Error:", error);
    return { code: "0000", message: "Catch an Error!" };
  }
};
const c_getproducts = async (user_id) => {
  let c_getproducts = axios
    .get(`${process.env.NEXT_PUBLIC_API_URL}/api/profile/products/${user_id}`, {
      headers: {
        "Content-Type": "application/json",
      },
      withCredentials: true, // Include credentials (cookies) with the request
    })
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getproducts;
};
const c_deleteproducts = async (product_id) => {
  let c_deletproducts = axios
    .delete(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/deleteproducts/${product_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true, // Include credentials (cookies) with the request
      }
    )
    .then((result) => {
      if (result.data.success) {
        return { code: "1111", message: "Information Saved" };
      } else {
        console.error("Error:", result.data.message);
        return { code: "0000", message: result.message };
      }
    })
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_deletproducts;
};
const c_getproductprofile = async (product_id) => {
  let c_getproductprofile = axios
    .get(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/productprofile/${product_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true, // Include credentials (cookies) with the request
      }
    )
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getproductprofile;
};
const c_getproducts_cover = async (product_id) => {
  let c_getproducts_cover = axios
    .get(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/productcover/${product_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true, // Include credentials (cookies) with the request
      }
    )
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getproducts_cover;
};
const c_getproducts_file = async (product_id) => {
  let c_getproducs_file = axios
    .get(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/productfile/${product_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true, // Include credentials (cookies) with the request
      }
    )
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getproducs_file;
};
const c_getproducts_download = async (product_id) => {
  let c_getproducts_download = axios
    .get(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/productdownload/${product_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true, // Include credentials (cookies) with the request
      }
    )
    .then((result) => {
      if (result.data) {
        window.location.href = `${process.env.NEXT_PUBLIC_API_URL}/api/profile/productdownload/${product_id}`;
        return "0000";
      } else {
        // Handle error case
        console.error("Failed to download product");
      }
    })
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getproducts_download;
};
const c_deleteproduct_cover = async (product_c_id) => {
  let c_deleteproduct_cover = axios
    .delete(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/deleteproductcover/${product_c_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      }
    )
    .then((result) => {
      if (result.data.success) {
        return { code: "0000", message: "Image deleted..." };
      } else {
        return { code: "1111", message: result.message };
      }
    })
    .catch((error) => {
      return { code: "1111", message: error };
    });

  return c_deleteproduct_cover;
};
const c_deleteproducts_file = async (product_f_id) => {
  let c_deleteproducts_file = axios
    .delete(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/deleteproductfile/${product_f_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      }
    )
    .then((result) => {
      if (result.data.success) {
        return { code: "1111", message: "Image deleted..." };
      } else {
        return { code: "0000", message: result.message };
      }
    })
    .catch((error) => {
      return { code: "0000", message: error };
    });

  return c_deleteproducts_file;
};
const c_updateproduct_file = async (
  dbproductId,
  dbproductName,
  dbproductDescription,
  dbprice,
  coverfile,
  productfile,
  dbuserId,
  dbproductFileId
) => {
  const formData = new FormData();
  formData.append("dbproductId", dbproductId);
  formData.append("dbuserId", dbuserId);
  formData.append("dbproductFileId", dbproductFileId);
  formData.append("dbproductName", dbproductName);
  formData.append("dbproductDescription", dbproductDescription);
  formData.append("dbprice", dbprice);

  for (let i = 0; i < coverfile.length; i++) {
    formData.append("coverfile[]", coverfile[i]);
  }
  formData.append("productfile", productfile);
  try {
    const c_updateproduct_file = await axios.put(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/updateproduct`,
      formData,
      {
        headers: {
          "Content-Type": "multipart/form-data", // Set the correct content type for form data
        },
        withCredentials: true,
      }
    );
    const result = c_updateproduct_file.data;
    if (result.success) {
      return { code: "1111", message: "Information Updated" };
    } else {
      return { code: "0000", message: result.message };
    }
  } catch (error) {
    return { code: "0000", message: "Catch an Error!" };
  }
};

export {
  c_saveproducts,
  c_getproducts,
  c_deleteproducts,
  c_getproducts_cover,
  c_getproductprofile,
  c_getproducts_file,
  c_getproducts_download,
  c_deleteproduct_cover,
  c_deleteproducts_file,
  c_updateproduct_file,
};
