import axios from "axios";

const c_saveservices = async (
  serviceName,
  serviceDescription,
  coverfile,
  rate,
  workSetUp,
  workper,
  userId
) => {
  try {
    const formData = new FormData();
    formData.append("serviceName", serviceName);
    formData.append("serviceDescription", serviceDescription);
    formData.append("rate", rate);
    // Append cover files
    for (let i = 0; i < coverfile.length; i++) {
      formData.append("coverfile[]", coverfile[i]);
    }
    // Append product file
    formData.append("workSetUp", workSetUp);
    formData.append("workper", workper);
    formData.append("userId", userId);

    const c_saveservices = await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/services`,
      formData,
      {
        headers: {
          "Content-Type": "multipart/form-data", // Use multipart/form-data for file uploads
        },
        withCredentials: true,
      }
    );
    const result = c_saveservices.data;
    if (result.success) {
      return { code: "1111", message: "Information Saved" };
    } else {
      console.error("Error:", result.message);
      return { code: "0000", message: result.message };
    }
  } catch (error) {
    console.error("Error:", error);
    return { code: "0000", message: "Catch an Error!" };
  }
};
const c_getservices = async (user_id) => {
  let c_getproducts = axios
    .get(`${process.env.NEXT_PUBLIC_API_URL}/api/profile/services/${user_id}`, {
      headers: {
        "Content-Type": "application/json",
      },
      withCredentials: true, // Include credentials (cookies) with the request
    })
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getproducts;
};
const c_deleteservice_cover = async (service_c_id) => {
  let c_deleteservice_cover = axios
    .delete(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/deleteservicecover/${service_c_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      }
    )
    .then((result) => {
      if (result.data.success) {
        return { code: "1111", message: "Image deleted..." };
      } else {
        return { code: "0000", message: result.message };
      }
    })
    .catch((error) => {
      return { code: "0000", message: error };
    });

  return c_deleteservice_cover;
};
const c_updateservices = async (
  dbserviceId,
  dbserviceUserId,
  dbserviceName,
  serviceworkSetUp,
  serviceWorkPer,
  dbservicerate,
  coverfile,
  dbsnewerviceDescription
) => {
  const formData = new FormData();
  formData.append("dbserviceId", dbserviceId);
  formData.append("dbserviceName", dbserviceName);
  formData.append("serviceworkSetUp", serviceworkSetUp);
  formData.append("serviceWorkPer", serviceWorkPer);
  formData.append("dbservicerate", dbservicerate);
  formData.append("dbsnewerviceDescription", dbsnewerviceDescription);
  formData.append("dbserviceUserId", dbserviceUserId);
  for (let i = 0; i < coverfile.length; i++) {
    formData.append("coverfile[]", coverfile[i]);
  }

  try {
    const c_updateservices = await axios.put(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/updateservice`,
      formData,
      {
        headers: {
          "Content-Type": "multipart/form-data", // Set the correct content type for form data
        },
        withCredentials: true,
      }
    );
    const result = c_updateservices.data;
    if (result.success) {
      return { code: "1111", message: "Information Updated" };
    } else {
      return { code: "0000", message: result.message };
    }
  } catch (error) {
    return { code: "0000", message: "Catch an Error!" };
  }
};
const c_deleteservices = async (service_id) => {
  let c_deleteservices = axios
    .delete(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/deleteservices/${service_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true, // Include credentials (cookies) with the request
      }
    )
    .then((result) => {
      if (result.data.success) {
        return { code: "1111", message: "Information Saved" };
      } else {
        console.error("Error:", result.data.message);
        return { code: "0000", message: result.message };
      }
    })
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_deleteservices;
};
const c_getserviceprofile = async (service_id) => {
  let c_getserviceprofile = axios
    .get(
      `${process.env.NEXT_PUBLIC_API_URL}/api/profile/serviceprofile/${service_id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true, // Include credentials (cookies) with the request
      }
    )
    .then((result) => result.data)
    .catch((error) => {
      console.error("Error occurred:", error);
    });

  return c_getserviceprofile;
};

export {
  c_saveservices,
  c_getservices,
  c_deleteservices,
  c_deleteservice_cover,
  c_updateservices,
  c_getserviceprofile,
};
