import axios from "axios";
const c_generatesession = async (email, name, image) => {
  try {
    const c_generatesession = await axios.post(
      `${process.env.NEXT_PUBLIC_API_URL}/api/main/login`,
      {
        email: email,
        name: name,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      }
    );
    const result = c_generatesession.data;
    if (result.success) {
      return { code: "1111", message: "Information Saved" };
    } else {
      console.error("Error:", result.message);
      return { code: "0000", message: result.message };
    }
  } catch (error) {
    return { code: "0000", message: "Catch an Error!" };
  }
};

export { c_generatesession };
