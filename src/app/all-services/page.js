"use client";
import Header from "@header/header";
import Services from "./onlineservices";
import TopFooter from "@topfooter/topfooter";
import Footer from "@footer/footer";
import { SessionProvider } from "next-auth/react";
import Protected from "../controller/protected";

export default function AllServices() {
  return (
    <div className='flex flex-col items-center justify-between bg-white'>
      <SessionProvider>
        <Header />
      </SessionProvider>
      <Services />
      <TopFooter />
      <Footer />
    </div>
  );
}
