import React, { useEffect, useState } from "react";
import { c_services } from "@/app/controller/(main)/c_main";
import Link from "next/link";
import Loading from "@/app/(profile)/profile/components/loading";

export default function OnlineServices() {
  const [services, setServices] = useState([]);
  const [filterQuery, setFilterQuery] = useState(""); // State to manage the filter query
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  function addCommas(numString) {
    return Number(numString).toLocaleString();
  }

  const ServicesCard = (
    {
      username,
      userlname,
      covers,
      userprofilepic,
      service_rate,
      service_name,
      service_work_setup,
      service_per,
      service_id,
    },
    index
  ) => (
    <>
      {service_name ? (
        <Link
          href={`/service/${service_id}`}
          key={index}
          target='_blank'
          className='w-full sm:w-1/2 md:w-1/3 lg:w-1/4 px-2 py-4'>
          <div className='bg-white shadow-lg rounded-lg overflow-hidden p-2'>
            <div
              className='w-full bg-white h-48 bg-cover bg-center items-end justify-center flex transform transition-transform hover:scale-105 cursor-pointer'
              style={{
                backgroundImage:
                  covers && covers.length > 0 && covers[0].service_cover
                    ? `url(/${covers[0].service_cover.replace(
                        /^\.\/public\//,
                        ""
                      )})`
                    : "none",
              }}>
              <div className='bg-slate-600 bg-opacity-90 w-11/12 p-1 border rounded-full flex mb-1'>
                <div
                  className='w-8 h-8 rounded-full bg-sky-300 bg-cover bg-center border'
                  style={{ backgroundImage: `url(${userprofilepic})` }}></div>
                <div className='items-center flex'>
                  <h5 className='text-xs ml-1 text-white truncate overflow-hidden'>
                    {username} {userlname}
                  </h5>
                </div>
              </div>
            </div>
            <div className='h-20'>
              <div className='bg-slate-50 px-1 h-10 mt-1 justify-center flex py-1 text-slate-600 font-semibold'>
                <div className='w-full'>
                  <h5 className='text-sm line-clamp-2'> {service_name}</h5>
                </div>
              </div>
              <div className='bg-slate-50  flex-wrap  h-10 px-1 justify-center items-center flex py-1 text-sky-600'>
                <div className='w-8/12 text-xs font-semibold'>
                  ₱ {addCommas(service_rate)}
                  {"/"}
                  {service_per}
                </div>
                <div className='w-4/12 text-xs items-start justify-end flex'>
                  ({service_work_setup})
                </div>
              </div>
            </div>
          </div>
        </Link>
      ) : (
        ""
      )}
    </>
  );

  const fetchServices = () => {
    c_services()
      .then((value) => {
        setServices(value.rows);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false);
      });
  };
  // Filter services based on filterQuery
  const filteredServices = services.filter(
    (service) =>
      service.service_name &&
      service.service_name.toLowerCase().includes(filterQuery.toLowerCase())
  );

  useEffect(() => {
    fetchServices();
  }, []);

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div className='bg-white w-9/12 px-4 mt-4'>
      <div className='w-full text-2xl mb-4'>
        <h1 className='font-roboto leading-normal text-sky-700 font-semibold'>
          Online Services
        </h1>
      </div>
      <div className='w-full mb-4'>
        <input
          type='text'
          className='w-full border border-slate-200 p-2 text-gray-700'
          placeholder='Search services...'
          value={filterQuery}
          onChange={(e) => setFilterQuery(e.target.value)}
        />
      </div>
      <div className='flex flex-wrap -mx-2'>
        {/* Map filtered services  */}
        {filteredServices.map((serviceprofile, index) => (
          <ServicesCard {...serviceprofile} key={index} />
        ))}
        {/* End Map filtered services  */}
      </div>
    </div>
  );
}
