import Link from "next/link";
import { signIn } from "next-auth/react";

export default function Jobs() {
  const handleSignInWithGoogle = async () => {
    const callbackUrl = "/callback";
    try {
      // Sign in with Google
      const { url } = await signIn("google", { callbackUrl });
      const popup = window.open(url, "_blank", "width=600,height=600");
      const closePopupTimer = setInterval(() => {
        if (popup.closed) {
          clearInterval(closePopupTimer);
        }
      }, 1000);
    } catch (error) {
      console.error("Error signing in and saving user data:", error);
    }
  };

  return (
    //   Header Navbar
    <div className='w-full h-screen justify-center items-center flex'>
      <div className='w-2/12 '>
        <div className='w-full h-10 rounded rounded-full mt-4 border items-center flex'>
          <div className='w-2/12 '>
            <img
              className='ml-2'
              src='/icons/google.png'
              alt='Google'
              width='30'
              height='30'
            />
          </div>
          <div className='w-10/12 ml-5 '>
            <Link
              href='#'
              className='cursor-pointer'
              onClick={() => handleSignInWithGoogle()}>
              Login with Google
            </Link>
          </div>
        </div>
        <div className='w-full h-10 rounded rounded-full mt-4 border items-center flex mt-10'>
          <div className='w-2/12 '>
            <img
              className='ml-2'
              src='/icons/facebook.png'
              alt='Facebook'
              width='30'
              height='30'
            />
          </div>
          <div className='w-10/12 ml-5'>Login with Facebook</div>
        </div>
        <div className='w-full h-10 rounded rounded-full mt-4 border items-center flex mt-10'>
          <div className='w-2/12 '>
            <img
              className='ml-2'
              src='/icons/linkedin.png'
              alt='Linkedin'
              width='30'
              height='30'
            />
          </div>
          <div className='w-10/12 ml-5'>Login with LinkedIn</div>
        </div>
        <div className='w-full mt-10'>
          <p className='text-xs text-center'>
            By continuing, you agree to Cayap’s Terms of Use and acknowledge
            you’ve read our
            <Link href='#' className='text-orange-500'>
              {" "}
              Privacy Policy
            </Link>
            .
          </p>
        </div>
      </div>
    </div>
  );
}
