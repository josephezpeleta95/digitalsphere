"use client";
import Header from "@header/header";
import Footer from "@footer/footer";
import TopFooter from "@topfooter/topfooter";
import Login from "./login/login";
import { SessionProvider } from "next-auth/react";
export default function LoginPage() {
  return (
    <main className='flex flex-col items-center justify-between bg-white'>
      <SessionProvider>
        <Header />
      </SessionProvider>
      <Login />
      <TopFooter />
      <Footer />
    </main>
  );
}
